<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['perpus_pinjam'] = 'Perpus Pinjam';
$lang['perpus_pinjam_id'] = 'Perpus Pinjam Id';
$lang['perpus_pinjam_tgl_pinjam'] = 'Tgl Pinjam';
$lang['perpus_pinjam_anggotaId'] = 'Anggota';
$lang['perpus_pinjam_tgl_kembali'] = 'Tgl Kembali';
$lang['perpus_pinjam_total_buku'] = 'Total Buku';
$lang['perpus_pinjam_status'] = 'Status';
