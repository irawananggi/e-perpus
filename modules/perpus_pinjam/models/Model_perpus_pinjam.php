<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_perpus_pinjam extends MY_Model {

    private $primary_key    = 'perpus_pinjam_id';
    private $table_name     = 'perpus_pinjam';
    private $field_search   = ['perpus_pinjam_tgl_pinjam', 'perpus_pinjam_anggotaId', 'perpus_pinjam_tgl_kembali', 'perpus_pinjam_total_buku', 'perpus_pinjam_status'];

    public function __construct()
    {
        $config = array(
            'primary_key'   => $this->primary_key,
            'table_name'    => $this->table_name,
            'field_search'  => $this->field_search,
         );

        parent::__construct($config);
    }

    public function count_all($q = null, $field = null)
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "perpus_pinjam.".$field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "perpus_pinjam.".$field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "perpus_pinjam.".$field . " LIKE '%" . $q . "%' )";
        }

        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $query = $this->db->get($this->table_name);

        return $query->num_rows();
    }

    public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [])
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "perpus_pinjam.".$field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "perpus_pinjam.".$field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "perpus_pinjam.".$field . " LIKE '%" . $q . "%' )";
        }

        if (is_array($select_field) AND count($select_field)) {
            $this->db->select($select_field);
        }
        
        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $this->db->limit($limit, $offset);
                $this->db->order_by('perpus_pinjam.'.$this->primary_key, "DESC");
                $query = $this->db->get($this->table_name);

        return $query->result();
    }

    public function join_avaiable() {
        $this->db->join('perpus_murid', 'perpus_murid.perpus_murid_id = perpus_pinjam.perpus_pinjam_anggotaId', 'LEFT');
        $this->db->join('transaksi_status', 'transaksi_status.transaksi_status_id = perpus_pinjam.perpus_pinjam_status', 'LEFT');
        
        $this->db->select('perpus_pinjam.*,perpus_murid.perpus_murid_nama as perpus_murid_perpus_murid_nama,transaksi_status.transaksi_status_keterangan as transaksi_status_transaksi_status_keterangan');


        return $this;
    }

    public function filter_avaiable() {

        if (!$this->aauth->is_admin()) {
            }

        return $this;
    }

}

/* End of file Model_perpus_pinjam.php */
/* Location: ./application/models/Model_perpus_pinjam.php */