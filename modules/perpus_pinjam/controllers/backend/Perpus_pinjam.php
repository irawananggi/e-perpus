<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Perpus Pinjam Controller
*| --------------------------------------------------------------------------
*| Perpus Pinjam site
*|
*/
class Perpus_pinjam extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_perpus_pinjam');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Perpus Pinjams
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('perpus_pinjam_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['perpus_pinjams'] = $this->model_perpus_pinjam->get($filter, $field, $this->limit_page, $offset);
		$this->data['perpus_pinjam_counts'] = $this->model_perpus_pinjam->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/perpus_pinjam/index/',
			'total_rows'   => $this->model_perpus_pinjam->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Perpus Pinjam List');
		$this->render('backend/standart/administrator/perpus_pinjam/perpus_pinjam_list', $this->data);
	}
	
	/**
	* Add new perpus_pinjams
	*
	*/
	public function add()
	{
		$this->is_allowed('perpus_pinjam_add');

		$this->template->title('Perpus Pinjam New');
		$this->render('backend/standart/administrator/perpus_pinjam/perpus_pinjam_add', $this->data);
	}


	/**
	* Add New Perpus Pinjams
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('perpus_pinjam_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('perpus_pinjam_tgl_pinjam', 'Tgl Pinjam', 'trim|required');
		$this->form_validation->set_rules('perpus_pinjam_anggotaId', 'Anggota', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('perpus_pinjam_tgl_kembali', 'Tgl Kembali', 'trim|required');
		$this->form_validation->set_rules('perpus_pinjam_total_buku', 'Total Buku', 'trim|required|max_length[4]');
		$this->form_validation->set_rules('perpus_pinjam_status', 'Status', 'trim|required|max_length[1]');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'perpus_pinjam_tgl_pinjam' => $this->input->post('perpus_pinjam_tgl_pinjam'),
				'perpus_pinjam_anggotaId' => $this->input->post('perpus_pinjam_anggotaId'),
				'perpus_pinjam_tgl_kembali' => $this->input->post('perpus_pinjam_tgl_kembali'),
				'perpus_pinjam_total_buku' => $this->input->post('perpus_pinjam_total_buku'),
				'perpus_pinjam_status' => $this->input->post('perpus_pinjam_status'),
			];

			
			$save_perpus_pinjam = $this->model_perpus_pinjam->store($save_data);
            

			if ($save_perpus_pinjam) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_perpus_pinjam;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/perpus_pinjam/edit/' . $save_perpus_pinjam, 'Edit Perpus Pinjam'),
						anchor('administrator/perpus_pinjam', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/perpus_pinjam/edit/' . $save_perpus_pinjam, 'Edit Perpus Pinjam')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/perpus_pinjam');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/perpus_pinjam');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Perpus Pinjams
	*
	* @var $id String
	*/
	public function kembali_transaksi($id)
	{
		$this->is_allowed('perpus_pinjam_update');

		$this->data['perpus_pinjam'] = $this->model_perpus_pinjam->find($id);

		$this->template->title('Perpus Pinjam Update');
		$this->render('backend/standart/administrator/perpus_pinjam/perpus_pinjam_kembali_transaksi', $this->data);
	}


	/**
	* Update Perpus Pinjams
	*
	* @var $id String
	*/
	public function kembali_transaksi_save($id)
	{
		if (!$this->is_allowed('perpus_pinjam_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('perpus_pinjam_tgl_dikembalikan', 'Tgl Pinjam', 'trim|required');

			$gettrans = $this->mymodel->getbywhere('perpus_pinjam',"perpus_pinjam_id",$id,'row');
			$getdenda = $this->mymodel->withquery("select * from perpus_denda where perpus_denda_status='A' ORDER BY perpus_denda_id DESC","row");


			$awal  = strtotime($gettrans->perpus_pinjam_tgl_dikembalikan);
			$akhir = time(); // waktu sekarang
			$selisih  = $akhir-$awal;
			$totselisih  = floor($selisih / (60 * 60 * 24));



		if ($this->form_validation->run()) {
		
			$save_data = [
				'perpus_kembali_pinjamId' => $id,
				'perpus_kembali_tgl_dikembalikan' => $this->input->post('perpus_pinjam_tgl_dikembalikan'),
				'perpus_kembali_terlambat' => $totselisih,
				'perpus_kembali_dendaId' => $getdenda->perpus_denda_id,
				'perpus_kembali_denda' => $totselisih*$getdenda->perpus_denda_nominal,
			];

		    $save_perpus_pinjam = $this->mymodel->insert(' perpus_kembali ',$save_data);

			if ($save_perpus_pinjam) {
				$datas = array(
				  "perpus_pinjam_status" =>2
				);
				$ss = $this->mymodel->update('perpus_pinjam ',$datas,"perpus_pinjam_id=",$id);
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/perpus_pinjam', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/perpus_pinjam');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/perpus_pinjam');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	
		/**
	* Update view Perpus Pinjams
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('perpus_pinjam_update');

		$this->data['perpus_pinjam'] = $this->model_perpus_pinjam->find($id);

		$this->template->title('Perpus Pinjam Update');
		$this->render('backend/standart/administrator/perpus_pinjam/perpus_pinjam_update', $this->data);
	}

	/**
	* Update Perpus Pinjams
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('perpus_pinjam_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('perpus_pinjam_tgl_pinjam', 'Tgl Pinjam', 'trim|required');
		$this->form_validation->set_rules('perpus_pinjam_anggotaId', 'Anggota', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('perpus_pinjam_tgl_kembali', 'Tgl Kembali', 'trim|required');
		$this->form_validation->set_rules('perpus_pinjam_total_buku', 'Total Buku', 'trim|required|max_length[4]');
		$this->form_validation->set_rules('perpus_pinjam_status', 'Status', 'trim|required|max_length[1]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'perpus_pinjam_tgl_pinjam' => $this->input->post('perpus_pinjam_tgl_pinjam'),
				'perpus_pinjam_anggotaId' => $this->input->post('perpus_pinjam_anggotaId'),
				'perpus_pinjam_tgl_kembali' => $this->input->post('perpus_pinjam_tgl_kembali'),
				'perpus_pinjam_total_buku' => $this->input->post('perpus_pinjam_total_buku'),
				'perpus_pinjam_status' => $this->input->post('perpus_pinjam_status'),
			];

			
			$save_perpus_pinjam = $this->model_perpus_pinjam->change($id, $save_data);

			if ($save_perpus_pinjam) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/perpus_pinjam', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/perpus_pinjam');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/perpus_pinjam');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Perpus Pinjams
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('perpus_pinjam_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'perpus_pinjam'), 'success');
        } else {
            set_message(cclang('error_delete', 'perpus_pinjam'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Perpus Pinjams
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('perpus_pinjam_view');

		$this->data['perpus_pinjam'] = $this->model_perpus_pinjam->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Perpus Pinjam Detail');
		$this->render('backend/standart/administrator/perpus_pinjam/perpus_pinjam_view', $this->data);
	}
	
	/**
	* delete Perpus Pinjams
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$perpus_pinjam = $this->model_perpus_pinjam->find($id);

		
		
		return $this->model_perpus_pinjam->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('perpus_pinjam_export');

		$this->model_perpus_pinjam->export('perpus_pinjam', 'perpus_pinjam');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('perpus_pinjam_export');

		$this->model_perpus_pinjam->pdf('perpus_pinjam', 'perpus_pinjam');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('perpus_pinjam_export');

		$table = $title = 'perpus_pinjam';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_perpus_pinjam->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file perpus_pinjam.php */
/* Location: ./application/controllers/administrator/Perpus Pinjam.php */