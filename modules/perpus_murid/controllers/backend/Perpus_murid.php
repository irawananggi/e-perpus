<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Perpus Murid Controller
*| --------------------------------------------------------------------------
*| Perpus Murid site
*|
*/
class Perpus_murid extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_perpus_murid');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Perpus Murids
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('perpus_murid_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['perpus_murids'] = $this->model_perpus_murid->get($filter, $field, $this->limit_page, $offset);
		$this->data['perpus_murid_counts'] = $this->model_perpus_murid->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/perpus_murid/index/',
			'total_rows'   => $this->model_perpus_murid->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Perpus Murid List');
		$this->render('backend/standart/administrator/perpus_murid/perpus_murid_list', $this->data);
	}
	
	/**
	* Add new perpus_murids
	*
	*/
	public function add()
	{
		$this->is_allowed('perpus_murid_add');

		$this->template->title('Perpus Murid New');
		$this->render('backend/standart/administrator/perpus_murid/perpus_murid_add', $this->data);
	}

	/**
	* Add New Perpus Murids
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('perpus_murid_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('perpus_murid_nama', 'Nama', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('perpus_murid_kelasId', 'Kelas', 'trim|required|max_length[2]');
		$this->form_validation->set_rules('perpus_murid_agamaId', 'Agama', 'trim|required|max_length[2]');
		$this->form_validation->set_rules('perpus_murid_jk', 'Perpus Murid Jk', 'trim|required');
		$this->form_validation->set_rules('perpus_murid_no_telp', 'Telepon', 'trim|required|max_length[15]');
		$this->form_validation->set_rules('perpus_murid_alamat', 'Alamat', 'trim|required');
		$this->form_validation->set_rules('perpus_murid_ket', 'Keterangan', 'trim|required');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'perpus_murid_nama' => $this->input->post('perpus_murid_nama'),
				'perpus_murid_kelasId' => $this->input->post('perpus_murid_kelasId'),
				'perpus_murid_agamaId' => $this->input->post('perpus_murid_agamaId'),
				'perpus_murid_jk' => $this->input->post('perpus_murid_jk'),
				'perpus_murid_no_telp' => $this->input->post('perpus_murid_no_telp'),
				'perpus_murid_alamat' => $this->input->post('perpus_murid_alamat'),
				'perpus_murid_ket' => $this->input->post('perpus_murid_ket'),
				'perpus_murid_username' => $this->input->post('perpus_murid_username'),
				'perpus_murid_password' => $this->input->post('perpus_murid_password'),
			];

			
			$save_perpus_murid = $this->model_perpus_murid->store($save_data);
            

			if ($save_perpus_murid) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_perpus_murid;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/perpus_murid/edit/' . $save_perpus_murid, 'Edit Perpus Murid'),
						anchor('administrator/perpus_murid', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/perpus_murid/edit/' . $save_perpus_murid, 'Edit Perpus Murid')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/perpus_murid');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/perpus_murid');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Perpus Murids
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('perpus_murid_update');

		$this->data['perpus_murid'] = $this->model_perpus_murid->find($id);

		$this->template->title('Perpus Murid Update');
		$this->render('backend/standart/administrator/perpus_murid/perpus_murid_update', $this->data);
	}

	/**
	* Update Perpus Murids
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('perpus_murid_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('perpus_murid_nama', 'Nama', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('perpus_murid_kelasId', 'Kelas', 'trim|required|max_length[2]');
		$this->form_validation->set_rules('perpus_murid_agamaId', 'Agama', 'trim|required|max_length[2]');
		$this->form_validation->set_rules('perpus_murid_jk', 'Perpus Murid Jk', 'trim|required');
		$this->form_validation->set_rules('perpus_murid_no_telp', 'Telepon', 'trim|required|max_length[15]');
		$this->form_validation->set_rules('perpus_murid_alamat', 'Alamat', 'trim|required');
		$this->form_validation->set_rules('perpus_murid_ket', 'Keterangan', 'trim|required');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'perpus_murid_nama' => $this->input->post('perpus_murid_nama'),
				'perpus_murid_kelasId' => $this->input->post('perpus_murid_kelasId'),
				'perpus_murid_agamaId' => $this->input->post('perpus_murid_agamaId'),
				'perpus_murid_jk' => $this->input->post('perpus_murid_jk'),
				'perpus_murid_no_telp' => $this->input->post('perpus_murid_no_telp'),
				'perpus_murid_alamat' => $this->input->post('perpus_murid_alamat'),
				'perpus_murid_ket' => $this->input->post('perpus_murid_ket'),
				'perpus_murid_username' => $this->input->post('perpus_murid_username'),
				'perpus_murid_password' => $this->input->post('perpus_murid_password'),
			];

			
			$save_perpus_murid = $this->model_perpus_murid->change($id, $save_data);

			if ($save_perpus_murid) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/perpus_murid', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/perpus_murid');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/perpus_murid');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Perpus Murids
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('perpus_murid_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'perpus_murid'), 'success');
        } else {
            set_message(cclang('error_delete', 'perpus_murid'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Perpus Murids
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('perpus_murid_view');

		$this->data['perpus_murid'] = $this->model_perpus_murid->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Perpus Murid Detail');
		$this->render('backend/standart/administrator/perpus_murid/perpus_murid_view', $this->data);
	}
	
	/**
	* delete Perpus Murids
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$perpus_murid = $this->model_perpus_murid->find($id);

		
		
		return $this->model_perpus_murid->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('perpus_murid_export');

		$this->model_perpus_murid->export('perpus_murid', 'perpus_murid');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('perpus_murid_export');

		$this->model_perpus_murid->pdf('perpus_murid', 'perpus_murid');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('perpus_murid_export');

		$table = $title = 'perpus_murid';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_perpus_murid->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file perpus_murid.php */
/* Location: ./application/controllers/administrator/Perpus Murid.php */