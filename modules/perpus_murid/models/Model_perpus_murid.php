<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_perpus_murid extends MY_Model {

    private $primary_key    = 'perpus_murid_id';
    private $table_name     = 'perpus_murid';
    private $field_search   = ['perpus_murid_nama', 'perpus_murid_kelasId', 'perpus_murid_agamaId', 'perpus_murid_jk', 'perpus_murid_no_telp', 'perpus_murid_alamat', 'perpus_murid_ket', 'perpus_murid_username', 'perpus_murid_password'];

    public function __construct()
    {
        $config = array(
            'primary_key'   => $this->primary_key,
            'table_name'    => $this->table_name,
            'field_search'  => $this->field_search,
         );

        parent::__construct($config);
    }

    public function count_all($q = null, $field = null)
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "perpus_murid.".$field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "perpus_murid.".$field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "perpus_murid.".$field . " LIKE '%" . $q . "%' )";
        }

        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $query = $this->db->get($this->table_name);

        return $query->num_rows();
    }

    public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [])
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "perpus_murid.".$field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "perpus_murid.".$field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "perpus_murid.".$field . " LIKE '%" . $q . "%' )";
        }

        if (is_array($select_field) AND count($select_field)) {
            $this->db->select($select_field);
        }
        
        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $this->db->limit($limit, $offset);
                $this->db->order_by('perpus_murid.'.$this->primary_key, "DESC");
                $query = $this->db->get($this->table_name);

        return $query->result();
    }

    public function join_avaiable() {
        $this->db->join('perpus_kelas', 'perpus_kelas.perpus_kelas_id = perpus_murid.perpus_murid_kelasId', 'LEFT');
        $this->db->join('perpus_agama', 'perpus_agama.perpus_agama_id = perpus_murid.perpus_murid_agamaId', 'LEFT');
        $this->db->join('jenis_kelamin', 'jenis_kelamin.jenis_kelamin_id = perpus_murid.perpus_murid_jk', 'LEFT');
        
        $this->db->select('perpus_murid.*,perpus_kelas.perpus_kelas_nama as perpus_kelas_perpus_kelas_nama,perpus_agama.perpus_agama_nama as perpus_agama_perpus_agama_nama,jenis_kelamin.jenis_kelamin_name as jenis_kelamin_jenis_kelamin_name');


        return $this;
    }

    public function filter_avaiable() {

        if (!$this->aauth->is_admin()) {
            }

        return $this;
    }

}

/* End of file Model_perpus_murid.php */
/* Location: ./application/models/Model_perpus_murid.php */