<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['perpus_murid'] = 'Perpus Murid';
$lang['perpus_murid_id'] = 'Perpus Murid Id';
$lang['perpus_murid_nama'] = 'Nama';
$lang['perpus_murid_kelasId'] = 'Kelas';
$lang['perpus_murid_agamaId'] = 'Agama';
$lang['perpus_murid_jk'] = 'Perpus Murid Jk';
$lang['perpus_murid_no_telp'] = 'Telepon';
$lang['perpus_murid_alamat'] = 'Alamat';
$lang['perpus_murid_ket'] = 'Keterangan';
$lang['perpus_murid_username'] = 'Username';
$lang['perpus_murid_password'] = 'Password';
