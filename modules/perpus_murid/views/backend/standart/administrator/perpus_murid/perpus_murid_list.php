
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>

<script type="text/javascript">
//This page is a result of an autogenerated content made by running test.html with firefox.
function domo(){
 
   // Binding keys
   $('*').bind('keydown', 'Ctrl+a', function assets() {
       window.location.href = BASE_URL + '/administrator/Perpus_murid/add';
       return false;
   });

   $('*').bind('keydown', 'Ctrl+f', function assets() {
       $('#sbtn').trigger('click');
       return false;
   });

   $('*').bind('keydown', 'Ctrl+x', function assets() {
       $('#reset').trigger('click');
       return false;
   });

   $('*').bind('keydown', 'Ctrl+b', function assets() {

       $('#reset').trigger('click');
       return false;
   });
}

jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
   <h1>
      Murid<small><?= cclang('list_all'); ?></small>
   </h1>
   <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Murid</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row" >
      
      <div class="col-md-12">
         <div class="box box-warning">
            <div class="box-body ">
               <!-- Widget: user widget style 1 -->
               <div class="box box-widget widget-user-2">
                  <!-- Add the bg color to the header using any of the bg-* classes -->
                  <div class="widget-user-header ">
                     <div class="row pull-right">
                        <?php is_allowed('perpus_murid_add', function(){?>
                        <a class="btn btn-flat btn-success btn_add_new" id="btn_add_new" title="<?= cclang('add_new_button', [cclang('perpus_murid')]); ?>  (Ctrl+a)" href="<?=  site_url('administrator/perpus_murid/add'); ?>"><i class="fa fa-plus-square-o" ></i> <?= cclang('add_new_button', [cclang('perpus_murid')]); ?></a>
                        <?php }) ?>
                        <?php is_allowed('perpus_murid_export', function(){?>
                        <a class="btn btn-flat btn-success" title="<?= cclang('export'); ?> Murid']); ?>" href="<?= site_url('administrator/perpus_murid/export'); ?>"><i class="fa fa-file-excel-o" ></i> <?= cclang('export'); ?> XLS</a>
                        <?php }) ?>
                        <?php is_allowed('perpus_murid_export', function(){?>
                        <a class="btn btn-flat btn-success" title="<?= cclang('export'); ?> pdf Murid']); ?>" href="<?= site_url('administrator/perpus_murid/export_pdf'); ?>"><i class="fa fa-file-pdf-o" ></i> <?= cclang('export'); ?> PDF</a>
                        <?php }) ?>
                     </div>
                     <div class="widget-user-image">
                        <img class="img-circle" src="<?= BASE_ASSET; ?>/img/list.png" alt="User Avatar">
                     </div>
                     <!-- /.widget-user-image -->
                     <h3 class="widget-user-username">Murid</h3>
                     <h5 class="widget-user-desc"><?= cclang('list_all', [cclang('perpus_murid')]); ?>  <i class="label bg-yellow"><?= $perpus_murid_counts; ?>  <?= cclang('items'); ?></i></h5>
                  </div>

                  <form name="form_perpus_murid" id="form_perpus_murid" action="<?= base_url('administrator/perpus_murid/index'); ?>">
                  

                  <div class="table-responsive"> 
                  <table class="table table-bordered table-striped dataTable">
                     <thead>
                        <tr class="">
                                                     <th>
                            <input type="checkbox" class="flat-red toltip" id="check_all" name="check_all" title="check all">
                           </th>
                                                    <th> <?= cclang('perpus_murid_nama') ?></th>
                           <th> <?= cclang('perpus_murid_kelasId') ?></th>
                           <th> <?= cclang('perpus_murid_agamaId') ?></th>
                           <th> <?= cclang('perpus_murid_jk') ?></th>
                           <th> <?= cclang('perpus_murid_no_telp') ?></th>
                           <th> <?= cclang('perpus_murid_alamat') ?></th>
                           <th> <?= cclang('perpus_murid_ket') ?></th>
                           <th> <?= cclang('perpus_murid_username') ?></th>
                           <th> <?= cclang('perpus_murid_password') ?></th>
                           <th>Action</th>                        </tr>
                     </thead>
                     <tbody id="tbody_perpus_murid">
                     <?php foreach($perpus_murids as $perpus_murid): ?>
                        <tr>
                                                       <td width="5">
                              <input type="checkbox" class="flat-red check" name="id[]" value="<?= $perpus_murid->perpus_murid_id; ?>">
                           </td>
                                                       
                           <td><?= _ent($perpus_murid->perpus_murid_nama); ?></td> 
                           <td><?php if  ($perpus_murid->perpus_murid_kelasId) {

                              echo anchor('administrator/perpus_kelas/view/'.$perpus_murid->perpus_murid_kelasId.'?popup=show', $perpus_murid->perpus_kelas_perpus_kelas_nama, ['class' => 'popup-view']); }?> </td>
                             
                           <td><?php if  ($perpus_murid->perpus_murid_agamaId) {

                              echo anchor('administrator/perpus_agama/view/'.$perpus_murid->perpus_murid_agamaId.'?popup=show', $perpus_murid->perpus_agama_perpus_agama_nama, ['class' => 'popup-view']); }?> </td>
                             
                           <td><?php if  ($perpus_murid->perpus_murid_jk) {

                              echo anchor('administrator/jenis_kelamin/view/'.$perpus_murid->perpus_murid_jk.'?popup=show', $perpus_murid->jenis_kelamin_jenis_kelamin_name, ['class' => 'popup-view']); }?> </td>
                             
                           <td><?= _ent($perpus_murid->perpus_murid_no_telp); ?></td> 
                           <td><?= _ent($perpus_murid->perpus_murid_alamat); ?></td> 
                           <td><?= _ent($perpus_murid->perpus_murid_ket); ?></td> 
                           <td><?= _ent($perpus_murid->perpus_murid_username); ?></td> 
                           <td><?= _ent($perpus_murid->perpus_murid_password); ?></td> 
                           <td width="200">
                            
                                                              <?php is_allowed('perpus_murid_view', function() use ($perpus_murid){?>
                                 <a href="<?= site_url('administrator/perpus_murid/single_pdf/' .$perpus_murid->perpus_murid_id); ?>" class="label-default"><i class="fa fa-file-pdf-o"></i> <?= cclang('PDF') ?>
                              <a href="<?= site_url('administrator/perpus_murid/view/' . $perpus_murid->perpus_murid_id); ?>" class="label-default"><i class="fa fa-newspaper-o"></i> <?= cclang('view_button'); ?>
                              <?php }) ?>
                              <?php is_allowed('perpus_murid_update', function() use ($perpus_murid){?>
                              <a href="<?= site_url('administrator/perpus_murid/edit/' . $perpus_murid->perpus_murid_id); ?>" class="label-default"><i class="fa fa-edit "></i> <?= cclang('update_button'); ?></a>
                              <?php }) ?>
                              <?php is_allowed('perpus_murid_delete', function() use ($perpus_murid){?>
                              <a href="javascript:void(0);" data-href="<?= site_url('administrator/perpus_murid/delete/' . $perpus_murid->perpus_murid_id); ?>" class="label-default remove-data"><i class="fa fa-close"></i> <?= cclang('remove_button'); ?></a>
                               <?php }) ?>

                           </td>                        </tr>
                      <?php endforeach; ?>
                      <?php if ($perpus_murid_counts == 0) :?>
                         <tr>
                           <td colspan="100">
                           Perpus Murid data is not available
                           </td>
                         </tr>
                      <?php endif; ?>
                     </tbody>
                  </table>
                  </div>
               </div>
               <hr>
               <!-- /.widget-user -->
               <div class="row">
                  <div class="col-md-8">
                     <div class="col-sm-2 padd-left-0 " >
                        <select type="text" class="form-control chosen chosen-select" name="bulk" id="bulk" placeholder="Site Email" >
                           <option value="">Bulk</option>
                                                     <option value="delete">Delete</option>
                                                  </select>
                     </div>
                     <div class="col-sm-2 padd-left-0 ">
                        <button type="button" class="btn btn-flat" name="apply" id="apply" title="<?= cclang('apply_bulk_action'); ?>"><?= cclang('apply_button'); ?></button>
                     </div>
                     <div class="col-sm-3 padd-left-0  " >
                        <input type="text" class="form-control" name="q" id="filter" placeholder="<?= cclang('filter'); ?>" value="<?= $this->input->get('q'); ?>">
                     </div>
                     <div class="col-sm-3 padd-left-0 " >
                        <select type="text" class="form-control chosen chosen-select" name="f" id="field" >
                           <option value=""><?= cclang('all'); ?></option>
                            <option <?= $this->input->get('f') == 'perpus_murid_nama' ? 'selected' :''; ?> value="perpus_murid_nama">Nama</option>
                           <option <?= $this->input->get('f') == 'perpus_murid_kelasId' ? 'selected' :''; ?> value="perpus_murid_kelasId">Kelas</option>
                           <option <?= $this->input->get('f') == 'perpus_murid_agamaId' ? 'selected' :''; ?> value="perpus_murid_agamaId">Agama</option>
                           <option <?= $this->input->get('f') == 'perpus_murid_jk' ? 'selected' :''; ?> value="perpus_murid_jk">Perpus Murid Jk</option>
                           <option <?= $this->input->get('f') == 'perpus_murid_no_telp' ? 'selected' :''; ?> value="perpus_murid_no_telp">Telepon</option>
                           <option <?= $this->input->get('f') == 'perpus_murid_alamat' ? 'selected' :''; ?> value="perpus_murid_alamat">Alamat</option>
                           <option <?= $this->input->get('f') == 'perpus_murid_ket' ? 'selected' :''; ?> value="perpus_murid_ket">Keterangan</option>
                           <option <?= $this->input->get('f') == 'perpus_murid_username' ? 'selected' :''; ?> value="perpus_murid_username">Username</option>
                           <option <?= $this->input->get('f') == 'perpus_murid_password' ? 'selected' :''; ?> value="perpus_murid_password">Password</option>
                          </select>
                     </div>
                     <div class="col-sm-1 padd-left-0 ">
                        <button type="submit" class="btn btn-flat" name="sbtn" id="sbtn" value="Apply" title="<?= cclang('filter_search'); ?>">
                        Filter
                        </button>
                     </div>
                     <div class="col-sm-1 padd-left-0 ">
                        <a class="btn btn-default btn-flat" name="reset" id="reset" value="Apply" href="<?= base_url('administrator/perpus_murid');?>" title="<?= cclang('reset_filter'); ?>">
                        <i class="fa fa-undo"></i>
                        </a>
                     </div>
                  </div>
                  </form>                  <div class="col-md-4">
                     <div class="dataTables_paginate paging_simple_numbers pull-right" id="example2_paginate" >
                        <?= $pagination; ?>
                     </div>
                  </div>
               </div>
            </div>
            <!--/box body -->
         </div>
         <!--/box -->
      </div>
   </div>
</section>
<!-- /.content -->

<!-- Page script -->
<script>
  $(document).ready(function(){
   
    $('.remove-data').click(function(){

      var url = $(this).attr('data-href');

      swal({
          title: "<?= cclang('are_you_sure'); ?>",
          text: "<?= cclang('data_to_be_deleted_can_not_be_restored'); ?>",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "<?= cclang('yes_delete_it'); ?>",
          cancelButtonText: "<?= cclang('no_cancel_plx'); ?>",
          closeOnConfirm: true,
          closeOnCancel: true
        },
        function(isConfirm){
          if (isConfirm) {
            document.location.href = url;            
          }
        });

      return false;
    });


    $('#apply').click(function(){

      var bulk = $('#bulk');
      var serialize_bulk = $('#form_perpus_murid').serialize();

      if (bulk.val() == 'delete') {
         swal({
            title: "<?= cclang('are_you_sure'); ?>",
            text: "<?= cclang('data_to_be_deleted_can_not_be_restored'); ?>",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "<?= cclang('yes_delete_it'); ?>",
            cancelButtonText: "<?= cclang('no_cancel_plx'); ?>",
            closeOnConfirm: true,
            closeOnCancel: true
          },
          function(isConfirm){
            if (isConfirm) {
               document.location.href = BASE_URL + '/administrator/perpus_murid/delete?' + serialize_bulk;      
            }
          });

        return false;

      } else if(bulk.val() == '')  {
          swal({
            title: "Upss",
            text: "<?= cclang('please_choose_bulk_action_first'); ?>",
            type: "warning",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Okay!",
            closeOnConfirm: true,
            closeOnCancel: true
          });

        return false;
      }

      return false;

    });/*end appliy click*/


    //check all
    var checkAll = $('#check_all');
    var checkboxes = $('input.check');

    checkAll.on('ifChecked ifUnchecked', function(event) {   
        if (event.type == 'ifChecked') {
            checkboxes.iCheck('check');
        } else {
            checkboxes.iCheck('uncheck');
        }
    });

    checkboxes.on('ifChanged', function(event){
        if(checkboxes.filter(':checked').length == checkboxes.length) {
            checkAll.prop('checked', 'checked');
        } else {
            checkAll.removeProp('checked');
        }
        checkAll.iCheck('update');
    });

  }); /*end doc ready*/
</script>