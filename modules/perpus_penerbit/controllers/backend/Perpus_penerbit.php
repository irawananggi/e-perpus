<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Perpus Penerbit Controller
*| --------------------------------------------------------------------------
*| Perpus Penerbit site
*|
*/
class Perpus_penerbit extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_perpus_penerbit');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Perpus Penerbits
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('perpus_penerbit_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['perpus_penerbits'] = $this->model_perpus_penerbit->get($filter, $field, $this->limit_page, $offset);
		$this->data['perpus_penerbit_counts'] = $this->model_perpus_penerbit->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/perpus_penerbit/index/',
			'total_rows'   => $this->model_perpus_penerbit->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Perpus Penerbit List');
		$this->render('backend/standart/administrator/perpus_penerbit/perpus_penerbit_list', $this->data);
	}
	
	/**
	* Add new perpus_penerbits
	*
	*/
	public function add()
	{
		$this->is_allowed('perpus_penerbit_add');

		$this->template->title('Perpus Penerbit New');
		$this->render('backend/standart/administrator/perpus_penerbit/perpus_penerbit_add', $this->data);
	}

	/**
	* Add New Perpus Penerbits
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('perpus_penerbit_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('perpus_penerbit_nama', 'Nama Penerbit', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('iperpus_penerbit_provinsiId', 'Provinsi', 'trim|required|max_length[4]');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'perpus_penerbit_nama' => $this->input->post('perpus_penerbit_nama'),
				'iperpus_penerbit_provinsiId' => $this->input->post('iperpus_penerbit_provinsiId'),
			];

			
			$save_perpus_penerbit = $this->model_perpus_penerbit->store($save_data);
            

			if ($save_perpus_penerbit) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_perpus_penerbit;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/perpus_penerbit/edit/' . $save_perpus_penerbit, 'Edit Perpus Penerbit'),
						anchor('administrator/perpus_penerbit', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/perpus_penerbit/edit/' . $save_perpus_penerbit, 'Edit Perpus Penerbit')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/perpus_penerbit');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/perpus_penerbit');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Perpus Penerbits
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('perpus_penerbit_update');

		$this->data['perpus_penerbit'] = $this->model_perpus_penerbit->find($id);

		$this->template->title('Perpus Penerbit Update');
		$this->render('backend/standart/administrator/perpus_penerbit/perpus_penerbit_update', $this->data);
	}

	/**
	* Update Perpus Penerbits
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('perpus_penerbit_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('perpus_penerbit_nama', 'Nama Penerbit', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('iperpus_penerbit_provinsiId', 'Provinsi', 'trim|required|max_length[4]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'perpus_penerbit_nama' => $this->input->post('perpus_penerbit_nama'),
				'iperpus_penerbit_provinsiId' => $this->input->post('iperpus_penerbit_provinsiId'),
			];

			
			$save_perpus_penerbit = $this->model_perpus_penerbit->change($id, $save_data);

			if ($save_perpus_penerbit) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/perpus_penerbit', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/perpus_penerbit');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/perpus_penerbit');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Perpus Penerbits
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('perpus_penerbit_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'perpus_penerbit'), 'success');
        } else {
            set_message(cclang('error_delete', 'perpus_penerbit'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Perpus Penerbits
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('perpus_penerbit_view');

		$this->data['perpus_penerbit'] = $this->model_perpus_penerbit->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Perpus Penerbit Detail');
		$this->render('backend/standart/administrator/perpus_penerbit/perpus_penerbit_view', $this->data);
	}
	
	/**
	* delete Perpus Penerbits
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$perpus_penerbit = $this->model_perpus_penerbit->find($id);

		
		
		return $this->model_perpus_penerbit->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('perpus_penerbit_export');

		$this->model_perpus_penerbit->export('perpus_penerbit', 'perpus_penerbit');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('perpus_penerbit_export');

		$this->model_perpus_penerbit->pdf('perpus_penerbit', 'perpus_penerbit');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('perpus_penerbit_export');

		$table = $title = 'perpus_penerbit';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_perpus_penerbit->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file perpus_penerbit.php */
/* Location: ./application/controllers/administrator/Perpus Penerbit.php */