<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Perpus Detail Buku Controller
*| --------------------------------------------------------------------------
*| Perpus Detail Buku site
*|
*/
class Perpus_detail_buku extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_perpus_detail_buku');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Perpus Detail Bukus
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('perpus_detail_buku_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');
		if($this->input->get('id') != NULL){
		$this->data['perpus_detail_bukus'] = $this->model_perpus_detail_buku->get($filter, $field, $this->limit_page, $offset);
        }else{
		$this->data['perpus_detail_bukus'] = '';
        }
		$this->data['perpus_detail_buku_counts'] = $this->model_perpus_detail_buku->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/perpus_detail_buku/index/',
			'total_rows'   => $this->model_perpus_detail_buku->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Perpus Detail Buku List');
		$this->render('backend/standart/administrator/perpus_detail_buku/perpus_detail_buku_list', $this->data);
	}
	
	/**
	* Add new perpus_detail_bukus
	*
	*/
	public function add()
	{
		$this->is_allowed('perpus_detail_buku_add');

		$this->template->title('Perpus Detail Buku New');
		$this->render('backend/standart/administrator/perpus_detail_buku/perpus_detail_buku_add', $this->data);
	}

	/**
	* Add New Perpus Detail Bukus
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('perpus_detail_buku_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('perpus_detail_buku_no', 'No', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('perpus_detail_buku_status', 'Status', 'trim|required|max_length[1]');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'perpus_detail_bukuId' => $this->input->post('id_buku'),
				'perpus_detail_buku_no' => $this->input->post('perpus_detail_buku_no'),
				'perpus_detail_buku_status' => $this->input->post('perpus_detail_buku_status'),
			];

			
			$save_perpus_detail_buku = $this->model_perpus_detail_buku->store($save_data);
            

			if ($save_perpus_detail_buku) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_perpus_detail_buku;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/perpus_detail_buku/edit/' . $save_perpus_detail_buku, 'Edit Perpus Detail Buku'),
						anchor('administrator/perpus_detail_buku', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/perpus_detail_buku/edit/' . $save_perpus_detail_buku, 'Edit Perpus Detail Buku')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/perpus_detail_buku?id='.$this->input->post('id_buku'));
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/perpus_detail_buku?id='.$this->input->post('id_buku'));
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Perpus Detail Bukus
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('perpus_detail_buku_update');

		$this->data['perpus_detail_buku'] = $this->model_perpus_detail_buku->find($id);

		$this->template->title('Perpus Detail Buku Update');
		$this->render('backend/standart/administrator/perpus_detail_buku/perpus_detail_buku_update', $this->data);
	}

	/**
	* Update Perpus Detail Bukus
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('perpus_detail_buku_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('perpus_detail_bukuId', 'Buku', 'trim|required|max_length[15]');
		$this->form_validation->set_rules('perpus_detail_buku_no', 'No', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('perpus_detail_buku_status', 'Status', 'trim|required|max_length[1]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'perpus_detail_bukuId' => $this->input->post('perpus_detail_bukuId'),
				'perpus_detail_buku_no' => $this->input->post('perpus_detail_buku_no'),
				'perpus_detail_buku_status' => $this->input->post('perpus_detail_buku_status'),
			];

			
			$save_perpus_detail_buku = $this->model_perpus_detail_buku->change($id, $save_data);

			if ($save_perpus_detail_buku) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/perpus_detail_buku', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/perpus_detail_buku?id='.$this->input->post('id_buku'));
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/perpus_detail_buku?id='.$this->input->post('id_buku'));
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Perpus Detail Bukus
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('perpus_detail_buku_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'perpus_detail_buku'), 'success');
        } else {
            set_message(cclang('error_delete', 'perpus_detail_buku'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Perpus Detail Bukus
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('perpus_detail_buku_view');

		$this->data['perpus_detail_buku'] = $this->model_perpus_detail_buku->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Perpus Detail Buku Detail');
		$this->render('backend/standart/administrator/perpus_detail_buku/perpus_detail_buku_view', $this->data);
	}
	
	/**
	* delete Perpus Detail Bukus
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$perpus_detail_buku = $this->model_perpus_detail_buku->find($id);

		
		
		return $this->model_perpus_detail_buku->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('perpus_detail_buku_export');

		$this->model_perpus_detail_buku->export('perpus_detail_buku', 'perpus_detail_buku');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('perpus_detail_buku_export');

		$this->model_perpus_detail_buku->pdf('perpus_detail_buku', 'perpus_detail_buku');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('perpus_detail_buku_export');

		$table = $title = 'perpus_detail_buku';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_perpus_detail_buku->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file perpus_detail_buku.php */
/* Location: ./application/controllers/administrator/Perpus Detail Buku.php */