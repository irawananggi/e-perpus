<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Perpus Provinsi Controller
*| --------------------------------------------------------------------------
*| Perpus Provinsi site
*|
*/
class Perpus_provinsi extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_perpus_provinsi');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Perpus Provinsis
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('perpus_provinsi_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['perpus_provinsis'] = $this->model_perpus_provinsi->get($filter, $field, $this->limit_page, $offset);
		$this->data['perpus_provinsi_counts'] = $this->model_perpus_provinsi->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/perpus_provinsi/index/',
			'total_rows'   => $this->model_perpus_provinsi->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Perpus Provinsi List');
		$this->render('backend/standart/administrator/perpus_provinsi/perpus_provinsi_list', $this->data);
	}
	
	/**
	* Add new perpus_provinsis
	*
	*/
	public function add()
	{
		$this->is_allowed('perpus_provinsi_add');

		$this->template->title('Perpus Provinsi New');
		$this->render('backend/standart/administrator/perpus_provinsi/perpus_provinsi_add', $this->data);
	}

	/**
	* Add New Perpus Provinsis
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('perpus_provinsi_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('perpus_provinsi_nama', 'Nama', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('perpus_provinsi_kota', 'Kota', 'trim|required|max_length[50]');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'perpus_provinsi_nama' => $this->input->post('perpus_provinsi_nama'),
				'perpus_provinsi_kota' => $this->input->post('perpus_provinsi_kota'),
			];

			
			$save_perpus_provinsi = $this->model_perpus_provinsi->store($save_data);
            

			if ($save_perpus_provinsi) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_perpus_provinsi;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/perpus_provinsi/edit/' . $save_perpus_provinsi, 'Edit Perpus Provinsi'),
						anchor('administrator/perpus_provinsi', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/perpus_provinsi/edit/' . $save_perpus_provinsi, 'Edit Perpus Provinsi')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/perpus_provinsi');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/perpus_provinsi');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Perpus Provinsis
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('perpus_provinsi_update');

		$this->data['perpus_provinsi'] = $this->model_perpus_provinsi->find($id);

		$this->template->title('Perpus Provinsi Update');
		$this->render('backend/standart/administrator/perpus_provinsi/perpus_provinsi_update', $this->data);
	}

	/**
	* Update Perpus Provinsis
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('perpus_provinsi_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('perpus_provinsi_nama', 'Nama', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('perpus_provinsi_kota', 'Kota', 'trim|required|max_length[50]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'perpus_provinsi_nama' => $this->input->post('perpus_provinsi_nama'),
				'perpus_provinsi_kota' => $this->input->post('perpus_provinsi_kota'),
			];

			
			$save_perpus_provinsi = $this->model_perpus_provinsi->change($id, $save_data);

			if ($save_perpus_provinsi) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/perpus_provinsi', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/perpus_provinsi');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/perpus_provinsi');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Perpus Provinsis
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('perpus_provinsi_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'perpus_provinsi'), 'success');
        } else {
            set_message(cclang('error_delete', 'perpus_provinsi'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Perpus Provinsis
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('perpus_provinsi_view');

		$this->data['perpus_provinsi'] = $this->model_perpus_provinsi->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Perpus Provinsi Detail');
		$this->render('backend/standart/administrator/perpus_provinsi/perpus_provinsi_view', $this->data);
	}
	
	/**
	* delete Perpus Provinsis
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$perpus_provinsi = $this->model_perpus_provinsi->find($id);

		
		
		return $this->model_perpus_provinsi->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('perpus_provinsi_export');

		$this->model_perpus_provinsi->export('perpus_provinsi', 'perpus_provinsi');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('perpus_provinsi_export');

		$this->model_perpus_provinsi->pdf('perpus_provinsi', 'perpus_provinsi');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('perpus_provinsi_export');

		$table = $title = 'perpus_provinsi';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_perpus_provinsi->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file perpus_provinsi.php */
/* Location: ./application/controllers/administrator/Perpus Provinsi.php */