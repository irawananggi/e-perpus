<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Hasil Polling Controller
*| --------------------------------------------------------------------------
*| Hasil Polling site
*|
*/
class Hasil_polling extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_hasil_polling');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Hasil Pollings
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('hasil_polling_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['hasil_pollings'] = $this->model_hasil_polling->get($filter, $field, $this->limit_page, $offset);
		$this->data['hasil_polling_counts'] = $this->model_hasil_polling->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/hasil_polling/index/',
			'total_rows'   => $this->model_hasil_polling->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Hasil Polling List');
		$this->render('backend/standart/administrator/hasil_polling/hasil_polling_list', $this->data);
	}
	
	/**
	* Add new hasil_pollings
	*
	*/
	public function add()
	{
		$this->is_allowed('hasil_polling_add');

		$this->template->title('Hasil Polling New');
		$this->render('backend/standart/administrator/hasil_polling/hasil_polling_add', $this->data);
	}

	/**
	* Add New Hasil Pollings
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('hasil_polling_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('id_polling[]', 'Id Polling', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('id_warga[]', 'Id Warga', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('id_pilihan_polling[]', 'Id Pilihan Polling', 'trim|required|max_length[11]');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'id_polling' => implode(',', (array) $this->input->post('id_polling')),
				'id_warga' => implode(',', (array) $this->input->post('id_warga')),
				'id_pilihan_polling' => implode(',', (array) $this->input->post('id_pilihan_polling')),
			];

			
			$save_hasil_polling = $this->model_hasil_polling->store($save_data);
            

			if ($save_hasil_polling) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_hasil_polling;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/hasil_polling/edit/' . $save_hasil_polling, 'Edit Hasil Polling'),
						anchor('administrator/hasil_polling', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/hasil_polling/edit/' . $save_hasil_polling, 'Edit Hasil Polling')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/hasil_polling');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/hasil_polling');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Hasil Pollings
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('hasil_polling_update');

		$this->data['hasil_polling'] = $this->model_hasil_polling->find($id);

		$this->template->title('Hasil Polling Update');
		$this->render('backend/standart/administrator/hasil_polling/hasil_polling_update', $this->data);
	}

	/**
	* Update Hasil Pollings
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('hasil_polling_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('id_polling[]', 'Id Polling', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('id_warga[]', 'Id Warga', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('id_pilihan_polling[]', 'Id Pilihan Polling', 'trim|required|max_length[11]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'id_polling' => implode(',', (array) $this->input->post('id_polling')),
				'id_warga' => implode(',', (array) $this->input->post('id_warga')),
				'id_pilihan_polling' => implode(',', (array) $this->input->post('id_pilihan_polling')),
			];

			
			$save_hasil_polling = $this->model_hasil_polling->change($id, $save_data);

			if ($save_hasil_polling) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/hasil_polling', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/hasil_polling');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/hasil_polling');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Hasil Pollings
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('hasil_polling_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'hasil_polling'), 'success');
        } else {
            set_message(cclang('error_delete', 'hasil_polling'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Hasil Pollings
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('hasil_polling_view');

		$this->data['hasil_polling'] = $this->model_hasil_polling->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Hasil Polling Detail');
		$this->render('backend/standart/administrator/hasil_polling/hasil_polling_view', $this->data);
	}
	
	/**
	* delete Hasil Pollings
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$hasil_polling = $this->model_hasil_polling->find($id);

		
		
		return $this->model_hasil_polling->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('hasil_polling_export');

		$this->model_hasil_polling->export('hasil_polling', 'hasil_polling');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('hasil_polling_export');

		$this->model_hasil_polling->pdf('hasil_polling', 'hasil_polling');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('hasil_polling_export');

		$table = $title = 'hasil_polling';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_hasil_polling->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file hasil_polling.php */
/* Location: ./application/controllers/administrator/Hasil Polling.php */