<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_hasil_polling extends MY_Model {

    private $primary_key    = 'id_hasil_polling';
    private $table_name     = 'hasil_polling';
    private $field_search   = ['id_polling', 'id_warga', 'id_pilihan_polling'];

    public function __construct()
    {
        $config = array(
            'primary_key'   => $this->primary_key,
            'table_name'    => $this->table_name,
            'field_search'  => $this->field_search,
         );

        parent::__construct($config);
    }

    public function count_all($q = null, $field = null)
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "hasil_polling.".$field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "hasil_polling.".$field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "hasil_polling.".$field . " LIKE '%" . $q . "%' )";
        }

        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $query = $this->db->get($this->table_name);

        return $query->num_rows();
    }

    public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [])
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "hasil_polling.".$field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "hasil_polling.".$field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "hasil_polling.".$field . " LIKE '%" . $q . "%' )";
        }

        if (is_array($select_field) AND count($select_field)) {
            $this->db->select($select_field);
        }
        
        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $this->db->limit($limit, $offset);
                $this->db->order_by('hasil_polling.'.$this->primary_key, "DESC");
                $query = $this->db->get($this->table_name);

        return $query->result();
    }

    public function join_avaiable() {
        $this->db->join('polling', 'polling.id_polling = hasil_polling.id_polling', 'LEFT');
        $this->db->join('warga', 'warga.id_warga = hasil_polling.id_warga', 'LEFT');
        $this->db->join('pilihan_polling', 'pilihan_polling.id_pilihan_polling = hasil_polling.id_pilihan_polling', 'LEFT');
        
        $this->db->select('hasil_polling.*,polling.judul_polling as polling_judul_polling,warga.nama_lengkap as warga_nama_lengkap,pilihan_polling.nama_pilihan as pilihan_polling_nama_pilihan');


        return $this;
    }

    public function filter_avaiable() {

        if (!$this->aauth->is_admin()) {
            }

        return $this;
    }

}

/* End of file Model_hasil_polling.php */
/* Location: ./application/models/Model_hasil_polling.php */