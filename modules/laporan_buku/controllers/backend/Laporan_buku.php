<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Perpus Buku Controller
*| --------------------------------------------------------------------------
*| Perpus Buku site
*|
*/
class Laporan_buku extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_laporan_buku');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Perpus Bukus
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('laporan_buku_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['perpus_bukus'] = $this->model_laporan_buku->get($filter, $field, $this->limit_page, $offset);
		$this->data['perpus_buku_counts'] = $this->model_laporan_buku->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/laporan_buku/index/',
			'total_rows'   => $this->model_laporan_buku->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Perpus Buku List');
		$this->render('backend/standart/administrator/laporan_buku/laporan_buku_list', $this->data);
	}
	
	/**
	* Add new perpus_bukus
	*
	*/
	public function add()
	{
		$this->is_allowed('perpus_buku_add');

		$this->template->title('Perpus Buku New');
		$this->render('backend/standart/administrator/laporan_buku/perpus_buku_add', $this->data);
	}

	/**
	* Add New Perpus Bukus
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('perpus_buku_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('perpus_buku_isbn', 'ISBN', 'trim|required|max_length[20]');
		$this->form_validation->set_rules('perpus_buku_judul', 'Judull', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('perpus_buku_kategoriId', 'Kategori', 'trim|required|max_length[3]');
		$this->form_validation->set_rules('perpus_buku_penerbitId', 'Penerbit', 'trim|required|max_length[3]');
		$this->form_validation->set_rules('perpus_buku_pengarangId', 'Pengarang', 'trim|required|max_length[3]');
		$this->form_validation->set_rules('perpus_buku_no_rak', 'No Rak', 'trim|required|max_length[2]');
		$this->form_validation->set_rules('perpus_buku_thn_terbit', 'Tahun Terbit', 'trim|required|max_length[4]');
		$this->form_validation->set_rules('perpus_buku_stok', 'Stok', 'trim|required|max_length[3]');
		$this->form_validation->set_rules('perpus_buku_ket', 'Keterangan', 'trim|required');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'perpus_buku_isbn' => $this->input->post('perpus_buku_isbn'),
				'perpus_buku_judul' => $this->input->post('perpus_buku_judul'),
				'perpus_buku_kategoriId' => $this->input->post('perpus_buku_kategoriId'),
				'perpus_buku_penerbitId' => $this->input->post('perpus_buku_penerbitId'),
				'perpus_buku_pengarangId' => $this->input->post('perpus_buku_pengarangId'),
				'perpus_buku_no_rak' => $this->input->post('perpus_buku_no_rak'),
				'perpus_buku_thn_terbit' => $this->input->post('perpus_buku_thn_terbit'),
				'perpus_buku_stok' => $this->input->post('perpus_buku_stok'),
				'perpus_buku_ket' => $this->input->post('perpus_buku_ket'),
			];

			
			$save_perpus_buku = $this->model_laporan_buku->store($save_data);
            

			if ($save_perpus_buku) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_perpus_buku;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/laporan_buku/edit/' . $save_perpus_buku, 'Edit Perpus Buku'),
						anchor('administrator/perpus_buku', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/laporan_buku/edit/' . $save_perpus_buku, 'Edit Perpus Buku')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/perpus_buku');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/perpus_buku');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Perpus Bukus
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('perpus_buku_update');

		$this->data['perpus_buku'] = $this->model_laporan_buku->find($id);

		$this->template->title('Perpus Buku Update');
		$this->render('backend/standart/administrator/laporan_buku/perpus_buku_update', $this->data);
	}

	/**
	* Update Perpus Bukus
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('perpus_buku_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('perpus_buku_isbn', 'ISBN', 'trim|required|max_length[20]');
		$this->form_validation->set_rules('perpus_buku_judul', 'Judull', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('perpus_buku_kategoriId', 'Kategori', 'trim|required|max_length[3]');
		$this->form_validation->set_rules('perpus_buku_penerbitId', 'Penerbit', 'trim|required|max_length[3]');
		$this->form_validation->set_rules('perpus_buku_pengarangId', 'Pengarang', 'trim|required|max_length[3]');
		$this->form_validation->set_rules('perpus_buku_no_rak', 'No Rak', 'trim|required|max_length[2]');
		$this->form_validation->set_rules('perpus_buku_thn_terbit', 'Tahun Terbit', 'trim|required|max_length[4]');
		$this->form_validation->set_rules('perpus_buku_stok', 'Stok', 'trim|required|max_length[3]');
		$this->form_validation->set_rules('perpus_buku_ket', 'Keterangan', 'trim|required');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'perpus_buku_isbn' => $this->input->post('perpus_buku_isbn'),
				'perpus_buku_judul' => $this->input->post('perpus_buku_judul'),
				'perpus_buku_kategoriId' => $this->input->post('perpus_buku_kategoriId'),
				'perpus_buku_penerbitId' => $this->input->post('perpus_buku_penerbitId'),
				'perpus_buku_pengarangId' => $this->input->post('perpus_buku_pengarangId'),
				'perpus_buku_no_rak' => $this->input->post('perpus_buku_no_rak'),
				'perpus_buku_thn_terbit' => $this->input->post('perpus_buku_thn_terbit'),
				'perpus_buku_stok' => $this->input->post('perpus_buku_stok'),
				'perpus_buku_ket' => $this->input->post('perpus_buku_ket'),
			];

			
			$save_perpus_buku = $this->model_laporan_buku->change($id, $save_data);

			if ($save_perpus_buku) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/perpus_buku', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/perpus_buku');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/perpus_buku');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Perpus Bukus
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('perpus_buku_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'perpus_buku'), 'success');
        } else {
            set_message(cclang('error_delete', 'perpus_buku'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Perpus Bukus
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('perpus_buku_view');

		$this->data['perpus_buku'] = $this->model_laporan_buku->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Perpus Buku Detail');
		$this->render('backend/standart/administrator/laporan_buku/perpus_buku_view', $this->data);
	}
	
	/**
	* delete Perpus Bukus
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$perpus_buku = $this->model_laporan_buku->find($id);

		
		
		return $this->model_laporan_buku->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('perpus_buku_export');

		$this->model_laporan_buku->export('perpus_buku', 'perpus_buku');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('perpus_buku_export');

		$this->model_laporan_buku->pdf('perpus_buku', 'perpus_buku');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('perpus_buku_export');

		$table = $title = 'perpus_buku';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_laporan_buku->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file perpus_buku.php */
/* Location: ./application/controllers/administrator/Perpus Buku.php */