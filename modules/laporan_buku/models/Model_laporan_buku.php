<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_laporan_buku extends MY_Model {

    private $primary_key    = 'perpus_buku_id';
    private $table_name     = 'perpus_buku';
    private $field_search   = ['perpus_buku_isbn', 'perpus_buku_judul', 'perpus_buku_kategoriId', 'perpus_buku_penerbitId', 'perpus_buku_pengarangId', 'perpus_buku_no_rak', 'perpus_buku_thn_terbit', 'perpus_buku_stok', 'perpus_buku_ket'];

    public function __construct()
    {
        $config = array(
            'primary_key'   => $this->primary_key,
            'table_name'    => $this->table_name,
            'field_search'  => $this->field_search,
         );

        parent::__construct($config);
    }

    public function count_all($q = null, $field = null)
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "perpus_buku.".$field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "perpus_buku.".$field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "perpus_buku.".$field . " LIKE '%" . $q . "%' )";
        }

        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $query = $this->db->get($this->table_name);

        return $query->num_rows();
    }

    public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [])
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "perpus_buku.".$field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "perpus_buku.".$field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "perpus_buku.".$field . " LIKE '%" . $q . "%' )";
        }

        if (is_array($select_field) AND count($select_field)) {
            $this->db->select($select_field);
        }
        
        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $this->db->limit($limit, $offset);
                $this->db->order_by('perpus_buku.'.$this->primary_key, "DESC");
                $query = $this->db->get($this->table_name);

        return $query->result();
    }

    public function join_avaiable() {
        $this->db->join('perpus_kategori', 'perpus_kategori.perpus_kategori_id = perpus_buku.perpus_buku_kategoriId', 'LEFT');
        $this->db->join('perpus_penerbit', 'perpus_penerbit.perpus_penerbit_id = perpus_buku.perpus_buku_penerbitId', 'LEFT');
        $this->db->join('perpus_pengarang', 'perpus_pengarang.perpus_pengarang_id = perpus_buku.perpus_buku_pengarangId', 'LEFT');
        $this->db->join('perpus_rak', 'perpus_rak.perpus_rak_no_rak = perpus_buku.perpus_buku_no_rak', 'LEFT');
        
        $this->db->select('perpus_buku.*,perpus_kategori.perpus_kategori_nama as perpus_kategori_perpus_kategori_nama,perpus_penerbit.perpus_penerbit_nama as perpus_penerbit_perpus_penerbit_nama,perpus_pengarang.perpus_pengarang_nama as perpus_pengarang_perpus_pengarang_nama,perpus_rak.perpus_rak_nama as perpus_rak_perpus_rak_nama');


        return $this;
    }

    public function filter_avaiable() {

        if (!$this->aauth->is_admin()) {
            }

        return $this;
    }

}

/* End of file Model_perpus_buku.php */
/* Location: ./application/models/Model_perpus_buku.php */