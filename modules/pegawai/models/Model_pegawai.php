<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_pegawai extends MY_Model {

    private $primary_key    = 'pegawai_id';
    private $table_name     = 'pegawai';
    private $field_search   = ['pegawai_nama', 'pegawai_nik', 'pegawai_email', 'pegawai_password', 'pegawai_foto', 'pegawai_jabatan', 'pegawai_jk', 'pegawai_pendidikan', 'pegawai_agama', 'pegawai_kode_referal'];

    public function __construct()
    {
        $config = array(
            'primary_key'   => $this->primary_key,
            'table_name'    => $this->table_name,
            'field_search'  => $this->field_search,
         );

        parent::__construct($config);
    }

    public function count_all($q = null, $field = null)
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "pegawai.".$field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "pegawai.".$field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "pegawai.".$field . " LIKE '%" . $q . "%' )";
        }

        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $query = $this->db->get($this->table_name);

        return $query->num_rows();
    }

    public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [])
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "pegawai.".$field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "pegawai.".$field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "pegawai.".$field . " LIKE '%" . $q . "%' )";
        }

        if (is_array($select_field) AND count($select_field)) {
            $this->db->select($select_field);
        }
        
        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $this->db->limit($limit, $offset);
                $this->db->order_by('pegawai.'.$this->primary_key, "DESC");
                $query = $this->db->get($this->table_name);

        return $query->result();
    }

    public function join_avaiable() {
        $this->db->join('jabatan', 'jabatan.jabatan_id = pegawai.pegawai_jabatan', 'LEFT');
        $this->db->join('jenis_kelamin', 'jenis_kelamin.id_jenis_kelamin = pegawai.pegawai_jk', 'LEFT');
        $this->db->join('pendidikan', 'pendidikan.pendidikan_id = pegawai.pegawai_pendidikan', 'LEFT');
        $this->db->join('agama', 'agama.agama_id = pegawai.pegawai_agama', 'LEFT');
        
        $this->db->select('pegawai.*,jabatan.jabatan_nama as jabatan_jabatan_nama,jenis_kelamin.name as jenis_kelamin_name,pendidikan.pendidikan_nama as pendidikan_pendidikan_nama,agama.agama_nama as agama_agama_nama');


        return $this;
    }

    public function filter_avaiable() {

        if (!$this->aauth->is_admin()) {
            }

        return $this;
    }

}

/* End of file Model_pegawai.php */
/* Location: ./application/models/Model_pegawai.php */