<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['pegawai'] = 'Pegawai';
$lang['pegawai_id'] = 'Pegawai Id';
$lang['pegawai_nama'] = 'Nama';
$lang['pegawai_nik'] = 'NIK';
$lang['pegawai_email'] = 'Email';
$lang['pegawai_password'] = 'Password';
$lang['pegawai_foto'] = 'Foto';
$lang['pegawai_jabatan'] = 'Jabatan';
$lang['pegawai_jk'] = 'JK';
$lang['pegawai_pendidikan'] = 'Pendidikan';
$lang['pegawai_agama'] = 'Agama';
$lang['pegawai_kode_referal'] = 'Kode Referal';
