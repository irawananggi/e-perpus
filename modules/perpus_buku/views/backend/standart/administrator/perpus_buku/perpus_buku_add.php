
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript">
    function domo(){
     
       // Binding keys
       $('*').bind('keydown', 'Ctrl+s', function assets() {
          $('#btn_save').trigger('click');
           return false;
       });
    
       $('*').bind('keydown', 'Ctrl+x', function assets() {
          $('#btn_cancel').trigger('click');
           return false;
       });
    
      $('*').bind('keydown', 'Ctrl+d', function assets() {
          $('.btn_save_back').trigger('click');
           return false;
       });
        
    }
    
    jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Perpus Buku        <small><?= cclang('new', ['Perpus Buku']); ?> </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class=""><a  href="<?= site_url('administrator/perpus_buku'); ?>">Perpus Buku</a></li>
        <li class="active"><?= cclang('new'); ?></li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row" >
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Perpus Buku</h3>
                            <h5 class="widget-user-desc"><?= cclang('new', ['Perpus Buku']); ?></h5>
                            <hr>
                        </div>
                        <?= form_open('', [
                            'name'    => 'form_perpus_buku', 
                            'class'   => 'form-horizontal form-step', 
                            'id'      => 'form_perpus_buku', 
                            'enctype' => 'multipart/form-data', 
                            'method'  => 'POST'
                            ]); ?>
                         
                                                <div class="form-group ">
                            <label for="perpus_buku_isbn" class="col-sm-2 control-label">ISBN 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="perpus_buku_isbn" id="perpus_buku_isbn" placeholder="ISBN" value="<?= set_value('perpus_buku_isbn'); ?>">
                                <small class="info help-block">
                                <b>Input Perpus Buku Isbn</b> Max Length : 20.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="perpus_buku_judul" class="col-sm-2 control-label">Judull 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="perpus_buku_judul" id="perpus_buku_judul" placeholder="Judull" value="<?= set_value('perpus_buku_judul'); ?>">
                                <small class="info help-block">
                                <b>Input Perpus Buku Judul</b> Max Length : 100.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="perpus_buku_kategoriId" class="col-sm-2 control-label">Kategori 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select-deselect" name="perpus_buku_kategoriId" id="perpus_buku_kategoriId" data-placeholder="Select Kategori" >
                                    <option value=""></option>
                                    <?php foreach (db_get_all_data('perpus_kategori') as $row): ?>
                                    <option value="<?= $row->perpus_kategori_id ?>"><?= $row->perpus_kategori_nama; ?></option>
                                    <?php endforeach; ?>  
                                </select>
                                <small class="info help-block">
                                <b>Input Perpus Buku KategoriId</b> Max Length : 3.</small>
                            </div>
                        </div>

                                                 
                                                <div class="form-group ">
                            <label for="perpus_buku_penerbitId" class="col-sm-2 control-label">Penerbit 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select-deselect" name="perpus_buku_penerbitId" id="perpus_buku_penerbitId" data-placeholder="Select Penerbit" >
                                    <option value=""></option>
                                    <?php foreach (db_get_all_data('perpus_penerbit') as $row): ?>
                                    <option value="<?= $row->perpus_penerbit_id ?>"><?= $row->perpus_penerbit_nama; ?></option>
                                    <?php endforeach; ?>  
                                </select>
                                <small class="info help-block">
                                <b>Input Perpus Buku PenerbitId</b> Max Length : 3.</small>
                            </div>
                        </div>

                                                 
                                                <div class="form-group ">
                            <label for="perpus_buku_pengarangId" class="col-sm-2 control-label">Pengarang 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select-deselect" name="perpus_buku_pengarangId" id="perpus_buku_pengarangId" data-placeholder="Select Pengarang" >
                                    <option value=""></option>
                                    <?php foreach (db_get_all_data('perpus_pengarang') as $row): ?>
                                    <option value="<?= $row->perpus_pengarang_id ?>"><?= $row->perpus_pengarang_nama; ?></option>
                                    <?php endforeach; ?>  
                                </select>
                                <small class="info help-block">
                                <b>Input Perpus Buku PengarangId</b> Max Length : 3.</small>
                            </div>
                        </div>

                                                 
                                                <div class="form-group ">
                            <label for="perpus_buku_no_rak" class="col-sm-2 control-label">No Rak 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select-deselect" name="perpus_buku_no_rak" id="perpus_buku_no_rak" data-placeholder="Select No Rak" >
                                    <option value=""></option>
                                    <?php foreach (db_get_all_data('perpus_rak') as $row): ?>
                                    <option value="<?= $row->perpus_rak_no_rak ?>"><?= $row->perpus_rak_nama; ?></option>
                                    <?php endforeach; ?>  
                                </select>
                                <small class="info help-block">
                                <b>Input Perpus Buku No Rak</b> Max Length : 2.</small>
                            </div>
                        </div>

                                                 
                                                <div class="form-group ">
                            <label for="perpus_buku_thn_terbit" class="col-sm-2 control-label">Tahun Terbit 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-2">
                                <select  class="form-control chosen chosen-select-deselect" name="perpus_buku_thn_terbit" id="perpus_buku_thn_terbit" data-placeholder="Select Tahun Terbit" >
                                    <option value=""></option>
                                    <?php for ($i = 1970; $i < date('Y')+100; $i++){ ?>
                                    <option value="<?= $i;?>"><?= $i; ?></option>
                                    <?php }; ?>  
                                </select>
                                <small class="info help-block">
                                <b>Input Perpus Buku Thn Terbit</b> Max Length : 4.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="perpus_buku_stok" class="col-sm-2 control-label">Stok 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="number" class="form-control" name="perpus_buku_stok" id="perpus_buku_stok" placeholder="Stok" value="<?= set_value('perpus_buku_stok'); ?>">
                                <small class="info help-block">
                                <b>Input Perpus Buku Stok</b> Max Length : 3.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="perpus_buku_ket" class="col-sm-2 control-label">Keterangan 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <textarea id="perpus_buku_ket" name="perpus_buku_ket" rows="5" cols="80"><?= set_value('Perpus Buku Ket'); ?></textarea>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                
                        
                                                <div class="message"></div>
                                                <div class="row-fluid col-md-7 container-button-bottom">
                           <button class="btn btn-flat btn-primary btn_save btn_action" id="btn_save" data-stype='stay' title="<?= cclang('save_button'); ?> (Ctrl+s)">
                            <i class="fa fa-save" ></i> <?= cclang('save_button'); ?>
                            </button>
                            <a class="btn btn-flat btn-info btn_save btn_action btn_save_back" id="btn_save" data-stype='back' title="<?= cclang('save_and_go_the_list_button'); ?> (Ctrl+d)">
                            <i class="ion ion-ios-list-outline" ></i> <?= cclang('save_and_go_the_list_button'); ?>
                            </a>
                            <a class="btn btn-flat btn-default btn_action" id="btn_cancel" title="<?= cclang('cancel_button'); ?> (Ctrl+x)">
                            <i class="fa fa-undo" ></i> <?= cclang('cancel_button'); ?>
                            </a>
                            <span class="loading loading-hide">
                            <img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg"> 
                            <i><?= cclang('loading_saving_data'); ?></i>
                            </span>
                        </div>
                                                 <?= form_close(); ?>
                    </div>
                </div>
                <!--/box body -->
            </div>
            <!--/box -->
        </div>
    </div>
</section>
<!-- /.content -->
<script src="<?= BASE_ASSET; ?>ckeditor/ckeditor.js"></script>
<!-- Page script -->
<script>
    $(document).ready(function(){

                   CKEDITOR.replace('perpus_buku_ket'); 
      var perpus_buku_ket = CKEDITOR.instances.perpus_buku_ket;
                   
      $('#btn_cancel').click(function(){
        swal({
            title: "<?= cclang('are_you_sure'); ?>",
            text: "<?= cclang('data_to_be_deleted_can_not_be_restored'); ?>",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: true
          },
          function(isConfirm){
            if (isConfirm) {
              window.location.href = BASE_URL + 'administrator/perpus_buku';
            }
          });
    
        return false;
      }); /*end btn cancel*/
    
      $('.btn_save').click(function(){
        $('.message').fadeOut();
        $('#perpus_buku_ket').val(perpus_buku_ket.getData());
                    
        var form_perpus_buku = $('#form_perpus_buku');
        var data_post = form_perpus_buku.serializeArray();
        var save_type = $(this).attr('data-stype');

        data_post.push({name: 'save_type', value: save_type});
    
        $('.loading').show();
    
        $.ajax({
          url: BASE_URL + '/administrator/perpus_buku/add_save',
          type: 'POST',
          dataType: 'json',
          data: data_post,
        })
        .done(function(res) {
          $('form').find('.form-group').removeClass('has-error');
          $('.steps li').removeClass('error');
          $('form').find('.error-input').remove();
          if(res.success) {
            
            if (save_type == 'back') {
              window.location.href = res.redirect;
              return;
            }
    
            $('.message').printMessage({message : res.message});
            $('.message').fadeIn();
            resetForm();
            $('.chosen option').prop('selected', false).trigger('chosen:updated');
            perpus_buku_ket.setData('');
                
          } else {
            if (res.errors) {
                
                $.each(res.errors, function(index, val) {
                    $('form #'+index).parents('.form-group').addClass('has-error');
                    $('form #'+index).parents('.form-group').find('small').prepend(`
                      <div class="error-input">`+val+`</div>
                      `);
                });
                $('.steps li').removeClass('error');
                $('.content section').each(function(index, el) {
                    if ($(this).find('.has-error').length) {
                        $('.steps li:eq('+index+')').addClass('error').find('a').trigger('click');
                    }
                });
            }
            $('.message').printMessage({message : res.message, type : 'warning'});
          }
    
        })
        .fail(function() {
          $('.message').printMessage({message : 'Error save data', type : 'warning'});
        })
        .always(function() {
          $('.loading').hide();
          $('html, body').animate({ scrollTop: $(document).height() }, 2000);
        });
    
        return false;
      }); /*end btn save*/
      
       
 
       

      
    
    
    }); /*end doc ready*/
</script>