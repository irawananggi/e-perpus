<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['perpus_buku'] = 'Perpus Buku';
$lang['perpus_buku_isbn'] = 'ISBN';
$lang['perpus_buku_judul'] = 'Judull';
$lang['perpus_buku_kategoriId'] = 'Kategori';
$lang['perpus_buku_penerbitId'] = 'Penerbit';
$lang['perpus_buku_pengarangId'] = 'Pengarang';
$lang['perpus_buku_no_rak'] = 'No Rak';
$lang['perpus_buku_thn_terbit'] = 'Tahun Terbit';
$lang['perpus_buku_stok'] = 'Stok';
$lang['perpus_buku_ket'] = 'Keterangan';
