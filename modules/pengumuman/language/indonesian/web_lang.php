<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['pengumuman'] = 'Pengumuman';
$lang['id_pengumuman'] = 'Id Pengumuman';
$lang['id_warga'] = 'Id Warga';
$lang['judul'] = 'Judul';
$lang['deskripsi'] = 'Deskripsi';
$lang['foto_video'] = 'Foto Video';
