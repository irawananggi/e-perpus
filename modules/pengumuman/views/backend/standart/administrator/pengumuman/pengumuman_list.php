<style type="text/css">
    .btn {
    padding: 10px 10px !important;
    font-size: 8px !important;
}

</style>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>

<script type="text/javascript">
//This page is a result of an autogenerated content made by running test.html with firefox.
function domo(){
 
   // Binding keys
   $('*').bind('keydown', 'Ctrl+a', function assets() {
       window.location.href = BASE_URL + '/administrator/Pengumuman/add';
       return false;
   });

   $('*').bind('keydown', 'Ctrl+f', function assets() {
       $('#sbtn').trigger('click');
       return false;
   });

   $('*').bind('keydown', 'Ctrl+x', function assets() {
       $('#reset').trigger('click');
       return false;
   });

   $('*').bind('keydown', 'Ctrl+b', function assets() {

       $('#reset').trigger('click');
       return false;
   });
}

jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<!-- <section class="content-header">
   <h1>
      <?= cclang('pengumuman') ?><small><?= cclang('list_all'); ?></small>
   </h1>
   <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><?= cclang('pengumuman') ?></li>
   </ol>
</section> -->
<!-- Main content -->
<section class="content" style="">
   <div class="row" >
      
      <div class="col-md-12">
         <div class="box box-warning custom-section">
            <div class="box-body ">
               <!-- Widget: user widget style 1 -->
               <div class="box box-widget widget-user-2">
                  <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header"> 
                    <h3 class="title"  style="padding: 10px;">
                      <span>Pengumuman</span>
                    </h3>  
                    <form action="<?= base_url('administrator/pengumuman/index'); ?>">
                       <div class="pull-left">
						
                          <button class="fa fa-search" type="submit" value="Apply" style="background: transparent;border: 1px snow;"></button>
                        
                          <input type="text" placeholder="Cari" class="search-custom" name="q" id="filter"
                             value="<?= $this->input->get('q'); ?>">
                          <button class="button-custom" type="submit" name="sbtn" id="sbtn" value="Apply">
                          </button>
                      </div>
                    </form>
                    <!-- <div class="pull-right">
                        <?php is_allowed('pengumuman_add', function(){?>
                       
						<a class="btn btn-flat btn-success btn_add_new" id="btn_add_new" title="<?= cclang('add_new_button', [cclang('pengumuman')]); ?>  (Ctrl+a)" href="<?=  site_url('administrator/pengumuman/add') ?>" 
                        data-table-title="Tambah Kategori"><i class="fa fa-plus" ></i> Tambah</a>

                        <?php }) ?>

                       
                    </div> -->
                  </div>
                  </div>
<br><br>
                <div class="col-lg-12" style="padding: 0px;">

                  <form name="form_pengumuman" id="form_pengumuman" action="<?= base_url('administrator/pengumuman/index'); ?>">
                  

                  <div class="table-responsive"> 
                  <table class="table table-bordered table-striped dataTable">
                     <thead>
                        <tr class="">
                                   
                                            <th>No</th>
                           <th> <?= cclang('judul') ?></th>
                           <th> <?= cclang('deskripsi') ?></th>
                           <th> <?= cclang('foto_video') ?></th>
                            <th> <?= cclang('tanggal') ?></th>
                            <th> <?= cclang('warga') ?></th>
                           <th>Action</th>                        </tr>
                     </thead>
                     <tbody id="tbody_pengumuman">
                     <?php $no=1;foreach($pengumumans as $pengumuman): ?>
                        <tr>
                                 
                                                      
                            <td> <?php echo $no; ?></td>
                                                       <!-- 
                           <td><?= join_multi_select($pengumuman->id_warga, 'warga', 'id_warga', 'nama_lengkap'); ?></td>  -->
                           <td><?= _ent($pengumuman->judul); ?></td> 
                           <td><?= _ent($pengumuman->deskripsi); ?></td> 
                           <td>
                              <?php if (!empty($pengumuman->foto_video)): ?>
                                <?php if (is_image($pengumuman->foto_video)): ?>
                                <a class="fancybox" rel="group" href="<?= BASE_URL . 'uploads/pengumuman/' . $pengumuman->foto_video; ?>">
                                  <img src="<?= BASE_URL . 'uploads/pengumuman/' . $pengumuman->foto_video; ?>" class="image-responsive" alt="image pengumuman" title="foto_video pengumuman" width="40px">
                                </a>
                                <?php else: ?>
                                  <a href="<?= BASE_URL . 'uploads/pengumuman/' . $pengumuman->foto_video; ?>">
                                   <img src="<?= get_icon_file($pengumuman->foto_video); ?>" class="image-responsive image-icon" alt="image pengumuman" title="foto_video <?= $pengumuman->foto_video; ?>" width="40px"> 
                                 </a>
                                <?php endif; ?>
                              <?php endif; ?>
                           </td>
	                        
                           <td><?= _ent($pengumuman->created_at); ?></td>     
                           <td> 
 							<a href="<?= BASE_URL . 'administrator/pengumuman/view/' . $pengumuman->id_pengumuman . '?popup=show'; ?>" class="btn btn-success popup-view" data-original-title="" title=""> Detail</a>

							</td>
                           <td width="200">
                              <?php is_allowed('pengumuman_update', function() use ($pengumuman){?>
                              <a title="edit kategori" class="btn btn-warning" href="<?= site_url('administrator/pengumuman/edit/' . $pengumuman->id_pengumuman); ?>" data-table-title="Edit Kategori" class="label-default"><?= cclang('edit'); ?></a>
                              <?php }) ?>
                              <?php is_allowed('pengumuman_delete', function() use ($pengumuman){?>
                              <a  title="delete kategori" class="btn btn-danger remove-data" href="javascript:void(0);" data-href="<?= site_url('administrator/pengumuman/delete/' . $pengumuman->id_pengumuman); ?>" > Delete</a>
                               <?php }) ?>

                           </td>                        </tr>
                      <?php $no++; endforeach; ?>
                      <?php if ($pengumuman_counts == 0) :?>
                         <tr>
                           <td colspan="100">
                           Pengumuman data is not available
                           </td>
                         </tr>
                      <?php endif; ?>
                     </tbody>
                  </table>
                  </div>
               </div>
               </div>
               <!-- /.widget-user -->
               <div class="row">
                 
                  </form>                  <div class="col-md-12">
                     <div class="dataTables_paginate paging_simple_numbers pull-right" id="example2_paginate" >
                        <?= $pagination; ?>
                     </div>
                  </div>
               </div>
            </div>
            <!--/box body -->
         </div>
         <!--/box -->
      </div>
   </div>
</section>
<!-- /.content -->

<!-- Page script -->
<script>
  $(document).ready(function(){
   
    $('.remove-data').click(function(){

      var url = $(this).attr('data-href');

      swal({
          title: "<?= cclang('are_you_sure'); ?>",
          text: "<?= cclang('data_to_be_deleted_can_not_be_restored'); ?>",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "<?= cclang('yes_delete_it'); ?>",
          cancelButtonText: "<?= cclang('no_cancel_plx'); ?>",
          closeOnConfirm: true,
          closeOnCancel: true
        },
        function(isConfirm){
          if (isConfirm) {
            document.location.href = url;            
          }
        });

      return false;
    });


    $('#apply').click(function(){

      var bulk = $('#bulk');
      var serialize_bulk = $('#form_pengumuman').serialize();

      if (bulk.val() == 'delete') {
         swal({
            title: "<?= cclang('are_you_sure'); ?>",
            text: "<?= cclang('data_to_be_deleted_can_not_be_restored'); ?>",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "<?= cclang('yes_delete_it'); ?>",
            cancelButtonText: "<?= cclang('no_cancel_plx'); ?>",
            closeOnConfirm: true,
            closeOnCancel: true
          },
          function(isConfirm){
            if (isConfirm) {
               document.location.href = BASE_URL + '/administrator/pengumuman/delete?' + serialize_bulk;      
            }
          });

        return false;

      } else if(bulk.val() == '')  {
          swal({
            title: "Upss",
            text: "<?= cclang('please_choose_bulk_action_first'); ?>",
            type: "warning",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Okay!",
            closeOnConfirm: true,
            closeOnCancel: true
          });

        return false;
      }

      return false;

    });/*end appliy click*/


    //check all
    var checkAll = $('#check_all');
    var checkboxes = $('input.check');

    checkAll.on('ifChecked ifUnchecked', function(event) {   
        if (event.type == 'ifChecked') {
            checkboxes.iCheck('check');
        } else {
            checkboxes.iCheck('uncheck');
        }
    });

    checkboxes.on('ifChanged', function(event){
        if(checkboxes.filter(':checked').length == checkboxes.length) {
            checkAll.prop('checked', 'checked');
        } else {
            checkAll.removeProp('checked');
        }
        checkAll.iCheck('update');
    });

  }); /*end doc ready*/
</script>