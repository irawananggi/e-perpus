<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Pengumuman Controller
*| --------------------------------------------------------------------------
*| Pengumuman site
*|
*/
class Pengumuman extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_pengumuman');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Pengumumans
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('pengumuman_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['pengumumans'] = $this->model_pengumuman->get($filter, $field, $this->limit_page, $offset);
		$this->data['pengumuman_counts'] = $this->model_pengumuman->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/pengumuman/index/',
			'total_rows'   => $this->model_pengumuman->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Pengumuman List');
		$this->render('backend/standart/administrator/pengumuman/pengumuman_list', $this->data);
	}
	
	/**
	* Add new pengumumans
	*
	*/
	public function add()
	{
		$this->is_allowed('pengumuman_add');

		$this->template->title('Pengumuman New');
		$this->render('backend/standart/administrator/pengumuman/pengumuman_add', $this->data);
	}

	/**
	* Add New Pengumumans
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('pengumuman_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('id_warga[]', 'Id Warga', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('judul', 'Judul', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');
		$this->form_validation->set_rules('pengumuman_foto_video_name', 'Foto Video', 'trim|required');
		

		if ($this->form_validation->run()) {
			$pengumuman_foto_video_uuid = $this->input->post('pengumuman_foto_video_uuid');
			$pengumuman_foto_video_name = $this->input->post('pengumuman_foto_video_name');
		
			$save_data = [
				'id_warga' => implode(',', (array) $this->input->post('id_warga')),
				'judul' => $this->input->post('judul'),
				'deskripsi' => $this->input->post('deskripsi'),
			];

			if (!is_dir(FCPATH . '/uploads/pengumuman/')) {
				mkdir(FCPATH . '/uploads/pengumuman/');
			}

			if (!empty($pengumuman_foto_video_name)) {
				$pengumuman_foto_video_name_copy = date('YmdHis') . '-' . $pengumuman_foto_video_name;

				rename(FCPATH . 'uploads/tmp/' . $pengumuman_foto_video_uuid . '/' . $pengumuman_foto_video_name, 
						FCPATH . 'uploads/pengumuman/' . $pengumuman_foto_video_name_copy);

				if (!is_file(FCPATH . '/uploads/pengumuman/' . $pengumuman_foto_video_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['foto_video'] = $pengumuman_foto_video_name_copy;
			}
		
			
			$save_pengumuman = $this->model_pengumuman->store($save_data);
            

			if ($save_pengumuman) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_pengumuman;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/pengumuman/edit/' . $save_pengumuman, 'Edit Pengumuman'),
						anchor('administrator/pengumuman', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/pengumuman/edit/' . $save_pengumuman, 'Edit Pengumuman')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/pengumuman');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/pengumuman');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Pengumumans
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('pengumuman_update');

		$this->data['pengumuman'] = $this->model_pengumuman->find($id);

		$this->template->title('Pengumuman Update');
		$this->render('backend/standart/administrator/pengumuman/pengumuman_update', $this->data);
	}

	/**
	* Update Pengumumans
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('pengumuman_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('id_warga[]', 'Id Warga', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('judul', 'Judul', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');
		$this->form_validation->set_rules('pengumuman_foto_video_name', 'Foto Video', 'trim|required');
		
		if ($this->form_validation->run()) {
			$pengumuman_foto_video_uuid = $this->input->post('pengumuman_foto_video_uuid');
			$pengumuman_foto_video_name = $this->input->post('pengumuman_foto_video_name');
		
			$save_data = [
				'id_warga' => implode(',', (array) $this->input->post('id_warga')),
				'judul' => $this->input->post('judul'),
				'deskripsi' => $this->input->post('deskripsi'),
			];

			if (!is_dir(FCPATH . '/uploads/pengumuman/')) {
				mkdir(FCPATH . '/uploads/pengumuman/');
			}

			if (!empty($pengumuman_foto_video_uuid)) {
				$pengumuman_foto_video_name_copy = date('YmdHis') . '-' . $pengumuman_foto_video_name;

				rename(FCPATH . 'uploads/tmp/' . $pengumuman_foto_video_uuid . '/' . $pengumuman_foto_video_name, 
						FCPATH . 'uploads/pengumuman/' . $pengumuman_foto_video_name_copy);

				if (!is_file(FCPATH . '/uploads/pengumuman/' . $pengumuman_foto_video_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['foto_video'] = $pengumuman_foto_video_name_copy;
			}
		
			
			$save_pengumuman = $this->model_pengumuman->change($id, $save_data);

			if ($save_pengumuman) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/pengumuman', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/pengumuman');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/pengumuman');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Pengumumans
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('pengumuman_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'pengumuman'), 'success');
        } else {
            set_message(cclang('error_delete', 'pengumuman'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Pengumumans
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('pengumuman_view');

		$this->data['pengumuman'] = $this->model_pengumuman->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Pengumuman Detail');
		$this->render('backend/standart/administrator/pengumuman/pengumuman_view', $this->data);
	}
	
	/**
	* delete Pengumumans
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$pengumuman = $this->model_pengumuman->find($id);

		if (!empty($pengumuman->foto_video)) {
			$path = FCPATH . '/uploads/pengumuman/' . $pengumuman->foto_video;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		
		
		return $this->model_pengumuman->remove($id);
	}
	
	/**
	* Upload Image Pengumuman	* 
	* @return JSON
	*/
	public function upload_foto_video_file()
	{
		if (!$this->is_allowed('pengumuman_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'pengumuman',
		]);
	}

	/**
	* Delete Image Pengumuman	* 
	* @return JSON
	*/
	public function delete_foto_video_file($uuid)
	{
		if (!$this->is_allowed('pengumuman_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		echo $this->delete_file([
            'uuid'              => $uuid, 
            'delete_by'         => $this->input->get('by'), 
            'field_name'        => 'foto_video', 
            'upload_path_tmp'   => './uploads/tmp/',
            'table_name'        => 'pengumuman',
            'primary_key'       => 'id_pengumuman',
            'upload_path'       => 'uploads/pengumuman/'
        ]);
	}

	/**
	* Get Image Pengumuman	* 
	* @return JSON
	*/
	public function get_foto_video_file($id)
	{
		if (!$this->is_allowed('pengumuman_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
				]);
			exit;
		}

		$pengumuman = $this->model_pengumuman->find($id);

		echo $this->get_file([
            'uuid'              => $id, 
            'delete_by'         => 'id', 
            'field_name'        => 'foto_video', 
            'table_name'        => 'pengumuman',
            'primary_key'       => 'id_pengumuman',
            'upload_path'       => 'uploads/pengumuman/',
            'delete_endpoint'   => 'administrator/pengumuman/delete_foto_video_file'
        ]);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('pengumuman_export');

		$this->model_pengumuman->export('pengumuman', 'pengumuman');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('pengumuman_export');

		$this->model_pengumuman->pdf('pengumuman', 'pengumuman');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('pengumuman_export');

		$table = $title = 'pengumuman';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_pengumuman->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file pengumuman.php */
/* Location: ./application/controllers/administrator/Pengumuman.php */