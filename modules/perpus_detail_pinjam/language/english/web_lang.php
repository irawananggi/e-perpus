<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['perpus_detail_pinjam'] = 'Perpus Detail Pinjam';
$lang['perpus_detail_pinjam_id'] = 'Perpus Detail Pinjam Id';
$lang['perpus_detail_pinjam_pinjamId'] = 'Transaksi Id';
$lang['perpus_detail_pinjam_bukuId'] = 'Buku';
$lang['perpus_detail_pinjam_no_buku'] = 'No Buku';
$lang['perpus_detail_pinjam_flag'] = 'Flag';
