<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Perpus Detail Pinjam Controller
*| --------------------------------------------------------------------------
*| Perpus Detail Pinjam site
*|
*/
class Perpus_detail_pinjam extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_perpus_detail_pinjam');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Perpus Detail Pinjams
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('perpus_detail_pinjam_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');
		

        if($this->input->get('id') != NULL){
        	$this->data['perpus_detail_pinjams'] = $this->model_perpus_detail_pinjam->get($filter, $field, $this->limit_page, $offset);
        }else{
        	$this->data['perpus_detail_pinjams'] = '';
        }
		
		$this->data['perpus_detail_pinjam_counts'] = $this->model_perpus_detail_pinjam->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/perpus_detail_pinjam/index/',
			'total_rows'   => $this->model_perpus_detail_pinjam->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Perpus Detail Pinjam List');
		$this->render('backend/standart/administrator/perpus_detail_pinjam/perpus_detail_pinjam_list', $this->data);
	}
	
	/**
	* Add new perpus_detail_pinjams
	*
	*/
	public function add()
	{
		$this->is_allowed('perpus_detail_pinjam_add');

		$this->template->title('Perpus Detail Pinjam New');
		$this->render('backend/standart/administrator/perpus_detail_pinjam/perpus_detail_pinjam_add', $this->data);
	}

	/**
	* Add New Perpus Detail Pinjams
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('perpus_detail_pinjam_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		$this->form_validation->set_rules('perpus_detail_pinjam_bukuId', 'Buku', 'trim|required|max_length[15]');
		$this->form_validation->set_rules('perpus_detail_pinjam_no_buku', 'No Buku', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('perpus_detail_pinjam_flag', 'Flag', 'trim|required|max_length[1]');
		
		//cek stok buku
		
		$getdetailBuku = $this->mymodel->withquery("select * from perpus_detail_buku where perpus_detail_buku_id='".$this->input->post('perpus_detail_pinjam_no_buku')."' AND perpus_detail_buku_status=2","row");
		

		if ($this->form_validation->run()) {
			if (!empty($getdetailBuku)) {
				$save_data = [
					'perpus_detail_pinjam_pinjamId' => $this->input->post('id_transkasi_pinjam'),
					'perpus_detail_pinjam_bukuId' => $this->input->post('perpus_detail_pinjam_bukuId'),
					'perpus_detail_pinjam_no_buku' => $this->input->post('perpus_detail_pinjam_no_buku'),
					'perpus_detail_pinjam_flag' => $this->input->post('perpus_detail_pinjam_flag'),
				];

				
				$save_perpus_detail_pinjam = $this->model_perpus_detail_pinjam->store($save_data);
	            

				if ($save_perpus_detail_pinjam) {
					$datas = array(
					  "perpus_detail_buku_status" =>1
					);
					$ss = $this->mymodel->update('perpus_detail_buku',$datas,"perpus_detail_buku_id=",$this->input->post('perpus_detail_pinjam_no_buku'));

					if ($this->input->post('save_type') == 'stay') {
						$this->data['success'] = true;
						$this->data['id'] 	   = $save_perpus_detail_pinjam;
						$this->data['message'] = cclang('success_save_data_stay', [
							anchor('administrator/perpus_detail_pinjam/edit/' . $save_perpus_detail_pinjam, 'Edit Perpus Detail Pinjam'),
							anchor('administrator/perpus_detail_pinjam', ' Go back to list')
						]);
					} else {
						set_message(
							cclang('success_save_data_redirect', [
							anchor('administrator/perpus_detail_pinjam/edit/' . $save_perpus_detail_pinjam, 'Edit Perpus Detail Pinjam')
						]), 'success');

	            		$this->data['success'] = true;
						$this->data['redirect'] = base_url('administrator/perpus_detail_pinjam?id='.$this->input->post('id_transkasi_pinjam'));
					}
				} else {
					if ($this->input->post('save_type') == 'stay') {
						$this->data['success'] = false;
						$this->data['message'] = cclang('data_not_change');
					} else {
	            		$this->data['success'] = false;
	            		$this->data['message'] = cclang('data_not_change');
						$this->data['redirect'] = base_url('administrator/perpus_detail_pinjam?id='.$this->input->post('id_transkasi_pinjam'));
					}
				}

			} else {
				$this->data['success'] = false;
				$this->data['message'] = 'Buku Tidak Tersedia';
				$this->data['errors'] = $this->form_validation->error_array();
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Perpus Detail Pinjams
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('perpus_detail_pinjam_update');

		$this->data['perpus_detail_pinjam'] = $this->model_perpus_detail_pinjam->find($id);

		$this->template->title('Perpus Detail Pinjam Update');
		$this->render('backend/standart/administrator/perpus_detail_pinjam/perpus_detail_pinjam_update', $this->data);
	}

	/**
	* Update Perpus Detail Pinjams
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('perpus_detail_pinjam_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('perpus_detail_pinjam_bukuId', 'Buku', 'trim|required|max_length[15]');
		$this->form_validation->set_rules('perpus_detail_pinjam_no_buku', 'No Buku', 'trim|required|max_length[4]');
		$this->form_validation->set_rules('perpus_detail_pinjam_flag', 'Flag', 'trim|required|max_length[1]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'perpus_detail_pinjam_bukuId' => $this->input->post('perpus_detail_pinjam_bukuId'),
				'perpus_detail_pinjam_no_buku' => $this->input->post('perpus_detail_pinjam_no_buku'),
				'perpus_detail_pinjam_flag' => $this->input->post('perpus_detail_pinjam_flag'),
			];

			
			$save_perpus_detail_pinjam = $this->model_perpus_detail_pinjam->change($id, $save_data);

			if ($save_perpus_detail_pinjam) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/perpus_detail_pinjam', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/perpus_detail_pinjam?id='.$this->input->post('id_transkasi_pinjam'));
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/perpus_detail_pinjam?id='.$this->input->post('id_transkasi_pinjam'));
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Perpus Detail Pinjams
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('perpus_detail_pinjam_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'perpus_detail_pinjam'), 'success');
        } else {
            set_message(cclang('error_delete', 'perpus_detail_pinjam'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Perpus Detail Pinjams
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('perpus_detail_pinjam_view');

		$this->data['perpus_detail_pinjam'] = $this->model_perpus_detail_pinjam->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Perpus Detail Pinjam Detail');
		$this->render('backend/standart/administrator/perpus_detail_pinjam/perpus_detail_pinjam_view', $this->data);
	}
	
	/**
	* delete Perpus Detail Pinjams
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$perpus_detail_pinjam = $this->model_perpus_detail_pinjam->find($id);

		
		
		return $this->model_perpus_detail_pinjam->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('perpus_detail_pinjam_export');

		$this->model_perpus_detail_pinjam->export('perpus_detail_pinjam', 'perpus_detail_pinjam');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('perpus_detail_pinjam_export');

		$this->model_perpus_detail_pinjam->pdf('perpus_detail_pinjam', 'perpus_detail_pinjam');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('perpus_detail_pinjam_export');

		$table = $title = 'perpus_detail_pinjam';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_perpus_detail_pinjam->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file perpus_detail_pinjam.php */
/* Location: ./application/controllers/administrator/Perpus Detail Pinjam.php */