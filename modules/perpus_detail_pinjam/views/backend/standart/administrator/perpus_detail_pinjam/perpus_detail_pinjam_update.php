

<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript">
    function domo(){
     
       // Binding keys
       $('*').bind('keydown', 'Ctrl+s', function assets() {
          $('#btn_save').trigger('click');
           return false;
       });
    
       $('*').bind('keydown', 'Ctrl+x', function assets() {
          $('#btn_cancel').trigger('click');
           return false;
       });
    
      $('*').bind('keydown', 'Ctrl+d', function assets() {
          $('.btn_save_back').trigger('click');
           return false;
       });
        
    }
    
    jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Perpus Detail Pinjam        <small>Edit Perpus Detail Pinjam</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class=""><a  href="<?= site_url('administrator/perpus_detail_pinjam'); ?>">Perpus Detail Pinjam</a></li>
        <li class="active">Edit</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row" >
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Perpus Detail Pinjam</h3>
                            <h5 class="widget-user-desc">Edit Perpus Detail Pinjam</h5>
                            <hr>
                        </div>
                        <?= form_open(base_url('administrator/perpus_detail_pinjam/edit_save/'.$this->uri->segment(4)), [
                            'name'    => 'form_perpus_detail_pinjam', 
                            'class'   => 'form-horizontal form-step', 
                            'id'      => 'form_perpus_detail_pinjam', 
                            'method'  => 'POST'
                            ]); ?>
                         
                                                               
                                <input type="hidden" class="form-control" name="id_transkasi_pinjam" id="id_transkasi_pinjam" placeholder="Flag" value="<?php echo $this->input->get('id_trasaksi_pinjam'); ?>">       


                                                 
                                                <div class="form-group ">
                            <label for="perpus_detail_pinjam_bukuId" class="col-sm-2 control-label">Buku 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select-deselect" name="perpus_detail_pinjam_bukuId" id="perpus_detail_pinjam_bukuId" data-placeholder="Select Buku" >
                                    <option value=""></option>
                                    <?php foreach (db_get_all_data('perpus_buku') as $row): ?>
                                    <option <?=  $row->perpus_buku_id ==  $perpus_detail_pinjam->perpus_detail_pinjam_bukuId ? 'selected' : ''; ?> value="<?= $row->perpus_buku_id ?>"><?= $row->perpus_buku_judul; ?></option>
                                    <?php endforeach; ?>  
                                </select>
                                <small class="info help-block">
                                <b>Input Perpus Detail Pinjam BukuId</b> Max Length : 15.</small>
                            </div>
                        </div>


                                                 
                                                <div class="form-group ">
                            <label for="perpus_detail_pinjam_no_buku" class="col-sm-2 control-label">No Buku 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select-deselect" name="perpus_detail_pinjam_no_buku" id="perpus_detail_pinjam_no_buku" data-placeholder="Select No Buku" >
                                    <option value=""></option>
                                    <?php foreach (db_get_all_data('perpus_detail_buku') as $row): ?>
                                    <option <?=  $row->perpus_detail_buku_id ==  $perpus_detail_pinjam->perpus_detail_pinjam_no_buku ? 'selected' : ''; ?> value="<?= $row->perpus_detail_buku_id ?>"><?= $row->perpus_detail_buku_no; ?></option>
                                    <?php endforeach; ?>  
                                </select>
                                <small class="info help-block">
                                <b>Input Perpus Detail Pinjam No Buku</b> Max Length : 4.</small>
                            </div>
                        </div>


                                                 
                                                <div class="form-group ">
                            <label for="perpus_detail_pinjam_flag" class="col-sm-2 control-label">Flag 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="number" class="form-control" name="perpus_detail_pinjam_flag" id="perpus_detail_pinjam_flag" placeholder="Flag" value="<?= set_value('perpus_detail_pinjam_flag', $perpus_detail_pinjam->perpus_detail_pinjam_flag); ?>">
                                <small class="info help-block">
                                <b>Input Perpus Detail Pinjam Flag</b> Max Length : 1.</small>
                            </div>
                        </div>
                                                
                                                 <div class="message"></div>
                                                <div class="row-fluid col-md-7 container-button-bottom">
                            <button class="btn btn-flat btn-primary btn_save btn_action" id="btn_save" data-stype='stay' title="<?= cclang('save_button'); ?> (Ctrl+s)">
                            <i class="fa fa-save" ></i> <?= cclang('save_button'); ?>
                            </button>
                            <a class="btn btn-flat btn-info btn_save btn_action btn_save_back" id="btn_save" data-stype='back' title="<?= cclang('save_and_go_the_list_button'); ?> (Ctrl+d)">
                            <i class="ion ion-ios-list-outline" ></i> <?= cclang('save_and_go_the_list_button'); ?>
                            </a>
                            <a class="btn btn-flat btn-default btn_action" id="btn_cancel" title="<?= cclang('cancel_button'); ?> (Ctrl+x)">
                            <i class="fa fa-undo" ></i> <?= cclang('cancel_button'); ?>
                            </a>
                            <span class="loading loading-hide">
                            <img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg"> 
                            <i><?= cclang('loading_saving_data'); ?></i>
                            </span>
                        </div>
                                                 <?= form_close(); ?>
                    </div>
                </div>
                <!--/box body -->
            </div>
            <!--/box -->
        </div>
    </div>
</section>
<!-- /.content -->
<!-- Page script -->
<script>
    $(document).ready(function(){
       
      
             
      $('#btn_cancel').click(function(){
        swal({
            title: "Are you sure?",
            text: "the data that you have created will be in the exhaust!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: true
          },
          function(isConfirm){
            if (isConfirm) {
              window.location.href = BASE_URL + 'administrator/perpus_detail_pinjam';
            }
          });
    
        return false;
      }); /*end btn cancel*/
    
      $('.btn_save').click(function(){
        $('.message').fadeOut();
            
        var form_perpus_detail_pinjam = $('#form_perpus_detail_pinjam');
        var data_post = form_perpus_detail_pinjam.serializeArray();
        var save_type = $(this).attr('data-stype');
        data_post.push({name: 'save_type', value: save_type});
    
        $('.loading').show();
    
        $.ajax({
          url: form_perpus_detail_pinjam.attr('action'),
          type: 'POST',
          dataType: 'json',
          data: data_post,
        })
        .done(function(res) {
          $('form').find('.form-group').removeClass('has-error');
          $('form').find('.error-input').remove();
          $('.steps li').removeClass('error');
          if(res.success) {
            var id = $('#perpus_detail_pinjam_image_galery').find('li').attr('qq-file-id');
            if (save_type == 'back') {
              window.location.href = res.redirect;
              return;
            }
    
            $('.message').printMessage({message : res.message});
            $('.message').fadeIn();
            $('.data_file_uuid').val('');
    
          } else {
            if (res.errors) {
               parseErrorField(res.errors);
            }
            $('.message').printMessage({message : res.message, type : 'warning'});
          }
    
        })
        .fail(function() {
          $('.message').printMessage({message : 'Error save data', type : 'warning'});
        })
        .always(function() {
          $('.loading').hide();
          $('html, body').animate({ scrollTop: $(document).height() }, 2000);
        });
    
        return false;
      }); /*end btn save*/
      
       
       
       

      async function chain(){
      }
       
      chain();


    
    
    }); /*end doc ready*/
</script>