<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_perpus_detail_pinjam extends MY_Model {

    private $primary_key    = 'perpus_detail_pinjam_id';
    private $table_name     = 'perpus_detail_pinjam';
    private $field_search   = ['perpus_detail_pinjam_pinjamId', 'perpus_detail_pinjam_bukuId', 'perpus_detail_pinjam_no_buku', 'perpus_detail_pinjam_flag'];

    public function __construct()
    {
        $config = array(
            'primary_key'   => $this->primary_key,
            'table_name'    => $this->table_name,
            'field_search'  => $this->field_search,
         );

        parent::__construct($config);
    }

    public function count_all($q = null, $field = null)
    {
        
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "perpus_detail_pinjam.".$field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "perpus_detail_pinjam.".$field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "perpus_detail_pinjam.".$field . " LIKE '%" . $q . "%' )";
        }
        
        $id = $this->input->get('id');

        if($id != NULL){
            $where = "perpus_detail_pinjam.perpus_detail_pinjam_pinjamId ='".$id."'";

        }
        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $query = $this->db->get($this->table_name);

        return $query->num_rows();
    }

    public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [])
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "perpus_detail_pinjam.".$field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "perpus_detail_pinjam.".$field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "perpus_detail_pinjam.".$field . " LIKE '%" . $q . "%' )";
        }

        if (is_array($select_field) AND count($select_field)) {
            $this->db->select($select_field);
        }

        $id = $this->input->get('id');

        if($id != NULL){
            $where = "perpus_detail_pinjam.perpus_detail_pinjam_pinjamId ='".$id."'";

        }
        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $this->db->limit($limit, $offset);
                $this->db->order_by('perpus_detail_pinjam.'.$this->primary_key, "DESC");
                $query = $this->db->get($this->table_name);

        return $query->result();
    }

    public function join_avaiable() {
        $this->db->join('perpus_pinjam', 'perpus_pinjam.perpus_pinjam_id = perpus_detail_pinjam.perpus_detail_pinjam_pinjamId', 'LEFT');
        $this->db->join('perpus_buku', 'perpus_buku.perpus_buku_id = perpus_detail_pinjam.perpus_detail_pinjam_bukuId', 'LEFT');
        $this->db->join('perpus_detail_buku', 'perpus_detail_buku.perpus_detail_buku_id = perpus_detail_pinjam.perpus_detail_pinjam_no_buku', 'LEFT');
        
        $this->db->select('perpus_detail_pinjam.*,perpus_pinjam.perpus_pinjam_id as perpus_pinjam_perpus_pinjam_id,perpus_buku.perpus_buku_judul as perpus_buku_perpus_buku_judul,perpus_detail_buku.perpus_detail_buku_no as perpus_detail_buku_perpus_detail_buku_no');


        return $this;
    }

    public function filter_avaiable() {

        if (!$this->aauth->is_admin()) {
            }

        return $this;
    }

}

/* End of file Model_perpus_detail_pinjam.php */
/* Location: ./application/models/Model_perpus_detail_pinjam.php */