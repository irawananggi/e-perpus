<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['options'] = 'Options';
$lang['id'] = 'Id';
$lang['name'] = 'Name';
$lang['value'] = 'Value';
