<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Options Controller
*| --------------------------------------------------------------------------
*| Options site
*|
*/
class Options extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_options');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Optionss
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('options_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['optionss'] = $this->model_options->get($filter, $field, $this->limit_page, $offset);
		$this->data['options_counts'] = $this->model_options->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/options/index/',
			'total_rows'   => $this->model_options->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Options List');
		$this->render('backend/standart/administrator/options/options_list', $this->data);
	}
	
	/**
	* Add new optionss
	*
	*/
	public function add()
	{
		$this->is_allowed('options_add');

		$this->template->title('Options New');
		$this->render('backend/standart/administrator/options/options_add', $this->data);
	}

	/**
	* Add New Optionss
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('options_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('name', 'Name', 'trim|required|max_length[250]');
		$this->form_validation->set_rules('value', 'Value', 'trim|required|max_length[11]');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'name' => $this->input->post('name'),
				'value' => $this->input->post('value'),
			];

			
			$save_options = $this->model_options->store($save_data);
            

			if ($save_options) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_options;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/options/edit/' . $save_options, 'Edit Options'),
						anchor('administrator/options', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/options/edit/' . $save_options, 'Edit Options')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/options');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/options');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Optionss
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('options_update');

		$this->data['options'] = $this->model_options->find($id);

		$this->template->title('Options Update');
		$this->render('backend/standart/administrator/options/options_update', $this->data);
	}

	/**
	* Update Optionss
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('options_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('name', 'Name', 'trim|required|max_length[250]');
		$this->form_validation->set_rules('value', 'Value', 'trim|required|max_length[11]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'name' => $this->input->post('name'),
				'value' => $this->input->post('value'),
			];

			
			$save_options = $this->model_options->change($id, $save_data);

			if ($save_options) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/options', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/options/edit/'.$id);
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/options');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Optionss
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('options_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'options'), 'success');
        } else {
            set_message(cclang('error_delete', 'options'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Optionss
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('options_view');

		$this->data['options'] = $this->model_options->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Options Detail');
		$this->render('backend/standart/administrator/options/options_view', $this->data);
	}
	
	/**
	* delete Optionss
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$options = $this->model_options->find($id);

		
		
		return $this->model_options->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('options_export');

		$this->model_options->export('options', 'options');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('options_export');

		$this->model_options->pdf('options', 'options');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('options_export');

		$table = $title = 'options';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_options->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file options.php */
/* Location: ./application/controllers/administrator/Options.php */