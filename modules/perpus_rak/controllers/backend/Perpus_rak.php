<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Perpus Rak Controller
*| --------------------------------------------------------------------------
*| Perpus Rak site
*|
*/
class Perpus_rak extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_perpus_rak');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Perpus Raks
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('perpus_rak_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['perpus_raks'] = $this->model_perpus_rak->get($filter, $field, $this->limit_page, $offset);
		$this->data['perpus_rak_counts'] = $this->model_perpus_rak->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/perpus_rak/index/',
			'total_rows'   => $this->model_perpus_rak->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Perpus Rak List');
		$this->render('backend/standart/administrator/perpus_rak/perpus_rak_list', $this->data);
	}
	
	/**
	* Add new perpus_raks
	*
	*/
	public function add()
	{
		$this->is_allowed('perpus_rak_add');

		$this->template->title('Perpus Rak New');
		$this->render('backend/standart/administrator/perpus_rak/perpus_rak_add', $this->data);
	}

	/**
	* Add New Perpus Raks
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('perpus_rak_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('perpus_rak_nama', 'Nama', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('perpus_rak_kategoriId', 'Kategori', 'trim|required|max_length[3]');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'perpus_rak_nama' => $this->input->post('perpus_rak_nama'),
				'perpus_rak_kategoriId' => $this->input->post('perpus_rak_kategoriId'),
			];

			
			$save_perpus_rak = $this->model_perpus_rak->store($save_data);
            

			if ($save_perpus_rak) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_perpus_rak;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/perpus_rak/edit/' . $save_perpus_rak, 'Edit Perpus Rak'),
						anchor('administrator/perpus_rak', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/perpus_rak/edit/' . $save_perpus_rak, 'Edit Perpus Rak')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/perpus_rak');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/perpus_rak');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Perpus Raks
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('perpus_rak_update');

		$this->data['perpus_rak'] = $this->model_perpus_rak->find($id);

		$this->template->title('Perpus Rak Update');
		$this->render('backend/standart/administrator/perpus_rak/perpus_rak_update', $this->data);
	}

	/**
	* Update Perpus Raks
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('perpus_rak_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('perpus_rak_nama', 'Nama', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('perpus_rak_kategoriId', 'Kategori', 'trim|required|max_length[3]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'perpus_rak_nama' => $this->input->post('perpus_rak_nama'),
				'perpus_rak_kategoriId' => $this->input->post('perpus_rak_kategoriId'),
			];

			
			$save_perpus_rak = $this->model_perpus_rak->change($id, $save_data);

			if ($save_perpus_rak) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/perpus_rak', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/perpus_rak');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/perpus_rak');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Perpus Raks
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('perpus_rak_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'perpus_rak'), 'success');
        } else {
            set_message(cclang('error_delete', 'perpus_rak'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Perpus Raks
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('perpus_rak_view');

		$this->data['perpus_rak'] = $this->model_perpus_rak->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Perpus Rak Detail');
		$this->render('backend/standart/administrator/perpus_rak/perpus_rak_view', $this->data);
	}
	
	/**
	* delete Perpus Raks
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$perpus_rak = $this->model_perpus_rak->find($id);

		
		
		return $this->model_perpus_rak->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('perpus_rak_export');

		$this->model_perpus_rak->export('perpus_rak', 'perpus_rak');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('perpus_rak_export');

		$this->model_perpus_rak->pdf('perpus_rak', 'perpus_rak');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('perpus_rak_export');

		$table = $title = 'perpus_rak';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_perpus_rak->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file perpus_rak.php */
/* Location: ./application/controllers/administrator/Perpus Rak.php */