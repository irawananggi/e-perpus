<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_informasi extends MY_Model {

    private $primary_key    = 'id_informasi';
    private $table_name     = 'informasi';
    private $field_search   = ['judul', 'deskripsi', 'foto_video'];

    public function __construct()
    {
        $config = array(
            'primary_key'   => $this->primary_key,
            'table_name'    => $this->table_name,
            'field_search'  => $this->field_search,
         );

        parent::__construct($config);
    }

    public function count_all($q = null, $field = null)
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "informasi.".$field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "informasi.".$field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "informasi.".$field . " LIKE '%" . $q . "%' )";
        }

        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $query = $this->db->get($this->table_name);

        return $query->num_rows();
    }

    public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [])
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "informasi.".$field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "informasi.".$field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "informasi.".$field . " LIKE '%" . $q . "%' )";
        }

        if (is_array($select_field) AND count($select_field)) {
            $this->db->select($select_field);
        }
        
        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $this->db->limit($limit, $offset);
                $this->db->order_by('informasi.'.$this->primary_key, "DESC");
                $query = $this->db->get($this->table_name);

        return $query->result();
    }

    public function join_avaiable() {
        
        $this->db->select('informasi.*');


        return $this;
    }

    public function filter_avaiable() {

        if (!$this->aauth->is_admin()) {
            }

        return $this;
    }
    
    function datagrab($param) {
        #$this->db->cache_on();
        if (!empty($param['select'])) $this->db->select($param['select'],false);
        if (is_array($param['tabel'])) {    
            $n = 1;
            foreach($param['tabel'] as $tab => $on) {
    
                if ($n > 1) {
                    if (is_array($on)) $this->db->join($tab,@$on[0],@$on[1]);
                    else $this->db->join($tab,$on);
                } else { $this->db->from($tab); }
                $n++;
            }
        } else {
            $this->db->from($param['tabel']);
        }
        if (!empty($param['where'])) 
            foreach($param['where'] as $w => $an) {
    /*                if (!empty($an)) $this->db->where($w,$an);
                else $this->db->where($w,null,false);*/
    // ini bikin error kalo empty jadi di ganti null
                if (is_null($an)){
                    $this->db->where($w,null,false);
                }else {
                    $this->db->where($w,$an);
                }

            }
        
        if(!empty($param['not_in']))
        if(is_array($param['not_in'])){
            foreach($param['not_in'] as $w => $an) {
/*                if (!empty($an)) $this->db->where($w,$an);
                else $this->db->where($w,null,false);*/
// ini bikin error kalo empty jadi di ganti null
                if (is_null($an)){
                    $this->db->where_not_in($w,null,false);
                }else {
                    $this->db->where_not_in($w,$an);
                }
            }
        }else{
            $this->db->where_not_in($param['not_in'],$param['not_in_isi']);
        }
        
        if(!empty($param['in']))
        if(is_array($param['in'])){
            foreach($param['in'] as $w => $an) {
                if (is_null($an)){
                    $this->db->where_in($w,null,false);
                }else {
                    $this->db->where_in($w,$an);
                }
            }
         }else{
            $this->db->where_in($param['in'],$param['in_isi']);
         }
//ini tambahan yang nda harus pake offset
        if (!empty($param['limit']) && !empty($param['offset'])) $this->db->limit($param['limit'],$param['offset']);
        if (!empty($param['limit']) && empty($param['offset'])) $this->db->limit($param['limit']);
        if (!empty($param['order'])) $this->db->order_by($param['order']);
        if (!empty($param['search'])) {
            foreach($param['search'] as $sc => $vl) {
                $this->db->or_like($sc,$vl);
            }
        }
        if (!empty($param['group_by'])) $this->db->group_by($param['group_by']);
        return $this->db->get();
    }
function save_data($tabel, $data = null, $column = null, $id = null){
        if (is_array($tabel)) {
            if (!empty($tabel['where'])) {
            $this->db->where($tabel['where']);
            return $this->db->update($tabel['tabel'],$tabel['data']);
            } else {
                $this->db->insert($tabel['tabel'],$tabel['data']);
                return $this->db->insert_id();
            }
        } else {
            if (!empty($id)) {
                $this->db->where($column,$id);
                $this->db->update($tabel,$data);
            } else {
                $this->db->insert($tabel,$data);
                return $this->db->insert_id();  
            }       
        }
    }

    public function getbywhere($table,$where,$id="",$result="result")
      {
        if ($result=='result') {
          if ($id=="") {
            return $this->db->distinct()->where($where)->get($table)->result();
          }else {
            return $this->db->distinct()->where($where,$id)->get($table)->result();
          }

        }else {
          if ($id=="") {
            return $this->db->distinct()->where($where)->get($table)->row();
          }else {
            return $this->db->distinct()->where($where,$id)->get($table)->row();
          }
        }

      }

}

/* End of file Model_informasi.php */
/* Location: ./application/models/Model_informasi.php */