<?php
defined('BASEPATH') OR exit('No direct script access allowed');

define( 'API_ACCESS_KEY', 'AAAAxYQ88yo:APA91bH4_6wnS4fpy89nnXOa-9t4eukXbj3XyBNh1UH8si7zPgAtoJaHqkNG13dEtOvpLF7WmZaeHoNT31SUD5ug-yTiz_MER-VHOYOruB9_ujr8rqqb_SGAn0kpeP0b1uYhKBcB7vUH' );


/**
*| --------------------------------------------------------------------------
*| Informasi Controller
*| --------------------------------------------------------------------------
*| Informasi site
*|
*/
class Informasi extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_informasi');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Informasis
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('informasi_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['informasis'] = $this->model_informasi->get($filter, $field, $this->limit_page, $offset);
		$this->data['informasi_counts'] = $this->model_informasi->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/informasi/index/',
			'total_rows'   => $this->model_informasi->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Informasi List');
		$this->render('backend/standart/administrator/informasi/informasi_list', $this->data);
	}
	
	/**
	* Add new informasis
	*
	*/
	public function add()
	{
		$this->is_allowed('informasi_add');

		$this->template->title('Informasi New');
		$this->render('backend/standart/administrator/informasi/informasi_add', $this->data);
	}

	/**
	* Add New Informasis
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('informasi_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('judul', 'Judul', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');
		$this->form_validation->set_rules('informasi_foto_video_name', 'Foto Video', 'trim|required');
		

		if ($this->form_validation->run()) {
			$informasi_foto_video_uuid = $this->input->post('informasi_foto_video_uuid');
			$informasi_foto_video_name = $this->input->post('informasi_foto_video_name');
		
			$save_data = [
				'judul' => $this->input->post('judul'),
				'deskripsi' => $this->input->post('deskripsi'),
				'created_at' => date('Y-m-d H:i:s'),
			];

			if (!is_dir(FCPATH . '/uploads/informasi/')) {
				mkdir(FCPATH . '/uploads/informasi/');
			}

			if (!empty($informasi_foto_video_name)) {
				$informasi_foto_video_name_copy = date('YmdHis') . '-' . $informasi_foto_video_name;

				rename(FCPATH . 'uploads/tmp/' . $informasi_foto_video_uuid . '/' . $informasi_foto_video_name, 
						FCPATH . 'uploads/informasi/' . $informasi_foto_video_name_copy);

				if (!is_file(FCPATH . '/uploads/informasi/' . $informasi_foto_video_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['foto_video'] = $informasi_foto_video_name_copy;
			}
		
			
			$save_informasi = $this->model_informasi->store($save_data);
            

			if ($save_informasi) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_informasi;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/informasi/edit/' . $save_informasi, 'Edit Informasi'),
						anchor('administrator/informasi', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/informasi/edit/' . $save_informasi, 'Edit Informasi')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/informasi');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/informasi');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		$list_warga = $this->model_informasi->getbywhere('warga',"kode_rt !=''",null,"result");

	    foreach ($list_warga as $key => $value) {
	        if (!empty($value->id_fcm)) {
	          //send notif


	        	$Msg = array(
			      'title' => "Informasi",
			      'body' => "".$value->nama_lengkap." ada informasi baru, klik disini untuk menampilkan informasi selengkapnya",
			      "content_available" =>true,
			      "click_action"=>"FLUTTER_NOTIFICATION_CLICK",
			      "priority" => "high",
			      "android_channel_id"=>"ayowarga"
			    );
			    
			    $fcmFields = array(
			      'to' => $value->id_fcm,
			      'notification' => $Msg,
			      'data'=>array('priority'=>'high','content_available'=>true,'route' => '/dashboard','is_payment'=>false)
			    );
			    $headers = array(
			      'Authorization: key=' . API_ACCESS_KEY,
			      'Content-Type: application/json'
			    );
			    $ch = curl_init();
			    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
			    curl_setopt( $ch,CURLOPT_POST, true );
			    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
			    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
			    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
			    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fcmFields ) );
			    $result = curl_exec($ch );
			    curl_close( $ch );

			    $cek_respon = explode(',',$result);
			    $berhasil = substr($cek_respon[1],strpos($cek_respon[1],':')+1);
			    

	        }
	      }


		echo json_encode($this->data);
	}
	
		/**
	* Update view Informasis
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('informasi_update');

		$this->data['informasi'] = $this->model_informasi->find($id);

		$this->template->title('Informasi Update');
		$this->render('backend/standart/administrator/informasi/informasi_update', $this->data);
	}

	/**
	* Update Informasis
	*
	* @var $id String
	*/

	public function edit_save($id)
	{
		if (!$this->is_allowed('informasi_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('judul', 'Judul', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');
		$this->form_validation->set_rules('informasi_foto_video_name', 'Foto Video', 'trim|required');
		
		if ($this->form_validation->run()) {
			$informasi_foto_video_uuid = $this->input->post('informasi_foto_video_uuid');
			$informasi_foto_video_name = $this->input->post('informasi_foto_video_name');
		
			$save_data = [
				'judul' => $this->input->post('judul'),
				'deskripsi' => $this->input->post('deskripsi'),
				'created_at' => date('Y-m-d H:i:s'),
			];

			if (!is_dir(FCPATH . '/uploads/informasi/')) {
				mkdir(FCPATH . '/uploads/informasi/');
			}

			if (!empty($informasi_foto_video_uuid)) {
				$informasi_foto_video_name_copy = date('YmdHis') . '-' . $informasi_foto_video_name;

				rename(FCPATH . 'uploads/tmp/' . $informasi_foto_video_uuid . '/' . $informasi_foto_video_name, 
						FCPATH . 'uploads/informasi/' . $informasi_foto_video_name_copy);

				if (!is_file(FCPATH . '/uploads/informasi/' . $informasi_foto_video_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['foto_video'] = $informasi_foto_video_name_copy;
			}
		
			
			$save_informasi = $this->model_informasi->change($id, $save_data);

			if ($save_informasi) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/informasi', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/informasi');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/informasi');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}
		$list_warga = $this->model_informasi->getbywhere('warga',"kode_rt !=''",null,"result");

		foreach ($list_warga as $key => $value) {
	        if (!empty($value->id_fcm)) {
	          //send notif


	        	$Msg = array(
			      'title' => "Informasi",
			      'body' => "".$value->nama_lengkap." ada informasi baru, klik disini untuk menampilkan informasi selengkapnya",
			      "content_available" =>true,
			      "click_action"=>"FLUTTER_NOTIFICATION_CLICK",
			      "priority" => "high",
			      "android_channel_id"=>"ayowarga"
			    );
			    
			    $fcmFields = array(
			      'to' => $value->id_fcm,
			      'notification' => $Msg,
			      'data'=>array('priority'=>'high','content_available'=>true,'route' => '/dashboard','is_payment'=>false)
			    );
			    $headers = array(
			      'Authorization: key=' . API_ACCESS_KEY,
			      'Content-Type: application/json'
			    );
			    $ch = curl_init();
			    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
			    curl_setopt( $ch,CURLOPT_POST, true );
			    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
			    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
			    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
			    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fcmFields ) );
			    $result = curl_exec($ch );
			    curl_close( $ch );

			    $cek_respon = explode(',',$result);
			    $berhasil = substr($cek_respon[1],strpos($cek_respon[1],':')+1);
			    

	        }
	      }

		echo json_encode($this->data);
	}
	
	/**
	* delete Informasis
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('informasi_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'informasi'), 'success');
        } else {
            set_message(cclang('error_delete', 'informasi'), 'error');
        }

		redirect_back();
	}


	public function edit_status($id = null)
	{
		
		$this->is_allowed('informasi_list');
		$datax = $this->model_informasi->datagrab(array('tabel'=>'informasi','where'=>array('id_informasi'=>$id)))->row();

		if($datax->status == 1){

			$datas = array('status'	=> '0');
			$remove = $this->model_informasi->save_data('informasi',$datas,'id_informasi',$id);

            set_message(cclang('Data Berhasil di Nonaktifkan', 'informasi'), 'error');
			
		}else{
			$datas = array('status'	=> '1');
			$remove = $this->model_informasi->save_data('informasi',$datas,'id_informasi',$id);

            set_message(cclang('Data Berhasil di Aktifkan', 'informasi'), 'success');
			
		}

		redirect_back();
	}

		/**
	* View view Informasis
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('informasi_view');

		$this->data['informasi'] = $this->model_informasi->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Informasi Detail');
		$this->render('backend/standart/administrator/informasi/informasi_view', $this->data);
	}
	
	/**
	* delete Informasis
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$informasi = $this->model_informasi->find($id);

		if (!empty($informasi->foto_video)) {
			$path = FCPATH . '/uploads/informasi/' . $informasi->foto_video;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		
		
		return $this->model_informasi->remove($id);
	}
	
	/**
	* Upload Image Informasi	* 
	* @return JSON
	*/
	public function upload_foto_video_file()
	{
		if (!$this->is_allowed('informasi_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'informasi',
		]);
	}

	/**
	* Delete Image Informasi	* 
	* @return JSON
	*/
	public function delete_foto_video_file($uuid)
	{
		if (!$this->is_allowed('informasi_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		echo $this->delete_file([
            'uuid'              => $uuid, 
            'delete_by'         => $this->input->get('by'), 
            'field_name'        => 'foto_video', 
            'upload_path_tmp'   => './uploads/tmp/',
            'table_name'        => 'informasi',
            'primary_key'       => 'id_informasi',
            'upload_path'       => 'uploads/informasi/'
        ]);
	}

	/**
	* Get Image Informasi	* 
	* @return JSON
	*/
	public function get_foto_video_file($id)
	{
		if (!$this->is_allowed('informasi_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
				]);
			exit;
		}

		$informasi = $this->model_informasi->find($id);

		echo $this->get_file([
            'uuid'              => $id, 
            'delete_by'         => 'id', 
            'field_name'        => 'foto_video', 
            'table_name'        => 'informasi',
            'primary_key'       => 'id_informasi',
            'upload_path'       => 'uploads/informasi/',
            'delete_endpoint'   => 'administrator/informasi/delete_foto_video_file'
        ]);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('informasi_export');

		$this->model_informasi->export('informasi', 'informasi');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('informasi_export');

		$this->model_informasi->pdf('informasi', 'informasi');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('informasi_export');

		$table = $title = 'informasi';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_informasi->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
  public function send_notif($title,$desc,$id_fcm,$data)
  {

    $Msg = array(
      'body' => $desc,
      'title' => $title,
      "content_available" => true,
      "click_action"=>"FLUTTER_NOTIFICATION_CLICK",
      "priority" => "high",
      "android_channel_id"=>"ayowarga"
    );
    $fcmFields = array(
      'to' => $id_fcm,
      'notification' => $Msg,
       'data'=>$data
    );
    $headers = array(
      'Authorization: key=' . API_ACCESS_KEY,
      'Content-Type: application/json'
    );
    $ch = curl_init();
    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
    curl_setopt( $ch,CURLOPT_POST, true );
    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fcmFields ) );
    $result = curl_exec($ch );
    curl_close( $ch );

    $cek_respon = explode(',',$result);
    $berhasil = substr($cek_respon[1],strpos($cek_respon[1],':')+1);
    
  }
}


/* End of file informasi.php */
/* Location: ./application/controllers/administrator/Informasi.php */