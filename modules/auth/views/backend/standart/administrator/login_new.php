<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login</title>
    <link rel="shortcut icon" href="./images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="./images/favicon.ico" type="image/x-icon">
    
    
  <script type="module" crossorigin src="<?= theme_asset(); ?>/laundry/js/main-9b991d96.js"></script>
  <link href="<?= theme_asset(); ?>/laundry/css/main-95503ed3.css" rel="stylesheet">
  <style type="text/css">
    .title-h1{
      color:var(--bs-body-bg);
      text-align: center;
      font-size: 45px;
    }
    .title-h5{
      color:var(--bs-body-bg);
      text-align: center;
      font-size: 12px;
    }
    .title{
      color:var(--bs-gray-600);
    }
    .title-footer{
      font-size: 12px;
      color:var(--bs-gray-600);
      text-align: center !important;
    }
    .xx{
      margin:50px auto;
      background: var(--bs-body-color);
      border-radius: 8px;
      box-shadow: 1px 2px 8px rgba(0, 0, 0, 0.65);
      padding:16px;
    }
  </style>
</head>

<body>
    
<!-- <header class="header">
    <div class="container-lg">
        <a href="index.html" class="logo me-4">
          
        </a>
        <div class="navs">
            <ul class="nav nav-desktop">
                <li class="nav-item">
                    <a href="blog.html" class="nav-link">Transaksi</a>
                </li>
                <li class="nav-item">
                    <a href="tournament.html" class="nav-link">Kasir</a>
                </li>
            </ul>
            <ul class="nav align-items-center ms-auto">
                <li class="nav-item">
                    <a href="sign-up.html" class="btn btn-gradient-primary">Login</a>
                </li>
            </ul>
            <button class="navbar-toggler ms-2" type="button" data-bs-toggle="collapse"
                data-bs-target="#menu_mobile">
                <i class="ico ico-menu"></i>
            </button>

        </div>
    </div>
</header> -->

     <p class="login-box-msg"><?= cclang('sign_to_start_your_session'); ?></p>
    <?php if(isset($error) AND !empty($error)): ?>
         <div class="callout callout-error"  style="color:#C82626">
              <h4><?= cclang('error'); ?>!</h4>
              <p><?= $error; ?></p>
            </div>
    <?php endif; ?>
    <?php
    $message = $this->session->flashdata('f_message'); 
    $type = $this->session->flashdata('f_type'); 
    if ($message):
    ?>
<div class="callout callout-<?= $type; ?>"  style="color:#C82626">
        <p><?= $message; ?></p>
      </div>
    <?php endif; ?>
<div class="page-wrapper">
    <div class="auth-container">
        <div class="container-lg">
            <div class="row">
              <div class="col-lg-12">
                <div class="col-lg-4 mb-4 xx">
                  <div class="col-lg-12 mb-4">

                <div class="col-lg-4 mb-4" style="margin:0px auto;padding:10px;">
                    <img src="<?= BASE_ASSET; ?>img/icon_small.png" style="width:100%;text-align: center">
                  </div>
                  </div>
                    <h1 class="title-h1 mb-3 mt-4">
                        E-Laundry <br>
                    </h1>
                    <div class="col-lg-12 mb-4">
                        <form action="">
                            <div class="mb-4">
                                <label class="mb-2 title">Username</label>
                                <input type="text" class="form-control" placeholder="Enter your Username">
                            </div>
                            <div class="mb-4">
                                <label class="mb-2 title">Password</label>
                                <input type="password" class="form-control" placeholder="Password">
                            </div>
                            <button type="submit" class="btn btn-gradient-primary w-100">Log In</button>
                            <br>
                            <br>
                            <div class="row">
                            <diw class="col-lg-12 title-footer">Created by LAUNDRY | All Right Reserved!</diw>
                          </div>
                        </form>
                    </div>
                  </div>
            </div>
        </div>
    </div>
</div>
    
    
    
</body>

</html>