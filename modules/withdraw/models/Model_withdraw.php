<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_withdraw extends MY_Model {

    private $primary_key    = 'id';
    private $table_name     = 'withdraw';
    public $field_search   = ['id_warga', 'id_bank','kode_rt', 'nama_pemilik','no_rekening', 'saldo_awal', 'jumlah', 'saldo_akhir'];

    public function __construct()
    {
        $config = array(
            'primary_key'   => $this->primary_key,
            'table_name'    => $this->table_name,
            'field_search'  => $this->field_search,
         );

        parent::__construct($config);
    }

    public function count_all($q = null, $field = null, $cf=NULL)
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "withdraw.".$field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "withdraw.".$field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }
            $where .= "OR " . "warga.nama_lengkap LIKE '%" . $q . "%' ";
            $where .= "OR " . "withdraw.kode_rt LIKE '%" . $q . "%' ";
            $where = '('.$where.')';
        } else {
            $where .= "(" . "withdraw.".$field . " LIKE '%" . $q . "%' )";
        }

        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        if ($cf['kode_rt'] != null) $this->db->where("withdraw.kode_rt = " . $cf['kode_rt']);
        $query = $this->db->get($this->table_name);

        return $query->num_rows();
    }

    public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [], $cf = [])
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "withdraw.".$field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "withdraw.".$field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where .= "OR " . "warga.nama_lengkap LIKE '%" . $q . "%' ";
            $where .= "OR " . "withdraw.kode_rt LIKE '%" . $q . "%' ";
            $where = '('.$where.')';
        } else {
            $where .= "(" . "withdraw.".$field . " LIKE '%" . $q . "%' )";
        }

        if (is_array($select_field) AND count($select_field)) {
            $this->db->select($select_field);
        }
        
        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        if ($cf['kode_rt'] != null) $this->db->where("withdraw.kode_rt = " . $cf['kode_rt']);
        $this->db->limit($limit, $offset);
        $this->db->order_by('id DESC');
        
        //$this->sortable();
        
        $query = $this->db->get($this->table_name);

        return $query->result();
    }

    public function join_avaiable() {
        $this->db->join('warga', 'warga.id_warga = withdraw.id_warga', 'LEFT');
        $this->db->join('bank', 'bank.id_bank = withdraw.id_bank', 'LEFT');
        
        $this->db->select('withdraw.*,warga.nama_lengkap as nama_lengkap,bank.nama_bank as bank_name');


        return $this;
    }

    public function filter_avaiable() {

        if (!$this->aauth->is_admin()) {
            }

        return $this;
    }
    
    function datagrabs($param) {

        if (!empty($param['count'])) {

            if (!empty($param['search'])) {

                    if (!empty($param['select'])) {
                        $this->db->select($param['select'],false);
                    }else{
                        $this->db->select('*',false);
                    }

            }else{
                $this->db->select($param['count'],false);
            }

        }elseif(!empty($param['select'])) {
            $this->db->select($param['select'],false);
         } 



        
        if (is_array($param['tabel'])) {    
            $n = 1;
            foreach($param['tabel'] as $tab => $on) {
    
                if ($n > 1) {
                    if (is_array($on)) $this->db->join($tab,$on[0],$on[1]);
                    else $this->db->join($tab,$on);
                } else { $this->db->from($tab); }
                $n+=1;
            }
        } else {
        $this->db->from($param['tabel']);
        }

        if (!empty($param['where'])) {
            foreach($param['where'] as $w => $an) {
// ini bikin error kalo empty jadi di ganti null
                if (is_null($an)){
                    $this->db->where($w,null,false);
                }else {
                    $this->db->where($w,$an);
                }
            }
        }
        if (!empty($param['order'])) $this->db->order_by($param['order']);
        if (!empty($param['group_by'])) $this->db->group_by($param['group_by']);
//        $this->output->enable_profiler(TRUE);
        if (!empty($param['search'])) {
            $q= $this->db->_compile_select(); 
            
            $this->db->_reset_select();
            if (!empty($param['count'])) {
                 $this->db->select($param['count']." from ($q) t",false);
            }else{
                $this->db->select("* from ($q) t",false);   
            }
            

            foreach($param['search'] as $sc => $vl) {
                $this->db->or_like('t.'.$sc,$vl);
            }
        }        
        if (!empty($param['limit'])){
            if(!empty($param['offset'])){
                $this->db->limit($param['limit'],$param['offset']);
            }else{
                $this->db->limit($param['limit']);
            }
        }
        return $this->db->get();
    }

    
    function save_data($tabel, $data = null, $column = null, $id = null){
        if (is_array($tabel)) {
            if (!empty($tabel['where'])) {
            $this->db->where($tabel['where']);
            return $this->db->update($tabel['tabel'],$tabel['data']);
            } else {
                $this->db->insert($tabel['tabel'],$tabel['data']);
                return $this->db->insert_id();
            }
        } else {
            if (!empty($id)) {
                $this->db->where($column,$id);
                $this->db->update($tabel,$data);
            } else {
                $this->db->insert($tabel,$data);
                return $this->db->insert_id();  
            }       
        }
    }

    function simpan_data($param) {
        if (!empty($param['where'])) {
            $this->db->where($param['where']);
            return $this->db->update($param['tabel'],$param['data']);
        } else {
            $this->db->insert($param['tabel'],$param['data']);
            return $this->db->insert_id();
        }
        //echo $this->db->last_query();
    }
    
    function delete_data($tabel, $column = null, $id = null) {
        if (is_array($tabel)) {
            foreach($tabel['where'] as $w => $an) {
                if (is_null($an)) $this->db->where($w,null,false);
                else $this->db->where($w,$an);
            }
            return $this->db->delete($tabel['tabel']);
        } else {
            if (!empty($column)) { 
                if (is_array($column)) $this->db->where($column);
                else $this->db->where($column,$id);
            }
            return $this->db->delete($tabel);
        }
    }
}

/* End of file Model_withdraw.php */
/* Location: ./application/models/Model_withdraw.php */