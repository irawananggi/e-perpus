<?php
defined('BASEPATH') OR exit('No direct script access allowed');

define( 'API_ACCESS_KEY', 'AAAAxYQ88yo:APA91bH4_6wnS4fpy89nnXOa-9t4eukXbj3XyBNh1UH8si7zPgAtoJaHqkNG13dEtOvpLF7WmZaeHoNT31SUD5ug-yTiz_MER-VHOYOruB9_ujr8rqqb_SGAn0kpeP0b1uYhKBcB7vUH' );

/**
*| --------------------------------------------------------------------------
*| Withdraw Controller
*| --------------------------------------------------------------------------
*| Withdraw site
*|
*/
class Withdraw extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_withdraw');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Withdraws
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('withdraw_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');
		$cf['kode_rt'] = $this->input->get('ti');

		$this->data['withdraws'] = $this->model_withdraw->get($filter, $field, $this->limit_page, $offset, [], $cf);
		$this->data['withdraw_counts'] = $this->model_withdraw->count_all($filter, $field, $cf);

		$config = [
			'base_url'     => 'administrator/withdraw/index/',
			'total_rows'   => $this->model_withdraw->count_all($filter, $field, $cf),
			//'total_rows'   => $this->data['withdraw_counts'],
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Withdraw List');
		$this->render('backend/standart/administrator/withdraw/withdraw_list', $this->data);
	}
	
	/**
	* Add new withdraws
	*
	*/
	public function add()
	{
		$this->is_allowed('withdraw_add');

		$this->template->title('Withdraw New');
		$this->render('backend/standart/administrator/withdraw/withdraw_add', $this->data);
	}

	/**
	* Add New Withdraws
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('withdraw_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('user_id', 'User Id', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('id_bank', 'Bank Id', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('no_rekening', 'No Rekening', 'trim|required|max_length[11]');
		//$this->form_validation->set_rules('jenis_toko_id', 'Jenis Toko Id', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('saldo_awal', 'Saldo Awal', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('jumlah', 'Total Withdraw', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('saldo_akhir', 'Saldo Akhir', 'trim|required|max_length[11]');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'user_id' => $this->input->post('user_id'),
				'id_bank' => $this->input->post('id_bank'),
				'no_rekening' => $this->input->post('no_rekening'),
				'saldo_awal' => $this->input->post('saldo_awal'),
				'jumlah' => $this->input->post('jumlah'),
				'saldo_akhir' => $this->input->post('saldo_akhir'),
			];

			
			$save_withdraw = $this->model_withdraw->store($save_data);
            

			if ($save_withdraw) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_withdraw;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/withdraw/edit/' . $save_withdraw, 'Edit Withdraw'),
						anchor('administrator/withdraw', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/withdraw/edit/' . $save_withdraw, 'Edit Withdraw')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/withdraw');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/withdraw');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Withdraws
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('withdraw_update');

		$this->data['withdraw'] = $this->model_withdraw->find($id);

		$this->template->title('Withdraw Update');
		$this->render('backend/standart/administrator/withdraw/withdraw_update', $this->data);
	}

	/**
	* Update Withdraws
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('withdraw_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('user_id', 'User Id', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('id_bank', 'Bank Id', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('no_rekening', 'No Rekening', 'trim|required|max_length[11]');
		//$this->form_validation->set_rules('jenis_toko_id', 'Jenis Toko Id', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('saldo_awal', 'Saldo Awal', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('jumlah', 'Total Withdraw', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('saldo_akhir', 'Saldo Akhir', 'trim|required|max_length[11]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'user_id' => $this->input->post('user_id'),
				'id_bank' => $this->input->post('id_bank'),
				'no_rekening' => $this->input->post('no_rekening'),
				//'jenis_toko_id' => $this->input->post('jenis_toko_id'),
				'saldo_awal' => $this->input->post('saldo_awal'),
				'jumlah' => $this->input->post('jumlah'),
				'saldo_akhir' => $this->input->post('saldo_akhir'),
			];

			
			$save_withdraw = $this->model_withdraw->change($id, $save_data);

			if ($save_withdraw) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/withdraw', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/withdraw');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/withdraw');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Withdraws
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('withdraw_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'withdraw'), 'success');
        } else {
            set_message(cclang('error_delete', 'withdraw'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Withdraws
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('withdraw_view');

		$this->data['withdraw'] = $this->model_withdraw->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Withdraw Detail');
		$this->render('backend/standart/administrator/withdraw/withdraw_view', $this->data);
	}
	
	/**
	* delete Withdraws
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$withdraw = $this->model_withdraw->find($id);

		
		
		return $this->model_withdraw->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('withdraw_export');

		$this->model_withdraw->export(
			'withdraw', 
			'withdraw',
			$this->model_withdraw->field_search
		);
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('withdraw_export');

		$this->model_withdraw->pdf('withdraw', 'withdraw');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('withdraw_export');

		$table = $title = 'withdraw';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_withdraw->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}


	
	/**
	* terima Produks
	*
	* @var $id String
	*/
	public function terima($id = null)
	{

		$cek_name = $this->model_withdraw->datagrabs(array('tabel'=>'withdraw','where'=>array('id'=>$id)))->row();

		$cek_saldo_off = $this->model_withdraw->datagrabs(array('tabel'=>'saldo','where'=>array('kode_rt'=>$cek_name->kode_rt)))->row();

		$saldo = $cek_saldo_off->saldo - $cek_name->jumlah;
		$par = array(
			'tabel'=>'saldo',
			'data'=>array(
				'saldo'=>$saldo
				),
			);
		if($id!=NULL){
				$par['where'] = array('kode_rt'=>$cek_name->kode_rt);
			}

		$sim = $this->model_withdraw->save_data($par);


		$par2 = array(
			'tabel'=>'withdraw',
			'data'=>array(
				
				'status_code'=>2,
				'status'=>'diterima'
				),
			);
		if($id!=NULL){
				$par2['where'] = array('id'=>$id);
			}

		$sim2 = $this->model_withdraw->save_data($par2);
		$data_rt = $this->mymodel->getbywhere('warga', "kode_rt ='$cek_name->kode_rt' AND id_jabatan =2", '', 'row');
		if (!empty($data_rt->id_fcm)) {
                    
            //$datas = $this->send_notif("Withdraw","Selamat Withdraw ".$cek_name->jumlah." diterima", $data_rt->id_fcm, array('priority'=>'high','content_available'=>true,'route' => '/dashboard','is_payment'=>false));
             $Msg = array(
		      'title' => "Withdraw",
		      'body' => "Selamat withdraw senilai ".rupiah($cek_name->jumlah)." yang anda ajukan pada tanggal ".$cek_name->created_at." - diterima dan sudah ditransfer ke Rekening anda",
		      "content_available" =>true,
		      "click_action"=>"FLUTTER_NOTIFICATION_CLICK",
		      "priority" => "high",
		      "android_channel_id"=>"ayowarga"
		    );
		    
		    $fcmFields = array(
		      'to' => $data_rt->id_fcm,
		      'notification' => $Msg,
		      'data'=>array('priority'=>'high','content_available'=>true,'route' => '/dashboard','is_payment'=>false)
		    );
		    $headers = array(
		      'Authorization: key=' . API_ACCESS_KEY,
		      'Content-Type: application/json'
		    );
		    $ch = curl_init();
		    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
		    curl_setopt( $ch,CURLOPT_POST, true );
		    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fcmFields ) );
		    $result = curl_exec($ch );
		    curl_close( $ch );

		    $cek_respon = explode(',',$result);
		    $berhasil = substr($cek_respon[1],strpos($cek_respon[1],':')+1);
		    

			
          }
					if ($sim OR $sim2) {
			            set_message(cclang('Saldo Behasil di withdraw', 'produk'), 'success');
			        } else {
			            set_message(cclang('gagal di simpan', 'produk'), 'error');
			        }

					redirect_back();
		/*cek($cek_name->jumlah);
		cek($data_rt->id_fcm);
		die();*/
		
	}

  public function send_notif($title,$desc,$id_fcm,$data)
  {


  }

	/**
	* delete Produks
	*
	* @var $id String
	*/
	public function tolak($id = null)
	{

		$cek_name = $this->model_withdraw->datagrabs(array('tabel'=>'withdraw','where'=>array('id'=>$id)))->row();


		$par2 = array(
			'tabel'=>'withdraw',
			'data'=>array(
				
				'status_code'=>3,
				'status'=>'ditolak'
				),
			);
		if($id!=NULL){
				$par2['where'] = array('id'=>$id);
			}

		$sim2 = $this->model_withdraw->save_data($par2);
		

		$data_rt = $this->mymodel->getbywhere('warga', "kode_rt ='$cek_name->kode_rt' AND id_jabatan =2", '', 'row');
		if (!empty($data_rt->id_fcm)) {
                    

             $Msg = array(
		      'title' => "Withdraw",
		      'body' => "Mohon maaf, withdraw senilai ".rupiah($cek_name->jumlah)." yang anda ajukan pada tanggal ".$cek_name->created_at." - ditolak",
		      "content_available" =>true,
		      "click_action"=>"FLUTTER_NOTIFICATION_CLICK",
		      "priority" => "high",
		      "android_channel_id"=>"ayowarga"
		    );
		    
		    $fcmFields = array(
		      'to' => $data_rt->id_fcm,
		      'notification' => $Msg,
		      'data'=>array('priority'=>'high','content_available'=>true,'route' => '/dashboard','is_payment'=>false)
		    );
		    $headers = array(
		      'Authorization: key=' . API_ACCESS_KEY,
		      'Content-Type: application/json'
		    );
		    $ch = curl_init();
		    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
		    curl_setopt( $ch,CURLOPT_POST, true );
		    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fcmFields ) );
		    $result = curl_exec($ch );
		    curl_close( $ch );

		    $cek_respon = explode(',',$result);
		    $berhasil = substr($cek_respon[1],strpos($cek_respon[1],':')+1);
		    

			
          }


		if ($sim OR $sim2) {
            set_message(cclang('withdraw di tolak', 'produk'), 'success');
        } else {
            set_message(cclang('gagal di simpan', 'produk'), 'error');
        }

		redirect_back();
	}
	
}


/* End of file withdraw.php */
/* Location: ./application/controllers/administrator/Withdraw.php */