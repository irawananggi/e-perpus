<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*| --------------------------------------------------------------------------
*| Web Controller
*| --------------------------------------------------------------------------
*| For default controller
*|
*/
class Web extends Front
{
	
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{  
        $perpus_murid_id = $this->session->userdata('perpus_murid_id');
        $data['mysession'] =  $this->mymodel->getbywhere('perpus_murid','perpus_murid_id',$perpus_murid_id,"row");

        if($this->input->post('judul') != NULL){
            $data['judul']= $this->input->post('judul');
            $data['perpus_bukus'] = $this->mymodel->withquery("select * from  perpus_buku where perpus_buku_judul='".$this->input->post('judul')."'","result");
        }else{
            $data['perpus_bukus'] = $this->mymodel->withquery("select * from  perpus_buku","result");
        }
		if (installation_complete()) {
            
            /*$perpus_bukus = $this->mymodel->withquery("select * from  perpus_buku where  perpus_bukuId ='".$perpus_buku->perpus_buku_id."' AND perpus_buku_status = 2","result");*/
            $this->template->build('home',$data);
			//$this->home($data);
            
		} else {
			redirect('wizzard/language','refresh');
		}
	}

	public function switch_lang($lang = 'english')
	{
        $this->load->helper(['cookie']);

        set_cookie('language', $lang, (60 * 60 * 24) * 365 );
        $this->lang->load('web', $lang);
        redirect_back();
	}

	public function home() 
	{
        if (defined('IS_DEMO')) {
          $this->template->build('home-demo');
        } else {
		  $this->template->build('home');
        }
	}
    public function kasir() 
    {
        
        $this->template->build('home');
    }
    public function save_cart() 
    {
        $post = $_POST;
        $getCart =  $this->mymodel->withquery("SELECT * FROM cart where cart_userId='".get_user_data('id')."' AND cart_layananId='".$post['layanan']."'","result");
        $getUser = $this->mymodel->getbywhere('aauth_users ',"id",get_user_data('id'),'row');
        if(count($getCart) > 0){
            $getCartQty =  $this->mymodel->withquery("SELECT * FROM cart where cart_userId='".get_user_data('id')."' AND cart_layananId='".$post['layanan']."'","row");

            $data = array(
                "cart_qty" => $getCartQty->cart_qty+1
            );
            $ss =  $this->mymodel->update('cart',$data,'cart_id',$getCartQty->cart_id);
        }else{
            $data = array( 
                "cart_userId" =>get_user_data('id'),
                "cart_kode_referal" =>$getUser->kode_referal,
                "cart_layananId" => $post['layanan'],
                "cart_diskon" => 0,
                "cart_qty" => 1,
                "cart_harga" => $post['harga']
            );
            $ss = $this->mymodel->insert('cart',$data);
        }
        
            $datas =  $this->mymodel->withquery("SELECT *,SUM(cart_qty*cart_harga) as total_cart,SUM(cart_qty) as total_id FROM cart where cart_userId='".get_user_data('id')."'","row");
        $output = json_encode($datas);
        die($output);

        $this->template->build('home',$output);
    }
    public function minus_cart() 
    {
        $post = $_POST;

        $getCartQty =  $this->mymodel->withquery("SELECT * FROM cart where  cart_id='".$post['cart_id']."'","row");

        $data = array(
            "cart_qty" => $getCartQty->cart_qty-1
        );
        $ss =  $this->mymodel->update('cart',$data,'cart_id',$getCartQty->cart_id);

        $dataNew =  $this->mymodel->withquery("SELECT * FROM cart where  cart_id='".$post['cart_id']."'","row");
        if($dataNew->cart_qty == 0){
            $this->mymodel->delete('cart','cart_id',$post['cart_id']); 
        }
        $datas =  $this->mymodel->withquery("SELECT *,SUM(cart_qty*cart_harga) as total_cart,SUM(cart_qty) as total_id FROM cart where cart_userId='".get_user_data('id')."'","row");
        $output = json_encode($datas);
        die($output);

        $this->template->build('home',$output);
    }
    public function plus_cart() 
    {
        $post = $_POST;

        $getCartQty =  $this->mymodel->withquery("SELECT * FROM cart where  cart_id='".$post['cart_id']."'","row");

        $data = array(
            "cart_qty" => $getCartQty->cart_qty+1
        );
        $ss =  $this->mymodel->update('cart',$data,'cart_id',$getCartQty->cart_id);
        
        $datas =  $this->mymodel->withquery("SELECT *,SUM(cart_qty*cart_harga) as total_cart,SUM(cart_qty) as total_id FROM cart where cart_userId='".get_user_data('id')."'","row");
        $output = json_encode($datas);
        die($output);

        $this->template->build('home',$output);
    }

	public function set_full_group_sql()
	{
        $this->db->query(" 
            set global sql_mode='STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
         "); 

        $this->db->query(" 
            set session sql_mode=’STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION’;
         ");

	}

    public function migrate($version = null)
    {
        $this->load->library('migration');
            
        if ($version) {
            if ($this->migration->version($version) === FALSE) {
               show_error($this->migration->error_string());
            }   
        } 
        else {
            if ($this->migration->latest() === FALSE) {
               show_error($this->migration->error_string());
            }   
        }

    }

    public function migrate_cicool()
    {
        $this->load->helper('file');
        $this->load->helper('directory');

        $files = (directory_map('application/controllers/administrator')); 

        foreach ($files as $file) {
            $f_name = str_replace('.php', '', $file);
            $f_name_lower = strtolower(str_replace('.php', '', $file));

            if ($file == 'index.html' ) {
                continue;
            }
            if ($f_name_lower != 'web') {
                
            mkdir('modules/'.$f_name);
            mkdir('modules/'.$f_name.'/models');
            mkdir('modules/'.$f_name.'/views');
            mkdir('modules/'.$f_name.'/controllers');
            mkdir('modules/'.$f_name.'/controllers/backend');
            mkdir('modules/'.$f_name.'/views/backend');
            mkdir('modules/'.$f_name.'/views/backend/standart');
            mkdir('modules/'.$f_name.'/views/backend/standart/administrator');
            copy(FCPATH.'/application/models/Model_'.$f_name_lower.'.php', 'modules/'.$f_name_lower.'/models/Model_'.$f_name_lower.'.php' );
            copy(FCPATH.'/application/controllers/administrator/'.$f_name.'.php', 'modules/'.$f_name.'/controllers/backend/'.$f_name.'.php' );
            if (is_dir(FCPATH.'/application/views/backend/standart/administrator/'.$f_name_lower)) {
                
            $this->recurse_copy(FCPATH.'/application/views/backend/standart/administrator/'.$f_name_lower, 'modules/'.$f_name.'/views/backend/standart/administrator/'.$f_name_lower );
            }
            //unlink('modules/'.$f_name_lower.'/models'.$f_name_lower.'.php' );
            }
        }


    }
    public function migrate_cicool_front()
    {
        $this->load->helper('file');
        $this->load->helper('directory');

        $files = (directory_map('application/controllers')); 

        foreach ($files as $file) {
            $f_name = str_replace('.php', '', $file);
            $f_name_lower = strtolower(str_replace('.php', '', $file));

            if ($file == 'index.html' ) {
                continue;
            }
            if ($f_name_lower != 'web') {
                
            mkdir('modules/'.$f_name);
            mkdir('modules/'.$f_name.'/models');
            mkdir('modules/'.$f_name.'/views');
            mkdir('modules/'.$f_name.'/controllers');
            mkdir('modules/'.$f_name.'/controllers');
            mkdir('modules/'.$f_name.'/views/backend');
            mkdir('modules/'.$f_name.'/views/backend/standart');
            mkdir('modules/'.$f_name.'/views/backend/standart/administrator');
            copy(FCPATH.'/application/models/Model_'.$f_name_lower.'.php', 'modules/'.$f_name_lower.'/models/Model_'.$f_name_lower.'.php' );
            copy(FCPATH.'/application/controllers/'.$f_name.'.php', 'modules/'.$f_name.'/controllers/'.$f_name.'.php' );
            if (is_dir(FCPATH.'/application/views/backend/standart/administrator/'.$f_name_lower)) {
                
            $this->recurse_copy(FCPATH.'/application/views/backend/standart/administrator/'.$f_name_lower, 'modules/'.$f_name.'/views/backend/standart/administrator/'.$f_name_lower );
            }
            //unlink('modules/'.$f_name_lower.'/models'.$f_name_lower.'.php' );
            }
        }


    }

    public function  recurse_copy($src,$dst) { 
        $dir = opendir($src); 
        @mkdir($dst); 
        while(false !== ( $file = readdir($dir)) ) { 
            if (( $file != '.' ) && ( $file != '..' )) { 
                if ( is_dir($src . '/' . $file) ) { 
                    $this->recurse_copy($src . '/' . $file,$dst . '/' . $file); 
                } 
                else { 
                    copy($src . '/' . $file,$dst . '/' . $file); 
                } 
            } 
        } 
        closedir($dir); 
    } 

    function image($mime_type_or_return = 'image/png')
    {
        $file_path = $this->input->get('path');
        $this->helper('file');

        $image_content = read_file($file_path);

        // Image was not found
        if($image_content === FALSE)
        {
            show_error('Image "'.$file_path.'" could not be found.');
            return FALSE;
        }

        // Return the image or output it?
        if($mime_type_or_return === TRUE)
        {
            return $image_content;
        }

        header('Content-Length: '.strlen($image_content)); // sends filesize header
        header('Content-Type: '.$mime_type_or_return); // send mime-type header
        header('Content-Disposition: inline; filename="'.basename($file_path).'";'); // sends filename header
        exit($image_content); // reads and outputs the file onto the output buffer
    }

    public function create_user()
    {
        for ($i=0; $i < 30; $i++) { 
        $this->aauth->create_user('user'.$i.'@gmail.com', 'admin123', 'user'.$i);
        }
    }

    public function sign_up() 
    {
        $this->template->build('sign_up');
    }
    public function save_signup() 
    {

              
                $perpus_murid_username = $this->input->post('perpus_murid_username');
                $cek = $this->mymodel->getbywhere('perpus_murid', "perpus_murid_username='$perpus_murid_username'", null);
             
            $cek_id = $this->mymodel->withquery("select * from  perpus_murid ORDER BY perpus_murid_id DESC","row");
            $xx  = str_replace('ANGG00000','',$cek_id->perpus_murid_id)+1;
            
        if (empty($cek)) {
                $datas_save = array(
                'perpus_murid_id' => 'ANGG00000'.$xx,
                'perpus_murid_nama' => $this->input->post('perpus_murid_nama'),
                'perpus_murid_kelasId' => $this->input->post('perpus_murid_kelasId'),
                'perpus_murid_agamaId' => $this->input->post('perpus_murid_agamaId'),
                'perpus_murid_jk' => $this->input->post('perpus_murid_jk'),
                'perpus_murid_no_telp' => $this->input->post('perpus_murid_no_telp'),
                'perpus_murid_alamat' => $this->input->post('perpus_murid_alamat'),
                'perpus_murid_ket' => $this->input->post('perpus_murid_ket'),
                'perpus_murid_username' => $this->input->post('perpus_murid_username'),
                'perpus_murid_password' => md5($this->input->post('perpus_murid_password')),
                );  
                
                if (!empty($datas_save)) {
                    $ss = $this->mymodel->insert('perpus_murid', $datas_save);
                    
                    redirect('login');
                }else{
                    $this->session->set_flashdata('err-reg', 'Gagal register Sudah Terdaftar');
                    redirect('sign-up');
                }
            }else{
                    $this->session->set_flashdata('err-reg', 'Username sudah Terdaftar');
                    redirect('sign-up');
                } 
    }
    public function login()
    {
       $this->template->build('login');
    }

    public function login_process() 
    {

       $username =  $_REQUEST['username'];
        $pass =  $_REQUEST['password'];
        $cekemail = $this->mymodel->getbywhere('perpus_murid', "perpus_murid_username='$username'", '', 'row');
        $perpus_murid_id = $this->session->userdata('perpus_murid_id');
        $data['mysession'] =  $this->mymodel->getbywhere('perpus_murid','perpus_murid_id',$perpus_murid_id,"row");
          
        if (isset($cekemail)) {
            $pass  = md5($pass);
            

            if ($pass == $cekemail->perpus_murid_password) {
                
                    $this->session->set_userdata('perpus_murid_username', $cekemail->perpus_murid__username);
                    $this->session->set_userdata('perpus_murid_id', $cekemail->perpus_murid_id);
                    redirect('');
            } else {
                $this->session->set_flashdata('err', 'Password Anda Salah');
                redirect('login');
            }
        } else {
            $this->session->set_flashdata('err', 'Username Tidak Terdaftar');
            redirect('login');
        }

    }

    public function logout($value = '')
    {
        $this->session->unset_userdata('perpus_murid__username');
        $this->session->unset_userdata('perpus_murid_id');
        redirect('');
    }
    public function histori_peminjaman()
        {  
            $perpus_murid_id = $this->session->userdata('perpus_murid_id');
            $data['mysession'] =  $this->mymodel->getbywhere('perpus_murid','perpus_murid_id',$perpus_murid_id,"row");
            
                $data['perpus_bukus'] = $this->mymodel->withquery("select * from  perpus_pinjam where perpus_pinjam_anggotaId ='".$perpus_murid_id."'","result");
            
            if (installation_complete()) {
                
                /*$perpus_bukus = $this->mymodel->withquery("select * from  perpus_buku where  perpus_bukuId ='".$perpus_buku->perpus_buku_id."' AND perpus_buku_status = 2","result");*/
                $this->template->build('histori_peminjaman',$data);
                //$this->home($data);
                
            } else {
                redirect('wizzard/language','refresh');
            }
        }

}


/* End of file Web.php */
/* Location: ./application/controllers/Web.php */