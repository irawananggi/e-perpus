<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_peg_presensi extends MY_Model {

    private $primary_key    = 'peg_presensi_id';
    private $table_name     = 'peg_presensi';
    private $field_search   = ['peg_presensi_pegawaiId', 'peg_presensi_unitId', 'peg_presensi_masuk', 'peg_presensi_keluar'];

    public function __construct()
    {
        $config = array(
            'primary_key'   => $this->primary_key,
            'table_name'    => $this->table_name,
            'field_search'  => $this->field_search,
         );

        parent::__construct($config);
    }

    public function count_all($q = null, $field = null)
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "peg_presensi.".$field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "peg_presensi.".$field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "peg_presensi.".$field . " LIKE '%" . $q . "%' )";
        }

        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $query = $this->db->get($this->table_name);

        return $query->num_rows();
    }

    public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [])
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "peg_presensi.".$field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "peg_presensi.".$field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "peg_presensi.".$field . " LIKE '%" . $q . "%' )";
        }

        if (is_array($select_field) AND count($select_field)) {
            $this->db->select($select_field);
        }
        
        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $this->db->limit($limit, $offset);
                $this->db->order_by('peg_presensi.'.$this->primary_key, "DESC");
                $query = $this->db->get($this->table_name);

        return $query->result();
    }

    public function join_avaiable() {
        $this->db->join('pegawai', 'pegawai.pegawai_id = peg_presensi.peg_presensi_pegawaiId', 'LEFT');
        $this->db->join('unit', 'unit.unit_id = peg_presensi.peg_presensi_unitId', 'LEFT');
        
        $this->db->select('peg_presensi.*,pegawai.pegawai_nama as pegawai_pegawai_nama,unit.unit_nama as unit_unit_nama');


        return $this;
    }

    public function filter_avaiable() {

        if (!$this->aauth->is_admin()) {
            }

        return $this;
    }

}

/* End of file Model_peg_presensi.php */
/* Location: ./application/models/Model_peg_presensi.php */