<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['peg_presensi'] = 'Peg Presensi';
$lang['peg_presensi_id'] = 'Peg Presensi Id';
$lang['peg_presensi_pegawaiId'] = 'Pegawai';
$lang['peg_presensi_unitId'] = 'Unit';
$lang['peg_presensi_masuk'] = 'Masuk';
$lang['peg_presensi_keluar'] = 'Keluar';
