<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Peg Presensi Controller
*| --------------------------------------------------------------------------
*| Peg Presensi site
*|
*/
class Peg_presensi extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_peg_presensi');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Peg Presensis
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('peg_presensi_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['peg_presensis'] = $this->model_peg_presensi->get($filter, $field, $this->limit_page, $offset);
		$this->data['peg_presensi_counts'] = $this->model_peg_presensi->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/peg_presensi/index/',
			'total_rows'   => $this->model_peg_presensi->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Peg Presensi List');
		$this->render('backend/standart/administrator/peg_presensi/peg_presensi_list', $this->data);
	}
	
	/**
	* Add new peg_presensis
	*
	*/
	public function add()
	{
		$this->is_allowed('peg_presensi_add');

		$this->template->title('Peg Presensi New');
		$this->render('backend/standart/administrator/peg_presensi/peg_presensi_add', $this->data);
	}

	/**
	* Add New Peg Presensis
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('peg_presensi_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('peg_presensi_pegawaiId', 'Pegawai', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('peg_presensi_unitId', 'Unit', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('peg_presensi_masuk', 'Masuk', 'trim|required');
		$this->form_validation->set_rules('peg_presensi_keluar', 'Keluar', 'trim|required');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'peg_presensi_pegawaiId' => $this->input->post('peg_presensi_pegawaiId'),
				'peg_presensi_unitId' => $this->input->post('peg_presensi_unitId'),
				'peg_presensi_masuk' => $this->input->post('peg_presensi_masuk'),
				'peg_presensi_keluar' => $this->input->post('peg_presensi_keluar'),
			];

			
			$save_peg_presensi = $this->model_peg_presensi->store($save_data);
            

			if ($save_peg_presensi) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_peg_presensi;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/peg_presensi/edit/' . $save_peg_presensi, 'Edit Peg Presensi'),
						anchor('administrator/peg_presensi', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/peg_presensi/edit/' . $save_peg_presensi, 'Edit Peg Presensi')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/peg_presensi');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/peg_presensi');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Peg Presensis
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('peg_presensi_update');

		$this->data['peg_presensi'] = $this->model_peg_presensi->find($id);

		$this->template->title('Peg Presensi Update');
		$this->render('backend/standart/administrator/peg_presensi/peg_presensi_update', $this->data);
	}

	/**
	* Update Peg Presensis
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('peg_presensi_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('peg_presensi_pegawaiId', 'Pegawai', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('peg_presensi_unitId', 'Unit', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('peg_presensi_masuk', 'Masuk', 'trim|required');
		$this->form_validation->set_rules('peg_presensi_keluar', 'Keluar', 'trim|required');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'peg_presensi_pegawaiId' => $this->input->post('peg_presensi_pegawaiId'),
				'peg_presensi_unitId' => $this->input->post('peg_presensi_unitId'),
				'peg_presensi_masuk' => $this->input->post('peg_presensi_masuk'),
				'peg_presensi_keluar' => $this->input->post('peg_presensi_keluar'),
			];

			
			$save_peg_presensi = $this->model_peg_presensi->change($id, $save_data);

			if ($save_peg_presensi) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/peg_presensi', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/peg_presensi');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/peg_presensi');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Peg Presensis
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('peg_presensi_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'peg_presensi'), 'success');
        } else {
            set_message(cclang('error_delete', 'peg_presensi'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Peg Presensis
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('peg_presensi_view');

		$this->data['peg_presensi'] = $this->model_peg_presensi->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Peg Presensi Detail');
		$this->render('backend/standart/administrator/peg_presensi/peg_presensi_view', $this->data);
	}
	
	/**
	* delete Peg Presensis
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$peg_presensi = $this->model_peg_presensi->find($id);

		
		
		return $this->model_peg_presensi->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('peg_presensi_export');

		$this->model_peg_presensi->export('peg_presensi', 'peg_presensi');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('peg_presensi_export');

		$this->model_peg_presensi->pdf('peg_presensi', 'peg_presensi');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('peg_presensi_export');

		$table = $title = 'peg_presensi';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_peg_presensi->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file peg_presensi.php */
/* Location: ./application/controllers/administrator/Peg Presensi.php */