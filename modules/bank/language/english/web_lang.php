<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['bank'] = 'Bank';
$lang['bank_id'] = 'Bank Id';
$lang['bank_norek'] = 'Norek';
$lang['bank_an'] = 'Atas Nama';
$lang['bank_jenis'] = 'Jenis';
