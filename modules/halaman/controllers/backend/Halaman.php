<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Halaman Controller
*| --------------------------------------------------------------------------
*| Halaman site
*|
*/
class Halaman extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_halaman');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Halamans
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('halaman_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['halamans'] = $this->model_halaman->get($filter, $field, $this->limit_page, $offset);
		$this->data['halaman_counts'] = $this->model_halaman->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/halaman/index/',
			'total_rows'   => $this->model_halaman->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Halaman List');
		$this->render('backend/standart/administrator/halaman/halaman_list', $this->data);
	}
	
	/**
	* Add new halamans
	*
	*/
	public function add()
	{
		$this->is_allowed('halaman_add');

		$this->template->title('Halaman New');
		$this->render('backend/standart/administrator/halaman/halaman_add', $this->data);
	}

	/**
	* Add New Halamans
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('halaman_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('judul', 'Judul', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'judul' => $this->input->post('judul'),
				'deskripsi' => $this->input->post('deskripsi'),
			];

			
			$save_halaman = $this->model_halaman->store($save_data);
            

			if ($save_halaman) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_halaman;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/halaman/edit/' . $save_halaman, 'Edit Halaman'),
						anchor('administrator/halaman', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/halaman/edit/' . $save_halaman, 'Edit Halaman')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/halaman');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/halaman');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Halamans
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('halaman_update');

		$this->data['halaman'] = $this->model_halaman->find($id);

		$this->template->title('Halaman Update');
		$this->render('backend/standart/administrator/halaman/halaman_update', $this->data);
	}

	/**
	* Update Halamans
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('halaman_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('judul', 'Judul', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'judul' => $this->input->post('judul'),
				'deskripsi' => $this->input->post('deskripsi'),
			];

			
			$save_halaman = $this->model_halaman->change($id, $save_data);

			if ($save_halaman) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/halaman', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/halaman');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/halaman');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Halamans
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('halaman_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'halaman'), 'success');
        } else {
            set_message(cclang('error_delete', 'halaman'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Halamans
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('halaman_view');

		$this->data['halaman'] = $this->model_halaman->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Halaman Detail');
		$this->render('backend/standart/administrator/halaman/halaman_view', $this->data);
	}
	
	/**
	* delete Halamans
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$halaman = $this->model_halaman->find($id);

		
		
		return $this->model_halaman->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('halaman_export');

		$this->model_halaman->export('halaman', 'halaman');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('halaman_export');

		$this->model_halaman->pdf('halaman', 'halaman');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('halaman_export');

		$table = $title = 'halaman';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_halaman->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file halaman.php */
/* Location: ./application/controllers/administrator/Halaman.php */