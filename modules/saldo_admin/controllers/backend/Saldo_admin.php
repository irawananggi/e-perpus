<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Saldo Admin Controller
*| --------------------------------------------------------------------------
*| Saldo Admin site
*|
*/
class Saldo_admin extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_saldo_admin');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Saldo Admins
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('saldo_admin_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['saldo_admins'] = $this->model_saldo_admin->get($filter, $field, $this->limit_page, $offset);
		$this->data['saldo_admin_counts'] = $this->model_saldo_admin->count_all($filter, $field);
		$this->data['offset'] = $offset;
		$config = [
			'base_url'     => 'administrator/saldo_admin/index/',
			'total_rows'   => $this->model_saldo_admin->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Saldo Admin List');
		$this->render('backend/standart/administrator/saldo_admin/saldo_admin_list', $this->data);
	}
	
	/**
	* Add new saldo_admins
	*
	*/
	public function add()
	{
		$this->is_allowed('saldo_admin_add');

		$this->template->title('Saldo Admin New');
		$this->render('backend/standart/administrator/saldo_admin/saldo_admin_add', $this->data);
	}

	/**
	* Add New Saldo Admins
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('saldo_admin_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('kode_rt', 'Kode Rt', 'trim|required|max_length[250]');
		$this->form_validation->set_rules('id_warga', 'Id Warga', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('debit', 'Debit', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('created_at', 'Created At', 'trim|required');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'transaksi_id' => $this->input->post('transaksi_id'),
				'kode_rt' => $this->input->post('kode_rt'),
				'id_warga' => $this->input->post('id_warga'),
				'debit' => $this->input->post('debit'),
				'created_at' => $this->input->post('created_at'),
			];

			
			$save_saldo_admin = $this->model_saldo_admin->store($save_data);
            

			if ($save_saldo_admin) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_saldo_admin;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/saldo_admin/edit/' . $save_saldo_admin, 'Edit Saldo Admin'),
						anchor('administrator/saldo_admin', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/saldo_admin/edit/' . $save_saldo_admin, 'Edit Saldo Admin')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/saldo_admin');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/saldo_admin');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Saldo Admins
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('saldo_admin_update');

		$this->data['saldo_admin'] = $this->model_saldo_admin->find($id);

		$this->template->title('Saldo Admin Update');
		$this->render('backend/standart/administrator/saldo_admin/saldo_admin_update', $this->data);
	}

	/**
	* Update Saldo Admins
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('saldo_admin_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('kode_rt', 'Kode Rt', 'trim|required|max_length[250]');
		$this->form_validation->set_rules('id_warga', 'Id Warga', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('debit', 'Debit', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('created_at', 'Created At', 'trim|required');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'transaksi_id' => $this->input->post('transaksi_id'),
				'kode_rt' => $this->input->post('kode_rt'),
				'id_warga' => $this->input->post('id_warga'),
				'debit' => $this->input->post('debit'),
				'created_at' => $this->input->post('created_at'),
			];

			
			$save_saldo_admin = $this->model_saldo_admin->change($id, $save_data);

			if ($save_saldo_admin) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/saldo_admin', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/saldo_admin');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/saldo_admin');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Saldo Admins
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('saldo_admin_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'saldo_admin'), 'success');
        } else {
            set_message(cclang('error_delete', 'saldo_admin'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Saldo Admins
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('saldo_admin_view');

		$this->data['saldo_admin'] = $this->model_saldo_admin->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Saldo Admin Detail');
		$this->render('backend/standart/administrator/saldo_admin/saldo_admin_view', $this->data);
	}
	
	/**
	* delete Saldo Admins
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$saldo_admin = $this->model_saldo_admin->find($id);

		
		
		return $this->model_saldo_admin->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('saldo_admin_export');

		$this->model_saldo_admin->export('saldo_admin', 'saldo_admin');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('saldo_admin_export');

		$this->model_saldo_admin->pdf('saldo_admin', 'saldo_admin');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('saldo_admin_export');

		$table = $title = 'saldo_admin';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_saldo_admin->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file saldo_admin.php */
/* Location: ./application/controllers/administrator/Saldo Admin.php */