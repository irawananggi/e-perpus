<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['unit'] = 'Unit';
$lang['unit_id'] = 'Unit Id';
$lang['unit_nama'] = 'Nama Unit';
$lang['unit_alamat'] = 'Alamat';
$lang['unit_batas'] = 'Batas Jangkauan(Meter)';
$lang['unit_latitude'] = 'Latitude';
$lang['unit_longiitude'] = 'Longiitude';
