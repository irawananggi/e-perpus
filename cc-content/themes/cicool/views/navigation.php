
<nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand page-scroll" href="#page-top">E-Perpus</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

            <ul class="nav navbar-nav navbar-right">
            	<?php foreach (get_menu('top-menu') as $menu): ?>
                    <?php if (app()->aauth->is_allowed('menu_'.$menu->label)): ?>
                        <li>
                            <a class="page-scroll" href="<?= site_url($menu->link); ?>"><?= $menu->label; ?></a>
                        </li>
                    <?php endif ?>
            	<?php endforeach; ?>
                <?php if (empty($mysession)): ?>
                <li>

                </li>
                <li>
                    <a class="page-scroll" href="<?= site_url('login'); ?>"><i class="fa fa-sign-in"></i> <?= cclang('login'); ?></a>
                </li>
                <?php else: ?>
                <li>
                    <a class="page-scroll dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
                        Profile
                        <?= get_user_data('full_name'); ?>
                        <span class="caret"></span>
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="<?= site_url('histori-peminjaman'); ?>">Histori Peminjaman</a>
                        <a class="dropdown-item" href="<?= site_url('profile'); ?>">Profile</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="<?= site_url('logout'); ?>"><i class="fa fa-sign-out"></i> Logout</a>
                    </div>
                </li>
                <?php endif; ?>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>