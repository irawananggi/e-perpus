<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>E-Perpus | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/module/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/module/bootstrap/css/style.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/plugins/iCheck/square/blue.css">
  <style type="text/css">
    .login-box-body {
      border-top: 5px solid #D7320C;
    }
  </style>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
        <div class="login-brand">
              <img src="<?= BASE_ASSET; ?>/img/icon-login.png" alt="logo" class="shadow-light rounded-circle" width="100">
            </div>
        <!-- /.login-logo -->
        <div class="card card-primary" >
            <div class="card-header"><h4>Login Siswa</h4></div>
          <?php if(isset($error) AND !empty($error)): ?>
               <div class="callout callout-error"  style="color:#C82626">
                    <h4><?= cclang('error'); ?>!</h4>
                    <p><?= $error; ?></p>
                  </div>
          <?php endif; ?>
          <?php
          $message = $this->session->flashdata('f_message'); 
          $type = $this->session->flashdata('f_type'); 
          if ($message):
          ?>
         <div class="callout callout-<?= $type; ?>"  style="color:#C82626">
              <p><?= $message; ?></p>
            </div>
          <?php endif; ?>
          <form  action="<?php echo base_url('login_member');?>" method="POST" enctype="multipart/form-data">
            <div class="card-body">
              <label for="username">Username</label>

              <div class="form-group has-feedback <?= form_error('username') ? 'has-error' :''; ?>">
                <input type="text" class="form-control" placeholder="username" name="username" value="">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
              </div>
              <label for="password" class="control-label">Password</label>
              <!-- <div class="float-right">
                <a href="<?= site_url('administrator/forgot-password'); ?>" class="text-small"><?= cclang('i_forgot_my_password'); ?>?
                  
                </a>
              </div> -->

              <div class="form-group has-feedback <?= form_error('password') ? 'has-error' :''; ?>">
                <input type="password" class="form-control" placeholder="Password" name="password" value="">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
              </div>
              <div class="row">
                <div class="col-lg-12">
                  <div class="checkbox icheck">
                    <label>
                      <input type="checkbox" name="remember" value="1"> <?= cclang('remember_me'); ?>
                    </label>
                  </div>
                </div>
                <!-- /.col -->
                <div class="col-lg-12">
                  <button type="submit" class="btn btn-primary btn-block btn-flat"><?= cclang('sign_in'); ?></button>
                </div>
                <!-- /.col -->
                <div class="col-lg-12">
                <a href="<?= site_url('sign-up'); ?>" class="text-small">Belum punya akun?
                  
                </a>
              </div>

              </div>
            </div>
          <?= form_close(); ?>

          <!-- /.social-auth-links -->

          <br>
          <div class="simple-footer">
              Copyright © ANGGI BIRD 2023
            </div>

        </div>
        <!-- /.login-box-body -->
      </div>
    </div>
  </div>
<!-- /.login-box -->

<!-- jQuery 2.2.3 -->
<script src="<?= BASE_ASSET; ?>/admin-lte/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?= BASE_ASSET; ?>/admin-lte/bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?= BASE_ASSET; ?>/admin-lte/plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>
