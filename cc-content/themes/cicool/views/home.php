<?= get_header(); ?>
<style type="text/css">
   header{
      background: #fff !important;
   }
</style>
<body id="page-top">
   <?= get_navigation(); ?>

         <div class="container" style="margin-top:50px;">
                <marquee style="color:black"> <h1 id="homeHeading">Selamat Datang di APLIKASI PERPUSTAKAAN</h1></marquee>
                <br>
                <br>
                <br>

         <div class="row">
                  <div class="col-md-12" style="padding-left:0px">
                     
                     <form name="form_perpus_buku" id="form_perpus_buku" action="<?= base_url(); ?>" method="POST">
                     <div class="col-sm-8 padd-left-0  " style="padding-left:0px">
                        <input type="text" class="form-control" name="judul" id="filter" placeholder="Filter" value="<?php echo $judul;?>">
                     </div>
                     
                     <div class="col-sm-1 padd-left-0 ">
                        <button type="submit" class="btn btn-flat" name="sbtn" id="sbtn" value="Apply" title="" data-original-title="Refining Search">
                        Filter
                        </button>
                     </div>
                     <div class="col-sm-1 padd-left-0 " style="padding-left:0px">
                        <a class="btn btn-default btn-flat" name="reset" id="reset" value="Apply" href="<?= base_url();?>" title="<?= cclang('reset_filter'); ?>">
                        <i class="fa fa-undo"></i>
                        </a>
                     </div>
                     </form>
                  </div>
                  <br>
                  <br>
            <div class="table-responsive"> 
                  <table class="table table-bordered table-striped dataTable">
                     <thead>
                        <tr class="">
                           <th> No</th>
                           <th> Judul</th>
                           <th> No Rak</th>
                           <th> Penerbit dan Pengarang</th>
                           <th> Terbit</th>
                           <th> Tersedia</th>
                        </tr>
                     </thead>
                     <tbody id="tbody_perpus_buku">
                     <?php $no=1; foreach($perpus_bukus as $perpus_buku): ?>
                        <tr>
                           <td><?php echo $no;?></td>
                           <td><?php echo $perpus_buku->perpus_buku_judul;?></td>

                           <td>
                             <?php
                              $getdetailRak = $this->mymodel->withquery("select * from  perpus_rak where  perpus_rak_no_rak ='".$perpus_buku->perpus_buku_no_rak."'","row");
                              
                                echo $getdetailRak->perpus_rak_nama;
                               
                              ?>
                           </td> 
                           <td> <?php
                              $getdetailpenerbit = $this->mymodel->withquery("select * from  perpus_penerbit where  perpus_penerbit_id ='".$perpus_buku->perpus_buku_penerbitId."'","row");
                               
                              $getdetailpengarang = $this->mymodel->withquery("select * from  perpus_pengarang where  perpus_pengarang_id ='".$perpus_buku->perpus_buku_pengarangId."'","row");
                              
                                echo $getdetailpenerbit->perpus_penerbit_nama.' - '. $getdetailpengarang->perpus_pengarang_nama;
                               
                              ?></td> 
                           <td><?= _ent($perpus_buku->perpus_buku_thn_terbit); ?></td> 
                           <td>                             <?php
                              $getdetailBuku = $this->mymodel->withquery("select * from  perpus_detail_buku where  perpus_detail_bukuId ='".$perpus_buku->perpus_buku_id."' AND perpus_detail_buku_status = 2","result");
                              
                              if(count($getdetailBuku) > 0){
                                echo count($getdetailBuku);
                               }else{ 

                                echo ' 0 ';
                               }
                              ?>
                           </td>
                          </tr>
                      <?php $no++; endforeach; ?>
                      <?php if ($perpus_buku_counts == 0) :?>
                         <tr>
                           <td colspan="100">
                           Perpus Buku data is not available
                           </td>
                         </tr>
                      <?php endif; ?>
                     </tbody>
                  </table>
                  </div>
           <div class="col-md-12">
                     <div class="dataTables_paginate paging_simple_numbers pull-right" id="example2_paginate" ><?= $pagination; ?>
                     </div>
                  </div>
            <hr>
            <p> By ANGGI BIRD <br></p>
            
         </div>
         </div>
   <?= get_footer(); ?>