<html lang="en"><head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>KASIR LAUNDRY</title>
  <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
  <link rel="stylesheet" href="<?= theme_asset(); ?>/kasir/style.css">
  <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body class="bg-blue-gray-50" x-data="initApp()" x-init="initDatabase()">
  <!-- noprint-area -->
  <div class="hide-print flex flex-row h-screen antialiased text-blue-gray-800">
    <!-- left sidebar -->
    

    <!-- page content -->
    <div class="flex-grow flex">
      <!-- store menu -->
      <div class="flex flex-col bg-blue-gray-50 h-full w-full py-4">
        <div class="flex px-2 flex-row relative">
          <div class="absolute left-5 top-3 px-2 py-2 rounded-full bg-cyan-500 text-white">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
              <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path>
            </svg>
          </div>
          <input type="text" class="bg-white rounded-3xl shadow text-lg full w-full h-16 py-4 pl-16 transition-shadow focus:shadow-2xl focus:outline-none" placeholder="Cari Layanan ..." x-model="keyword">
        </div>
        <div class="h-full overflow-hidden mt-4">
          <div class="h-full overflow-y-auto px-2">
            <div x-show="filteredProducts().length" class="grid grid-cols-4 gap-4 pb-3">
              <template x-for="product in filteredProducts()" :key="product.id">
                <div role="button" class="select-none cursor-pointer transition-shadow overflow-hidden rounded-2xl bg-white shadow hover:shadow-lg" :title="product.name" x-on:click="addToCart(product)">
                  <img :src="product.image" :alt="product.name">
                  <div class="flex pb-3 px-3 text-sm -mt-3">
                    <p class="flex-grow truncate mr-1" x-text="product.name"></p>
                    <p class="nowrap font-semibold" x-text="priceFormat(product.price)"></p>
                  </div>
                </div>
              </template>
              <?php
              $getData =  $this->mymodel->withquery("SELECT * FROM layanan order by layanan_nama","result");
              
              foreach ($getData as $key => $value) { ?>
                  <a id="Layanan" data-id="<?php echo $value->layanan_id;?>"  data-harga="<?php echo $value->layanan_harga;?>" >
              <div role="button" class="select-none cursor-pointer transition-shadow overflow-hidden rounded-2xl bg-white shadow hover:shadow-lg" :title="product.name" x-on:click="addToCart(product)" title="<?php echo $value->layanan_nama;?>" style="height: 200px;" >
                  <img :src="product.image" :alt="product.name" src="<?php echo base_url('uploads/layanan/'.$value->layanan_icon);?>" alt="<?php echo $value->layanan_nama;?>" style="width:100%;height: 140px;">
                  <br>
                  <div class="flex pb-3 px-3 text-sm -mt-3" style="text-align: center;width: 100%">
                    <p class="nowrap font-semibold" x-text="priceFormat(product.price)" style="text-align: center;width: 100%; font-weight: bold"><?php echo numberToCurrency($value->layanan_harga);?></p>
                  </div>
                  <div class="flex pb-3 px-3 text-sm -mt-3" style="text-align: center;width: 100%">
                    <p class="flex-grow truncate mr-1" x-text="product.name" style="text-align: center;width: 100%"><?php echo $value->layanan_nama;?></p>
                  </div>
                </div>
              </a>
             <?php }
              ?>
              </div>
          </div>
        </div>
      </div>
      <!-- end of store menu -->

      <!-- right sidebar -->
      <div class="w-5/12 flex flex-col bg-blue-gray-50 h-full bg-white pr-4 pl-2 py-4">
        <div class="bg-white rounded-3xl flex flex-col h-full shadow">

          <!-- cart items -->
          <div x-show="cart.length > 0" class="flex-1 flex flex-col overflow-auto">
            <div class="h-16 text-center flex justify-center">
              <div class="pl-8 text-left text-lg py-4 relative">
                <!-- cart icon -->
                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 inline-block" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 3h2l.4 2M7 13h10l4-8H5.4M7 13L5.4 5M7 13l-2.293 2.293c-.63.63-.184 1.707.707 1.707H17m0 0a2 2 0 100 4 2 2 0 000-4zm-8 2a2 2 0 11-4 0 2 2 0 014 0z"></path>
                </svg>
                <div x-show="getItemsCount() > 0" class="text-center absolute bg-red-500 text-white w-5 h-5 text-xs p-0 leading-5 rounded-full -right-2 top-3" x-text="getItemsCount()" id="total_id">
                <?php 
                $dataCart = $this->mymodel->getbywhere('cart',"cart_userId",get_user_data('id'),'result');
                $dataX =  $this->mymodel->withquery("SELECT *,SUM(cart_qty*cart_harga) as total_cart,SUM(cart_qty) as total_id FROM cart where cart_userId='".get_user_data('id')."'","row"); 

                echo $dataX->total_id;?> 
                </div>
              </div>
              <div class="flex-grow px-8 text-right text-lg py-4 relative">
                <!-- trash button -->
                <button x-on:click="clear()" class="text-blue-gray-300 hover:text-pink-500 focus:outline-none">
                  <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 inline-block" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"></path>
                  </svg>
                </button>
              </div>
            </div>

            <div class="flex-1 w-full px-4 overflow-auto">
              <div class="mb-3 text-blue-gray-700 px-3 pt-2 pb-3 rounded-lg bg-blue-gray-50">
              <div class="flex text-sl font-semibold">
                <div class="flex-grow text-left" style="width: 33%;float:left">Member</div>
                <div class="flex text-right" style="width: 66%;float:left">
                  <input type="text" class="w-28 text-right bg-white shadow rounded-lg focus:bg-white focus:shadow-lg px-2 focus:outline-none" value='Anggi Bird' style="width: 100%;color:#e3e3e3;">
                  <div class="mr-2"><div class="rounded-full bg-cyan-500 text-white" style="padding:2px;font-size: 10px;float: left;
margin-left: -200px;">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
              <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path>
            </svg>
          </div></div>
                </div>
              </div>
              <!-- <hr class="my-2">
              <div class="grid grid-cols-3 gap-2 mt-2">
                <template x-for="money in moneys">
                  <button x-on:click="addCash(money)" class="bg-white rounded-lg shadow hover:shadow-lg focus:outline-none inline-block px-2 py-1 text-sm">+<span x-text="numberFormat(money)"></span></button>
                </template>
                </div> -->
            </div>
                
              
                
                <div id="container">
                <?php 
                if(count($dataCart) > 0){ 
                  foreach ($dataCart as $key => $value) {

                  $getLayanan = $this->mymodel->getbywhere('layanan',"layanan_id",$value->cart_layananId,'row');

                     # code...
                   ?>
                  <div class="select-none mb-3 bg-blue-gray-50 rounded-lg w-full text-blue-gray-700 py-2 px-2 flex justify-center">
                  <img :src="item.image" alt="" class="rounded-lg h-10 w-10 bg-white shadow mr-2" src="<?php echo base_url('uploads/layanan/'.$getLayanan->layanan_icon);?>">
                  <div class="flex-grow">
                    <h5 class="text-sm" x-text="item.name"><?php echo $getLayanan->layanan_nama;?></h5>
                    <p class="text-xs block" x-text="priceFormat(item.price)"><?php echo numberToCurrency($getLayanan->layanan_harga);?></p>
                  </div>
                  <div class="py-1">
                    <div class="w-28 grid grid-cols-3 gap-2 ml-2">
                      <button id="MinusCart" data-id="<?php echo $value->cart_id;?>"  class="rounded-lg text-center py-1 text-white bg-blue-gray-600 hover:bg-blue-gray-700 focus:outline-none">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-3 inline-block" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                          <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M20 12H4"></path>
                        </svg>
                      </button>
                      <input x-model.number="item.qty" type="text" class="bg-white rounded-lg text-center shadow focus:outline-none focus:shadow-lg text-sm" value="<?php echo $value->cart_qty;?>">
                      <button  id="PlusCart" data-id="<?php echo $value->cart_id;?>"   class="rounded-lg text-center py-1 text-white bg-blue-gray-600 hover:bg-blue-gray-700 focus:outline-none">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-3 inline-block" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                          <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6v6m0 0v6m0-6h6m-6 0H6"></path>
                        </svg>
                      </button>
                    </div>
                  </div>
                </div>
                <?php } }else{
                  echo '<div class="select-none mb-3 bg-blue-gray-50 rounded-lg w-full text-blue-gray-700 py-2 px-2 flex justify-center">
                  Data Kosong
                </div>';
                }
                ?>
                 </div>

              </div>
          </div>
          <!-- end of cart items -->

          <!-- payment info -->
          <div class="select-none h-auto w-full text-center pt-3 pb-4 px-4">
            <div class="flex mb-3 text-lg font-semibold text-blue-gray-700">
              <div>Diskon</div>
              <div class="text-right w-full" x-text="priceFormat(getTotalPrice())"> 0</div>
            </div>
            <div class="flex mb-3 text-lg font-semibold text-blue-gray-700">
              <div>TOTAL</div>
              <div class="text-right w-full" x-text="priceFormat(getTotalPrice())" id="total_cart">
                <?php 
                $datas =  $this->mymodel->withquery("SELECT *,SUM(cart_qty*cart_harga) as total_cart FROM cart where cart_userId='".get_user_data('id')."'","row");
                
                if(empty($datas->cart_userId)){
                  echo ' 0';
                }else{
                  echo ' '.numberToCurrency($datas->total_cart);
                } ?>

              </div>
            </div>
            <div class="mb-3 text-blue-gray-700 px-3 pt-2 pb-3 rounded-lg bg-blue-gray-50">
              <div class="flex text-lg font-semibold">
                <div class="flex-grow text-left">CASH</div>
                <div class="flex text-right">
                  <div class="mr-2">Rp</div>
                  <input type="text" class="w-28 text-right bg-white shadow rounded-lg focus:bg-white focus:shadow-lg px-2 focus:outline-none" value='' onkeyup="keyChange(this)" id='getcash' onkeypress="return hanyaAngka(event)">
                  <input type="hidden" class="w-28 text-right bg-white shadow rounded-lg focus:bg-white focus:shadow-lg px-2 focus:outline-none" id='getcash2'>
                </div>
              </div>
              <!-- <hr class="my-2">
              <div class="grid grid-cols-3 gap-2 mt-2">
                <template x-for="money in moneys">
                  <button x-on:click="addCash(money)" class="bg-white rounded-lg shadow hover:shadow-lg focus:outline-none inline-block px-2 py-1 text-sm">+<span x-text="numberFormat(money)"></span></button>
                </template>
                </div> -->
            </div>
            <div x-show="change > 0" class="flex mb-3 text-lg font-semibold bg-cyan-50 text-blue-gray-700 rounded-lg py-2 px-3">
              <div class="text-cyan-800">CHANGE</div>
              <div class="text-right flex-grow text-cyan-600" id="getchange">0</div>
            </div>
            <!-- <div x-show="change < 0" class="flex mb-3 text-lg font-semibold bg-pink-100 text-blue-gray-700 rounded-lg py-2 px-3">
              <div class="text-right flex-grow text-pink-600" x-text="priceFormat(change)">Rp. 32.000</div>
            </div> -->
            <div x-show="change == 0 &amp;&amp; cart.length > 0" class="flex justify-center mb-3 text-lg font-semibold bg-cyan-50 text-cyan-700 rounded-lg py-2 px-3" style="display: none;">
              <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 inline-block" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M14 10h4.764a2 2 0 011.789 2.894l-3.5 7A2 2 0 0115.263 21h-4.017c-.163 0-.326-.02-.485-.06L7 20m7-10V5a2 2 0 00-2-2h-.095c-.5 0-.905.405-.905.905 0 .714-.211 1.412-.608 2.006L7 11v9m7-10h-2M7 20H5a2 2 0 01-2-2v-6a2 2 0 012-2h2.5"></path>
              </svg>
            </div>
            <button class="text-white rounded-2xl text-lg w-full py-3 focus:outline-none bg-cyan-500 hover:bg-cyan-600" x-bind:class="{
                'bg-cyan-500 hover:bg-cyan-600': submitable(),
                'bg-blue-gray-200': !submitable()
              }" :disabled="!submitable()" x-on:click="submit()">
              SUBMIT
            </button>
          </div>
          <!-- end of payment info -->
        </div>
      </div>
      <!-- end of right sidebar -->
      <!-- right sidebar -->
      <div class="w-5/12 flex flex-col bg-blue-gray-50 h-full bg-white pr-4 pl-2 py-4">
        <div class="bg-white rounded-3xl flex flex-col h-full shadow">

          <!-- cart items -->
          <div x-show="cart.length > 0" class="flex-1 flex flex-col overflow-auto">
            <div class="h-16 text-center flex justify-center">
              <div class="pl-8 text-left text-lg py-4 relative">
                <!-- cart icon -->
                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 inline-block" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 3h2l.4 2M7 13h10l4-8H5.4M7 13L5.4 5M7 13l-2.293 2.293c-.63.63-.184 1.707.707 1.707H17m0 0a2 2 0 100 4 2 2 0 000-4zm-8 2a2 2 0 11-4 0 2 2 0 014 0z"></path>
                </svg>
                <div x-show="getItemsCount() > 0" class="text-center absolute bg-red-500 text-white w-5 h-5 text-xs p-0 leading-5 rounded-full -right-2 top-3" x-text="getItemsCount()" id="total_id">
                <?php 
                $dataCart = $this->mymodel->getbywhere('cart',"cart_userId",get_user_data('id'),'result');
                $dataX =  $this->mymodel->withquery("SELECT *,SUM(cart_qty*cart_harga) as total_cart,SUM(cart_qty) as total_id FROM cart where cart_userId='".get_user_data('id')."'","row"); 

                echo $dataX->total_id;?> 
                </div>
              </div>
              <div class="flex-grow px-8 text-right text-lg py-4 relative">
                <!-- trash button -->
                <button x-on:click="clear()" class="text-blue-gray-300 hover:text-pink-500 focus:outline-none">
                  <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 inline-block" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"></path>
                  </svg>
                </button>
              </div>
            </div>

            <div class="flex-1 w-full px-4 overflow-auto">
              <div class="mb-3 text-blue-gray-700 px-3 pt-2 pb-3 rounded-lg bg-blue-gray-50">
              <div class="flex text-sl font-semibold">
                <div class="flex-grow text-left" style="width: 33%;float:left">Member</div>
                <div class="flex text-right" style="width: 66%;float:left">
                  <input type="text" class="w-28 text-right bg-white shadow rounded-lg focus:bg-white focus:shadow-lg px-2 focus:outline-none" value='Anggi Bird' style="width: 100%;color:#e3e3e3;">
                  <div class="mr-2"><div class="rounded-full bg-cyan-500 text-white" style="padding:2px;font-size: 10px;float: left;
margin-left: -200px;">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
              <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path>
            </svg>
          </div></div>
                </div>
              </div>
              <!-- <hr class="my-2">
              <div class="grid grid-cols-3 gap-2 mt-2">
                <template x-for="money in moneys">
                  <button x-on:click="addCash(money)" class="bg-white rounded-lg shadow hover:shadow-lg focus:outline-none inline-block px-2 py-1 text-sm">+<span x-text="numberFormat(money)"></span></button>
                </template>
                </div> -->
            </div>
                
              
                
                <div id="container">
                <?php 
                if(count($dataCart) > 0){ 
                  foreach ($dataCart as $key => $value) {

                  $getLayanan = $this->mymodel->getbywhere('layanan',"layanan_id",$value->cart_layananId,'row');

                     # code...
                   ?>
                  <div class="select-none mb-3 bg-blue-gray-50 rounded-lg w-full text-blue-gray-700 py-2 px-2 flex justify-center">
                  <img :src="item.image" alt="" class="rounded-lg h-10 w-10 bg-white shadow mr-2" src="<?php echo base_url('uploads/layanan/'.$getLayanan->layanan_icon);?>">
                  <div class="flex-grow">
                    <h5 class="text-sm" x-text="item.name"><?php echo $getLayanan->layanan_nama;?></h5>
                    <p class="text-xs block" x-text="priceFormat(item.price)"><?php echo numberToCurrency($getLayanan->layanan_harga);?></p>
                  </div>
                  <div class="py-1">
                    <div class="w-28 grid grid-cols-3 gap-2 ml-2">
                      <button id="MinusCart" data-id="<?php echo $value->cart_id;?>"  class="rounded-lg text-center py-1 text-white bg-blue-gray-600 hover:bg-blue-gray-700 focus:outline-none">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-3 inline-block" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                          <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M20 12H4"></path>
                        </svg>
                      </button>
                      <input x-model.number="item.qty" type="text" class="bg-white rounded-lg text-center shadow focus:outline-none focus:shadow-lg text-sm" value="<?php echo $value->cart_qty;?>">
                      <button  id="PlusCart" data-id="<?php echo $value->cart_id;?>"   class="rounded-lg text-center py-1 text-white bg-blue-gray-600 hover:bg-blue-gray-700 focus:outline-none">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-3 inline-block" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                          <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6v6m0 0v6m0-6h6m-6 0H6"></path>
                        </svg>
                      </button>
                    </div>
                  </div>
                </div>
                <?php } }else{
                  echo '<div class="select-none mb-3 bg-blue-gray-50 rounded-lg w-full text-blue-gray-700 py-2 px-2 flex justify-center">
                  Data Kosong
                </div>';
                }
                ?>
                 </div>

              </div>
          </div>
          <!-- end of cart items -->

          <!-- payment info -->
          <div class="select-none h-auto w-full text-center pt-3 pb-4 px-4">
            <div class="flex mb-3 text-lg font-semibold text-blue-gray-700">
              <div>Diskon</div>
              <div class="text-right w-full" x-text="priceFormat(getTotalPrice())"> 0</div>
            </div>
            <div class="flex mb-3 text-lg font-semibold text-blue-gray-700">
              <div>TOTAL</div>
              <div class="text-right w-full" x-text="priceFormat(getTotalPrice())" id="total_cart">
                <?php 
                $datas =  $this->mymodel->withquery("SELECT *,SUM(cart_qty*cart_harga) as total_cart FROM cart where cart_userId='".get_user_data('id')."'","row");
                
                if(empty($datas->cart_userId)){
                  echo ' 0';
                }else{
                  echo ' '.numberToCurrency($datas->total_cart);
                } ?>

              </div>
            </div>
            <div class="mb-3 text-blue-gray-700 px-3 pt-2 pb-3 rounded-lg bg-blue-gray-50">
              <div class="flex text-lg font-semibold">
                <div class="flex-grow text-left">CASH</div>
                <div class="flex text-right">
                  <div class="mr-2">Rp</div>
                  <input type="text" class="w-28 text-right bg-white shadow rounded-lg focus:bg-white focus:shadow-lg px-2 focus:outline-none" value='' onkeyup="keyChange(this)" id='getcash' onkeypress="return hanyaAngka(event)">
                  <input type="hidden" class="w-28 text-right bg-white shadow rounded-lg focus:bg-white focus:shadow-lg px-2 focus:outline-none" id='getcash2'>
                </div>
              </div>
              <!-- <hr class="my-2">
              <div class="grid grid-cols-3 gap-2 mt-2">
                <template x-for="money in moneys">
                  <button x-on:click="addCash(money)" class="bg-white rounded-lg shadow hover:shadow-lg focus:outline-none inline-block px-2 py-1 text-sm">+<span x-text="numberFormat(money)"></span></button>
                </template>
                </div> -->
            </div>
            <div x-show="change > 0" class="flex mb-3 text-lg font-semibold bg-cyan-50 text-blue-gray-700 rounded-lg py-2 px-3">
              <div class="text-cyan-800">CHANGE</div>
              <div class="text-right flex-grow text-cyan-600" id="getchange">0</div>
            </div>
            <!-- <div x-show="change < 0" class="flex mb-3 text-lg font-semibold bg-pink-100 text-blue-gray-700 rounded-lg py-2 px-3">
              <div class="text-right flex-grow text-pink-600" x-text="priceFormat(change)">Rp. 32.000</div>
            </div> -->
            <div x-show="change == 0 &amp;&amp; cart.length > 0" class="flex justify-center mb-3 text-lg font-semibold bg-cyan-50 text-cyan-700 rounded-lg py-2 px-3" style="display: none;">
              <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 inline-block" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M14 10h4.764a2 2 0 011.789 2.894l-3.5 7A2 2 0 0115.263 21h-4.017c-.163 0-.326-.02-.485-.06L7 20m7-10V5a2 2 0 00-2-2h-.095c-.5 0-.905.405-.905.905 0 .714-.211 1.412-.608 2.006L7 11v9m7-10h-2M7 20H5a2 2 0 01-2-2v-6a2 2 0 012-2h2.5"></path>
              </svg>
            </div>
            <button class="text-white rounded-2xl text-lg w-full py-3 focus:outline-none bg-cyan-500 hover:bg-cyan-600" x-bind:class="{
                'bg-cyan-500 hover:bg-cyan-600': submitable(),
                'bg-blue-gray-200': !submitable()
              }" :disabled="!submitable()" x-on:click="submit()">
              SUBMIT
            </button>
          </div>
          <!-- end of payment info -->
        </div>
      </div>
      <!-- end of right sidebar -->
    </div>

    <!-- modal receipt -->
    <!--untuk prosess cetak -->
    <!-- <div x-show="isShowModalReceipt" class="fixed w-full h-screen left-0 top-0 z-10 flex flex-wrap justify-center content-center p-24" style="display: none;">
      <div x-show="isShowModalReceipt" class="fixed glass w-full h-screen left-0 top-0 z-0" x-on:click="closeModalReceipt()" x-transition:enter="transition ease-out duration-100" x-transition:enter-start="opacity-0" x-transition:enter-end="opacity-100" x-transition:leave="transition ease-in duration-100" x-transition:leave-start="opacity-100" x-transition:leave-end="opacity-0" style="display: none;"></div>
      <div x-show="isShowModalReceipt" class="w-96 rounded-3xl bg-white shadow-xl overflow-hidden z-10" x-transition:enter="transition ease-out duration-100" x-transition:enter-start="opacity-0 transform scale-90" x-transition:enter-end="opacity-100 transform scale-100" x-transition:leave="transition ease-in duration-100" x-transition:leave-start="opacity-100 transform scale-100" x-transition:leave-end="opacity-0 transform scale-90" style="display: none;">
        <div id="receipt-content" class="text-left w-full text-sm p-6 overflow-auto">
          <div class="text-center">
            <img src="img/receipt-logo.png" alt="Tailwind POS" class="mb-3 w-8 h-8 inline-block">
            <h2 class="text-xl font-semibold">TAILWIND POS</h2>
            <p>CABANG KONOHA SELATAN</p>
          </div>
          <div class="flex mt-4 text-xs">
            <div class="flex-grow">No: <span x-text="receiptNo">TWPOS-KS-1676346641</span></div>
            <div x-text="receiptDate">14/02/23 10.50</div>
          </div>
          <hr class="my-2">
          <div>
            <table class="w-full text-xs">
              <thead>
                <tr>
                  <th class="py-1 w-1/12 text-center">#</th>
                  <th class="py-1 text-left">Item</th>
                  <th class="py-1 w-2/12 text-center">Qty</th>
                  <th class="py-1 w-3/12 text-right">Subtotal</th>
                </tr>
              </thead>
              <tbody>
                <template x-for="(item, index) in cart" :key="item">
                  <tr>
                    <td class="py-2 text-center" x-text="index+1"></td>
                    <td class="py-2 text-left">
                      <span x-text="item.name"></span>
                      <br>
                      <small x-text="priceFormat(item.price)"></small>
                    </td>
                    <td class="py-2 text-center" x-text="item.qty"></td>
                    <td class="py-2 text-right" x-text="priceFormat(item.qty * item.price)"></td>
                  </tr>
                </template>
              
                  
                
                  
                
                  
                
                  
                
                  
                
                  
                
                  
                
                  
                
                  
                
                  
                
                  <tr>
                    <td class="py-2 text-center" x-text="index+1">1</td>
                    <td class="py-2 text-left">
                      <span x-text="item.name">Croissant</span>
                      <br>
                      <small x-text="priceFormat(item.price)">Rp. 16.000</small>
                    </td>
                    <td class="py-2 text-center" x-text="item.qty">7</td>
                    <td class="py-2 text-right" x-text="priceFormat(item.qty * item.price)">Rp. 112.000</td>
                  </tr>
                
                  
                
                  
                
                  
                
                  
                
                  
                
                  
                
                  
                
                  
                
                  <tr>
                    <td class="py-2 text-center" x-text="index+1">2</td>
                    <td class="py-2 text-left">
                      <span x-text="item.name">Beef Burger sadasdad</span>
                      <br>
                      <small x-text="priceFormat(item.price)">Rp. 45.000</small>
                    </td>
                    <td class="py-2 text-center" x-text="item.qty">2</td>
                    <td class="py-2 text-right" x-text="priceFormat(item.qty * item.price)">Rp. 90.000</td>
                  </tr>
                </tbody>
            </table>
          </div>
          <hr class="my-2">
          <div>
            <div class="flex font-semibold">
              <div class="flex-grow">TOTAL</div>
              <div x-text="priceFormat(getTotalPrice())">Rp. 202.000</div>
            </div>
            <div class="flex text-xs font-semibold">
              <div class="flex-grow">PAY AMOUNT</div>
              <div x-text="priceFormat(cash)">Rp. 100.000</div>
            </div>
            <hr class="my-2">
            <div class="flex text-xs font-semibold">
              <div class="flex-grow">CHANGE</div>
              <div x-text="priceFormat(change)">Rp. -102.000</div>
            </div>
          </div>
        </div>
        <div class="p-4 w-full">
          <button class="bg-cyan-500 text-white text-lg px-4 py-3 rounded-2xl w-full focus:outline-none" x-on:click="printAndProceed()">PROCEED</button>
        </div>
      </div>
    </div> -->
  </div>
  <!-- end of noprint-area -->

  <div id="print-area" class="print-area"></div>

<script type="text/javascript">
 

 $(document).on('click','#MinusCart', function(e) {
    e.preventDefault();
    var cart_id = $(this).data('id');
    var location="<?php echo base_url('minus-cart');?>";
    $.ajax({
        url: location,
        type:'POST',
        dataType:'JSON',
        data:{cart_id:cart_id},
    success: function(data){
    
      $( "#container" ).load(window.location.href + " #container" );
      //$("#total_cart").html(data.total_cart);
       var number_string = data.total_cart.toString(),
        sisa  = number_string.length % 3,
        rupiah  = number_string.substr(0, sisa),
        ribuan  = number_string.substr(sisa).match(/\d{3}/g);
          
      if (ribuan) {
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
      }

    $("#total_cart").html("" + rupiah);
    $("#total_id").html(data.total_id);

    }
    });
   
});
 $(document).on('click','#PlusCart', function(e) {
    e.preventDefault();
    var cart_id = $(this).data('id');
    var location="<?php echo base_url('plus-cart');?>";
    $.ajax({
        url: location,
        type:'POST',
        dataType:'JSON',
        data:{cart_id:cart_id},
    success: function(data){
    
      $( "#container" ).load(window.location.href + " #container" );
      //$("#total_cart").html(data.total_cart);
       var number_string = data.total_cart.toString(),
        sisa  = number_string.length % 3,
        rupiah  = number_string.substr(0, sisa),
        ribuan  = number_string.substr(sisa).match(/\d{3}/g);
          
      if (ribuan) {
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
      }

    $("#total_cart").html("" + rupiah);
    $("#total_id").html(data.total_id);

    }
    });
   
});
    
 $(document).on('click','#Layanan', function(e) {
    e.preventDefault();
    var aid = $(this).data('id');
    var sid = $(this).data('harga');
    var location="<?php echo base_url('insert-cart');?>";
    $.ajax({
        url: location,
        type:'POST',
        dataType:'JSON',
        data:{layanan:aid,harga:sid},
    success: function(data){
    
      $( "#container" ).load(window.location.href + " #container" );
      //$("#total_cart").html(data.total_cart);
       var number_string = data.total_cart.toString(),
        sisa  = number_string.length % 3,
        rupiah  = number_string.substr(0, sisa),
        ribuan  = number_string.substr(sisa).match(/\d{3}/g);
          
      if (ribuan) {
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
      }

    $("#total_cart").html("" + rupiah);
    $("#total_id").html(data.total_id);

    }
    });
    //var $p = $('#container');
   
});
    
function keyChange(objek) {
 

  var xy = document.getElementById("getcash").value;

  var x = document.getElementById("getcash2").value = xy.replace(/[^\w\s]/gi, '');

  var y = '<?php echo @$datas->total_cart;?>';
  if(parseInt(x) > parseInt(y)){
    var t = parseInt(x) - parseInt('<?php echo @$datas->total_cart;?>');
  }else{ 
    var t = parseInt('<?php echo @$datas->total_cart;?>') - parseInt(x);
  }
  var number_strings = t.toString(),
        sisas  = number_strings.length % 3,
        rupiahs  = number_strings.substr(0, sisas),
        ribuans  = number_strings.substr(sisas).match(/\d{3}/g);
          
      if (ribuans) {
        separator = sisas ? '.' : '';
        rupiahs += separator + ribuans.join('.');
      }
      if(parseInt(y)  > parseInt(x) ){
        var xx = '- ' + rupiahs;
      }else{
        var xx = rupiahs;

      }
  document.getElementById("getchange").innerHTML = xx;
  var element = document.getElementById("getchange");
  

  if(parseInt(y) > parseInt(x)){
    element.classList.remove("text-cyan-600");
    element.classList.add("text-red-600");
  }else{
    element.classList.remove("text-red-600");
    element.classList.add("text-cyan-600");
  }
   separators = "."; 
   a = objek.value; 
   b = a.replace(/[^\d]/g,""); 
   c = ""; 
   panjang = b.length; 
   j = 0; for (i = panjang; i > 0; i--) { 
   j = j + 1; if (((j % 3) == 1) && (j != 1)) { 
   c = b.substr(i-1,1) + separators + c; } else { 
   c = b.substr(i-1,1) + c; } } objek.value = c;
   
}
function hanyaAngka(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
 
    return false;
    return true;
}

</script>
</body></html>