<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>E-Perpus | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/module/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/module/bootstrap/css/style.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/plugins/iCheck/square/blue.css">
  <style type="text/css">
    .login-box-body {
      border-top: 5px solid #D7320C;
    }
  </style>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
        <div class="login-brand">
              <img src="<?= BASE_ASSET; ?>/img/icon-login.png" alt="logo" class="shadow-light rounded-circle" width="100">
            </div>
        <!-- /.login-logo -->
        <div class="card card-primary" >
            <div class="card-header"><h4>Daftar Akun E-perpus</h4></div>
          <?php if(isset($error) AND !empty($error)): ?>
               <div class="callout callout-error"  style="color:#C82626">
                    <h4><?= cclang('error'); ?>!</h4>
                    <p><?= $error; ?></p>
                  </div>
          <?php endif; ?>
          <?php
          $message = $this->session->flashdata('f_message'); 
          $type = $this->session->flashdata('f_type'); 
          if ($message):
          ?>
         <div class="callout callout-<?= $type; ?>"  style="color:#C82626">
              <p><?= $message; ?></p>
            </div>
          <?php endif; ?>
          <form  action="<?php echo base_url('save-signup');?>" method="POST" enctype="multipart/form-data">
            
              <div class="form-groups">
                            <label for="perpus_murid_nama" class="col-sm-12 control-label">Nama 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" name="perpus_murid_nama" id="perpus_murid_nama" placeholder="Nama" value="<?= set_value('perpus_murid_nama'); ?>">
                                <small class="info help-block">
                                <b>Input Perpus Murid Nama</b> Max Length : 100.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-groups">
                            <label for="perpus_murid_kelasId" class="col-sm-12 control-label">Kelas 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-12">
                                <select  class="form-control chosen chosen-select-deselect" name="perpus_murid_kelasId" id="perpus_murid_kelasId" data-placeholder="Select Kelas" >
                                    <option value=""></option>
                                    <?php foreach (db_get_all_data('perpus_kelas') as $row): ?>
                                    <option value="<?= $row->perpus_kelas_id ?>"><?= $row->perpus_kelas_nama; ?></option>
                                    <?php endforeach; ?>  
                                </select>
                                <small class="info help-block">
                                <b>Input Perpus Murid KelasId</b> Max Length : 2.</small>
                            </div>
                        </div>

                                                 
                                                <div class="form-groups">
                            <label for="perpus_murid_agamaId" class="col-sm-12 control-label">Agama 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-12">
                                <select  class="form-control chosen chosen-select-deselect" name="perpus_murid_agamaId" id="perpus_murid_agamaId" data-placeholder="Select Agama" >
                                    <option value=""></option>
                                    <?php foreach (db_get_all_data('perpus_agama') as $row): ?>
                                    <option value="<?= $row->perpus_agama_id ?>"><?= $row->perpus_agama_nama; ?></option>
                                    <?php endforeach; ?>  
                                </select>
                                <small class="info help-block">
                                <b>Input Perpus Murid AgamaId</b> Max Length : 2.</small>
                            </div>
                        </div>

                                                 
                                                <div class="form-groups">
                            <label for="perpus_murid_jk" class="col-sm-12 control-label">Perpus Murid Jk 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-12">
                                <select  class="form-control chosen chosen-select-deselect" name="perpus_murid_jk" id="perpus_murid_jk" data-placeholder="Select Perpus Murid Jk" >
                                    <option value=""></option>
                                    <?php foreach (db_get_all_data('jenis_kelamin') as $row): ?>
                                    <option value="<?= $row->jenis_kelamin_id ?>"><?= $row->jenis_kelamin_name; ?></option>
                                    <?php endforeach; ?>  
                                </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>

                                                 
                                                <div class="form-groups">
                            <label for="perpus_murid_no_telp" class="col-sm-12 control-label">Telepon 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" name="perpus_murid_no_telp" id="perpus_murid_no_telp" placeholder="Telepon" value="<?= set_value('perpus_murid_no_telp'); ?>">
                                <small class="info help-block">
                                <b>Input Perpus Murid No Telp</b> Max Length : 15.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-groups">
                            <label for="perpus_murid_alamat" class="col-sm-8 control-label">Alamat 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-12">
                                <textarea id="perpus_murid_alamat" name="perpus_murid_alamat" rows="5" cols="45"><?= set_value('Perpus Murid Alamat'); ?></textarea>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-groups">
                            <label for="perpus_murid_ket" class="col-sm-8 control-label">Keterangan 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-12">
                                <textarea id="perpus_murid_ket" name="perpus_murid_ket" rows="5" cols="45"><?= set_value('Perpus Murid Ket'); ?></textarea>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-groups">
                            <label for="perpus_murid_username" class="col-sm-12 control-label">Username 
                            </label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" name="perpus_murid_username" id="perpus_murid_username" placeholder="Username" value="<?= set_value('perpus_murid_username'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-groups">
                            <label for="perpus_murid_password" class="col-sm-12 control-label">Password 
                            </label>
                            <div class="col-sm-12">
                                <input type="password" class="form-control" name="perpus_murid_password" id="perpus_murid_password" placeholder="Password" value="<?= set_value('perpus_murid_password'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                <!-- /.col -->
                            <br>
                            <br>
                            <br>
                <div class="col-lg-12">
                  <button type="submit" class="btn btn-primary btn-block btn-flat"><?= cclang('sign_in'); ?></button>
                </div>

              </div>
            
          <?= form_close(); ?>

          <!-- /.social-auth-links -->

          <br>
          <div class="simple-footer">
              Copyright © ANGGI BIRD 2023
            </div>

        </div>
        <!-- /.login-box-body -->
      </div>
    </div>
  </div>
<!-- /.login-box -->

<!-- jQuery 2.2.3 -->
<script src="<?= BASE_ASSET; ?>/admin-lte/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?= BASE_ASSET; ?>/admin-lte/bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?= BASE_ASSET; ?>/admin-lte/plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>
