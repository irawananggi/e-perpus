<style type="text/css">
   .widget-user-header {
      padding-left: 20px !important;
   }
   .info-box-text{
    margin-top: 15px !important;
   }
   .widget-chart .widget-numbers{
    font-size: 10px !important;
   }
</style>

<link rel="stylesheet" href="<?= BASE_ASSET; ?>admin-lte/plugins/morris/morris.css">

<section class="content-header">
    <h1>
        <?= cclang('dashboard') ?>
        <small>
            
        <?= cclang('control_panel') ?>
        </small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="#">
                <i class="fa fa-dashboard">
                </i>
                <?= cclang('home') ?>
            </a>
        </li>
        <li class="active">
            <?= cclang('dashboard') ?>
        </li>
    </ol>
</section>

<section class="content">
    <div class="row">
      <?php cicool()->eventListen('dashboard_content_top'); ?>

       <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="info-box button" onclick="#">
                <span class="info-box-icon bg-red">
                    <i class="ion ion-ios-gear">
                    </i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">
                         Baru
                         <br>
                         2
                    </span>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="info-box button" onclick="#">
                <span class="info-box-icon bg-yellow">
                    <i class="ion ion-social-chrome">
                    </i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">
                        Keluar
                         <br>
                         4
                    </span>
                </div>
            </div>
        </div>

         <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="info-box button" onclick="goUrl('administrator/page')">
                <span class="info-box-icon bg-aqua">
                    <i class="ion ion-asterisk">
                    </i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">
                        Belum Dikembalikan
                         <br>
                         1
                    </span>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="info-box button" onclick="goUrl('administrator/form')">
                <span class="info-box-icon bg-blue">
                    <i class="ion ion-flash">
                    </i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">
                        Sudah Dikembalikan
                         <br>
                         3
                    </span>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="info-box button" onclick="goUrl('administrator/form')">
                <span class="info-box-icon bg-green">
                    <i class="ion ion-bag">
                    </i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">
                        Selesai
                         <br>
                         100
                    </span>
                </div>
            </div>
        </div>


  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/css/main.css">


  
        
    

    </div>
      <!-- /.row -->
      <?php cicool()->eventListen('dashboard_content_bottom'); ?>

</section>
<!-- /.content -->
