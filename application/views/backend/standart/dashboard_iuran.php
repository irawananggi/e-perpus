<style type="text/css">
   .widget-user-header {
      padding-left: 20px !important;
   }
</style>

<link rel="stylesheet" href="<?= BASE_ASSET; ?>admin-lte/plugins/morris/morris.css">



<section class="content">
    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12 col-mobile" style="padding: 0px 20px;">
            <a href="<?php echo base_url('/administrator/dashboard');?>" ><i class="fa fa-arrow-left "></i></a> Data Iuran
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 col-mobile" style="padding: 0px 20px;">
            <div class="col-md-4 col-sm-4 col-xs-4 col-mobile">
                <div class="row">
                    <div class="info-box button" onclick="goUrl('administrator/iuran')">
                        <div class="col-md-12 col-sm-12 col-xs-12 col-mobile">
                            <img src="<?php echo base_url('asset/icon/real/data-1.PNG');?>" style="width: 100%;"/>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 col-mobile" style="padding:0px 0px;font-size: 12px;text-align: center;">
                            iuran
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4 col-mobile">
                <div class="row">
                    <div class="info-box button" onclick="goUrl('administrator/kelompok')">
                        <div class="col-md-12 col-sm-12 col-xs-12 col-mobile">
                            <img src="<?php echo base_url('asset/icon/real/data-2.PNG');?>" style="width: 100%;"/>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 col-mobile" style="padding:10px 0px;font-size: 12px;text-align: center;">
                            Kelompok Iuran
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  
      <!-- /.row -->
      <?php cicool()->eventListen('dashboard_content_bottom'); ?>

</section>
<!-- /.content -->
