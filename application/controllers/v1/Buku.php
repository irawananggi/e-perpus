<?php
header("Access-Control-Allow-Origin: *");
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');
class ProdukKategori extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_get()
    {
      $status = "";
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['x-token']))
        $token =  $headers['x-token'];

      if ($token != '') {
          $customer = $this->mymodel->getbywhere('customer','customer_token',$token,"row");
          if (!empty($customer)) {
              $data = $this->mymodel->withquery("select * from produk where produk_kategori='".$this->input->get('kategori')."' AND produk_status=1","result");
              $status = "200";
              $msg = array('status' => 1, 'message'=>'Berhasil Ambil Data' ,'data' => $data);
           
          }else {
              $status = "401";
              $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ', 'data' => array());
          }

          $this->response($msg,$status);
      }else {
        $status = "401";
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong', 'data' =>array());
        $this->response($msg,$status);
      }
    }

}