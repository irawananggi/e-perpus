<?php
header('Access-Control-Allow-Origin: *');
date_default_timezone_set('Asia/Jakarta');
defined('BASEPATH') OR exit('No direct script access allowed');

//require APPPATH . 'libraries/REST_Controller.php';
ob_start();
class Register extends REST_Controller {

  function __construct($config = 'rest') {
      parent::__construct($config);
  }

  function index_post() {

        $telp = $this->post('telp');
        $nama = $this->post('nama');
        $email = $this->post('email');

        $ceknotelp = $this->mymodel->getbywhere('customer',"customer_telp",$telp,'row');
        $key = random_int(0, 9999);
		$key = str_pad($key, 4, 0, STR_PAD_LEFT);
		if (empty($ceknotelp)) {
			$data = array( 
					"customer_role" =>1,
					"customer_nama" => $nama,
					"customer_email" => $email,
					"customer_telp" => $telp,
					"customer_token" => md5($email."".time()),
					"customer_otp" => $key
			);
		    $ss = $this->mymodel->insert('customer',$data);
		    $getdatacust = $this->mymodel->getbywhere('customer',"customer_telp",$telp,'row');
			$msg = array('status' => 1, 'message'=>'Berhasil Simpan Data', 'data'=> $getdatacust );
			//send_wa($telp,$key);
		}else{
		  $msg = array('status' => 0, 'message'=>'Nomor Telepon sudah terdaftar',
			'data'=> new stdClass() );
		}
        $this->response($msg,$status);        
    }
	  
}
?>
